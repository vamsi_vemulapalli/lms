 package com.merilytics.bo;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="tbl_compoff_transactions")
public class CompOffTransactionsBO {
	@Id
	@GenericGenerator(name = "gen", strategy = "native")
	@GeneratedValue(generator = "gen")
	private Integer compOffID;
	private Integer employeeID;
	private String compOffReason;
    private Timestamp compOffDate;
    private Integer compOffSession;
    private Timestamp expiryDate;
    private Integer compOffStatus;
    private Integer approvedByORRejectedBy;
    private String reasonForRejectionORRevoke;
    private Integer createdBy;
    private Integer updatedBy;
    private Timestamp createdDate;
    private Timestamp updatedDate;
	public Integer getCompOffID() {
		return compOffID;
	}
	public void setCompOffID(Integer compOffID) {
		this.compOffID = compOffID;
	}
	public Integer getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}
	public String getCompOffReason() {
		return compOffReason;
	}
	public void setCompOffReason(String compOffReason) {
		this.compOffReason = compOffReason;
	}
	public Timestamp getCompOffDate() {
		return compOffDate;
	}
	public void setCompOffDate(Timestamp compOffDate) {
		this.compOffDate = compOffDate;
	}
	public Integer getCompOffSession() {
		return compOffSession;
	}
	public void setCompOffSession(Integer compOffSession) {
		this.compOffSession = compOffSession;
	}
	public Timestamp getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Timestamp expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Integer getCompOffStatus() {
		return compOffStatus;
	}
	public void setCompOffStatus(Integer compOffStatus) {
		this.compOffStatus = compOffStatus;
	}
	
	public Integer getApprovedByORRejectedBy() {
		return approvedByORRejectedBy;
	}
	public void setApprovedByORRejectedBy(Integer approvedByORRejectedBy) {
		this.approvedByORRejectedBy = approvedByORRejectedBy;
	}
	public String getReasonForRejectionORRevoke() {
		return reasonForRejectionORRevoke;
	}
	public void setReasonForRejectionORRevoke(String reasonForRejectionORRevoke) {
		this.reasonForRejectionORRevoke = reasonForRejectionORRevoke;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
		
}
