package com.merilytics.bo;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="tbl_employee_current_leave_balance_transactions")
public class EmployeeCurrentLeaveBalanceTransactionsBO {
	@Id
	@GenericGenerator(name = "gen", strategy = "native")
	@GeneratedValue(generator = "gen")
	private Integer ID;
	private Integer employeeID;
	
	 private Double maternityORPaternityLeaves;
	 private Double PTO;
	 private Double floaterHoliday;
	 private Double compOff;
	 private Timestamp createdDate;
	 private Timestamp updatedDate;
	 private Integer createdBy;
	 private Integer updatedBy;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public Integer getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}
	
	public Double getMaternityORPaternityLeaves() {
		return maternityORPaternityLeaves;
	}
	public void setMaternityORPaternityLeaves(Double maternityORPaternityLeaves) {
		this.maternityORPaternityLeaves = maternityORPaternityLeaves;
	}
	public Double getPTO() {
		return PTO;
	}
	public void setPTO(Double pTO) {
		PTO = pTO;
	}
	public Double getFloaterHoliday() {
		return floaterHoliday;
	}
	public void setFloaterHoliday(Double floaterHoliday) {
		this.floaterHoliday = floaterHoliday;
	}
	public Double getCompOff() {
		return compOff;
	}
	public void setCompOff(Double compOff) {
		this.compOff = compOff;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

}
