package com.merilytics.bo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="tbl_static_leave_status")
public class LeaveStatusBO {
	@Id
	@GenericGenerator(name = "gen", strategy = "native")
	@GeneratedValue(generator = "gen")
	private Integer  leaveStatusID;
	private String leaveStatus;
	
	public String getLeaveStatus() {
		return leaveStatus;
	}
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	public Integer getLeaveStatusID() {
		return leaveStatusID;
	}
	public void setLeaveStatusID(Integer leaveStatusID) {
		this.leaveStatusID = leaveStatusID;
	} 

}
