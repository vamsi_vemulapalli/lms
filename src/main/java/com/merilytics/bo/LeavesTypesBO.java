package com.merilytics.bo;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="tbl_static_leave_types")
public class LeavesTypesBO {
	@Id
	@GenericGenerator(name = "gen", strategy = "native")
	@GeneratedValue(generator = "gen")
	private Integer leaveTypeID;
	private String leaveType;
	private Integer totalLeavesAllowed;
	private Integer maxLeavesAllowedAtOnce;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public Integer getTotalLeavesAllowed() {
		return totalLeavesAllowed;
	}
	public void setTotalLeavesAllowed(Integer totalLeavesAllowed) {
		this.totalLeavesAllowed = totalLeavesAllowed;
	}
	public Integer getMaxLeavesAllowedAtOnce() {
		return maxLeavesAllowedAtOnce;
	}
	public void setMaxLeavesAllowedAtOnce(Integer maxLeavesAllowedAtOnce) {
		this.maxLeavesAllowedAtOnce = maxLeavesAllowedAtOnce;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getLeaveTypeID() {
		return leaveTypeID;
	}
	public void setLeaveTypeID(Integer leaveTypeID) {
		this.leaveTypeID = leaveTypeID;
	}
	@Override
	public String toString() {
		return "LeavesTypesBO [leaveTypeID=" + leaveTypeID + ", leaveType=" + leaveType + ", totalLeavesAllowed="
				+ totalLeavesAllowed + ", maxLeavesAllowedAtOnce=" + maxLeavesAllowedAtOnce + ", createdDate="
				+ createdDate + ", updatedDate=" + updatedDate + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy
				+ "]";
	}
	
	

}
