package com.merilytics.bo;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "tbl_mobile_comments")
public class MobileCommentsBO {
	@Id
	@GenericGenerator(name = "gen", strategy = "native")
	@GeneratedValue(generator = "gen")

	private Integer commentID;
	private Integer commentProvider;
	private String comment;
	private Integer leaveID;

	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	public Integer getCommentID() {
		return commentID;
	}
	public void setCommentID(Integer commentID) {
		this.commentID = commentID;
	}
	public Integer getCommentProvider() {
		return commentProvider;
	}
	public void setCommentProvider(Integer commentProvider) {
		this.commentProvider = commentProvider;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getLeaveID() {
		return leaveID;
	}
	public void setLeaveID(Integer leaveID) {
		this.leaveID = leaveID;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Override
	public String toString() {
		return "MobileCommentsBO [commentID=" + commentID + ", commentProvider=" + commentProvider + ", comment="
				+ comment + ", leaveID=" + leaveID + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate
				+ ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + "]";
	}
	
}
