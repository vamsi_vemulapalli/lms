package com.merilytics.config;

import java.util.List;

import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.merilytics.dao.UserListDAO;
import com.merilytics.util.HibernateUtil;

@Named("TokenValidator2")
public class TokenValidator {

	@Autowired
	private HibernateTemplate LMSauthenticationTemplate;

	@Autowired
	private UserListDAO dao;

	public int isTokenAvailable(String token) {
		Session session = HibernateUtil.getSession(LMSauthenticationTemplate);
		int tokenCount = 0;
		List<Integer> tokenAvailability = null;
		Query query = session.createNativeQuery("select count(1) from employee.tbl_login_details where userToken=:token");
		query.setParameter("token", token);
		tokenAvailability = query.getResultList();
		if (tokenAvailability != null) {
			tokenCount = tokenAvailability.get(0);
			if (tokenCount != 0) {
				try {
					int id = dao.userListwithToken(token);
					if (id != 0) {
						//EmployeeDetails.setEmpID(id);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return tokenCount;
	}
}
