package com.merilytics.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.dto.EmploymentStatusDTO;
import com.merilytics.dto.HolidaysDTO;
import com.merilytics.dto.LeavesTypesDTO;
import com.merilytics.dto.MobileCommentsDTO;
import com.merilytics.service.AdminSettingsService;
import com.merilytics.util.Conversion;
import com.merilytics.util.Loggers;

@Controller
public class AdminsettingsController {
	@Autowired
	private AdminSettingsService settingsService;

	@RequestMapping(value = "/LeavePolicySettings", method = RequestMethod.GET)
	public ResponseEntity<?> getAdminsettings() {
		try {
			Map<String, Object> finalmap = null;
			finalmap = settingsService.getAdminsettings();
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/getUpcomingHolidays", method = RequestMethod.GET)
	public ResponseEntity<?> getUpcomingHolidays() {
		try {

			List<Map<String, Object>> finalData = settingsService.getUpcomingHolidays();

			return new ResponseEntity<>(finalData, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/SaveorUpdateLeaveSettings", method = RequestMethod.POST)
	public ResponseEntity<?> SaveorUpdateLeaveSettings(@RequestBody Map<String, Object> input) {
		Map data = (Map) input.get("data");
		try {

			boolean status = false;
			Integer empid = (Integer) input.get("empId");
			Map<String, Object> mapDto = (Map<String, Object>) input.get("data");
			LeavesTypesDTO dto = new LeavesTypesDTO();
			dto.setLeaveTypeID((Integer) mapDto.get("leaveTypeID"));
			dto.setMaxLeavesAllowedAtOnce((Integer) mapDto.get("maxLeavesAllowedAtOnce"));
			dto.setTotalLeavesAllowed((Integer) mapDto.get("totalLeavesAllowed"));
			dto.setLeaveType((String) mapDto.get("leaveType"));
			dto.setUpdatedBy(empid);
			dto.setUpdatedDate(new Timestamp(new Date().getTime()));
			status = settingsService.SaveorUpdateLeaveSettings(dto, empid);
			Loggers.INFO_lOGGER.info(input + " " + status);
			if (status == true) {
				return new ResponseEntity<>(
						"{\"status\":\"'" + data.get("leaveType") + "' policy details has been updated.\"}",
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while Updating details\"}",
						HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}
	}

	@RequestMapping(value = "/saveOrUpdateProbationAndNoticePeriod", method = RequestMethod.POST)
	public ResponseEntity<?> saveOrUpdateProbationAndNoticePeriod(@RequestBody Map<String, Object> input) {
		Map data = (Map) input.get("data");
		try {
			boolean status = false;
			Integer empid = (Integer) input.get("empId");

			Map<String, Object> mapDto = (Map<String, Object>) input.get("data");
			EmploymentStatusDTO dto = new EmploymentStatusDTO();
			dto.setEmploymentStatusID((Integer) mapDto.get("employmentStatusID"));
			dto.setEmploymentStatus((String) mapDto.get("employmentStatus"));
			dto.setDurationInMonths((Integer) mapDto.get("durationInMonths"));
			dto.setUpdatedBy(empid);
			dto.setUpdatedDate(new Timestamp(new Date().getTime()));

			status = settingsService.saveOrUpdateProbationAndNoticePeriod(dto, empid);
			Loggers.INFO_lOGGER.info(input + " " + status);
			if (status == true) {
				return new ResponseEntity<>(
						"{\"status\":\"'" + data.get("employmentStatus") + "' policy details has been updated.\"}",
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while updating details\"}",
						HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}
	}

	@RequestMapping(value = "/saveOrUpdateHolidays", method = RequestMethod.POST)
	public ResponseEntity<?> saveOrUpdateHolidays(@RequestBody Map<String, Object> input) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			boolean status = false;
			Integer empid = (Integer) input.get("empId");
			HolidaysDTO dto = new HolidaysDTO();
			// dto.setHolidayDate((Date)input.get("holidayDate"));
			Map<String, Object> mapDto = (Map<String, Object>) input.get("data");
			String changeddate = (String) mapDto.get("holidayDate");
			Date date = formatter.parse(changeddate);
			dto.setHolidayID(Conversion.objectToInteger(mapDto.get("holidayID")));
			dto.setHolidayName(Conversion.objectToString(mapDto.get("holidayName")));
			dto.setHolidayTypeID(Conversion.objectToInteger(mapDto.get("holidayTypeID")));
			dto.setHolidayDate(Conversion.convertJavaDateToSqlDate(date));
			dto.setDayOfTheWeek(Conversion.objectToString(mapDto.get("dayOfTheWeek")));
			dto.setUpdatedBy(empid);
			dto.setUpdatedDate(new Timestamp(new Date().getTime()));

			status = settingsService.SaveorUpdateHolidays(dto, empid);
			Loggers.INFO_lOGGER.info(input + " " + status);
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Holiday list has been updated.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while Updating details\"}",
						HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while updating Details \"}",
					HttpStatus.BAD_REQUEST);

		}
	}

	@RequestMapping(value = "/DeleteHolidays", method = RequestMethod.POST)
	public ResponseEntity<?> DeleteHolidays(@RequestBody Map<String, Object> input) {
		try {
			boolean status = false;
			status = settingsService.DeleteHolidays(input);
			Loggers.INFO_lOGGER.info(input + " " + status);
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"" + input.get("holidayName") + " Holiday entry deleted\"}",
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while deleting details\"}",
						HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while deleting Details \"}",
					HttpStatus.BAD_REQUEST);

		}
	}

	// private static final int PAGE_SIZE=10;
	@RequestMapping(value = "/getAdminTeamDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getAdminTeamDetails(@RequestBody Map<String, Object> data) {
		List<Map<String, Object>> teamHistory = null;
		Map<String, Object> details = new HashMap<String, Object>();
		try {

			// pagination logic
			int pageNo = 0;
			int startPos = 0;
			int pagesCount = 0;
			long rowCount = 0;
			int pageSize = 0;
			// read page no
			pageNo = Conversion.objectToInteger(data.get("pageNo"));
			pageSize = Conversion.objectToInteger(data.get("pageSize"));
			// get STart Position to begin the report
			startPos = (pageNo * pageSize) - pageSize;
			// get records to display
			teamHistory = settingsService.getAdminTeamHistory(startPos + 1, pageSize + startPos,
					Conversion.objectToString(data.get("search")));

			// get no.of pages display
			if (teamHistory.size() != 0) {
				Map<String, Object> internalMap = teamHistory.get(0);
				rowCount = Conversion.objectToInteger(internalMap.get("totalCount"));
			}
			pagesCount = (int) rowCount / pageSize;
			if (pagesCount == 0 || rowCount % pageSize > 0) {
				pagesCount++;
				// keep List, pageCount in request attributes
			}

			details.put("teamHistory", teamHistory);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);

			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/getAdminHistoryDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getAdminHistoryDetails(@RequestBody Map<String, Object> data) {
		List<Map<String, Object>> myHistory = null;
		Map<String, Object> details = new HashMap<String, Object>();
		try {

			// pagination logic
			int pageNo = 0;
			int startPos = 0;
			int pagesCount = 0;
			long rowCount = 0;
			int pageSize = 0;
			// read page no
			pageNo = Conversion.objectToInteger(data.get("pageNo"));
			pageSize = Conversion.objectToInteger(data.get("pageSize"));
			// get STart Position to begin the report
			startPos = (pageNo * pageSize) - pageSize;
			// get records to display

			myHistory = settingsService.getAdminHistory(startPos + 1, pageSize + startPos,
					Conversion.objectToString(data.get("search")));
			// get no.of pages display
			if (myHistory.size() != 0) {
				Map<String, Object> internalMap = myHistory.get(0);
				rowCount = Conversion.objectToInteger(internalMap.get("totalCount"));
			}
			pagesCount = (int) rowCount / pageSize;
			if (pagesCount == 0 || rowCount % pageSize > 0) {
				pagesCount++;
				// keep List, pageCount in request attributes
			}
			details.put("myHistory", myHistory);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);

			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/saveComments", method = RequestMethod.POST)
	public ResponseEntity<?> saveComments(@RequestBody MobileCommentsDTO dto) {
		try {
			boolean status = false;
			System.out.println(dto);
			status = settingsService.SaveorUpdateCommetns(dto);
			return new ResponseEntity<>(status, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
}
