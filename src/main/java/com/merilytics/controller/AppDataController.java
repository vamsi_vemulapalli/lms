package com.merilytics.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.dao.UserListDAO;
import com.merilytics.service.AppDataService;
import com.merilytics.util.Loggers;

@Controller
public class AppDataController {
	@Resource
	private AppDataService service;
	@Resource
	private UserListDAO dao;

	@RequestMapping(value = "/getAppList", method = RequestMethod.GET)
	public ResponseEntity<?> getApps(HttpServletResponse response, HttpServletRequest request) {
		List users = null;
		int empID = 0;
		String token = request.getHeader("token");
		response.addHeader("token", token);
		try {
			empID = dao.userListwithToken(token);
			users = service.getAllApps(token);
			Loggers.INFO_lOGGER.info(empID + " " + users);
			return new ResponseEntity<>(users, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"No data Found\"}", HttpStatus.NO_CONTENT);

		}
	}

	@RequestMapping(value = "/getLMSReports", method = RequestMethod.POST)
	public ResponseEntity<?> getLMSReports(@RequestBody int empId, HttpServletResponse response,
			HttpServletRequest request) {

		List data = null;

		String token = request.getHeader("token");
		response.addHeader("token", token);
		try {
			data = service.getLMSReports(empId);

			return new ResponseEntity<>(data, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"No data Found\"}", HttpStatus.NO_CONTENT);

		}
	}
}
