package com.merilytics.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.merilytics.dao.AdminSettingsDao;
import com.merilytics.dao.EmployeeDetailsDao;
import com.merilytics.dao.EmployeeLeaveDisplayDao;
import com.merilytics.dto.EmployeeLeaveTransactionsDTO;
import com.merilytics.service.ApplyLeaveService;
import com.merilytics.service.CompOffCreditService;
import com.merilytics.service.LeaveValidationsService;
import com.merilytics.service.WorkFromHomeService;
import com.merilytics.util.Conversion;
import com.merilytics.util.Loggers;

@Controller
public class ApplyLeaveController {

	@Autowired
	private ApplyLeaveService applyLeaveService;
	@Autowired
	private WorkFromHomeService workFromHomeService;
	@Autowired
	private EmployeeDetailsDao EmployeeDao;
	
	@Autowired
	private CompOffCreditService compOffCreditService;

	@Autowired
	private AdminSettingsDao settingsServiceDao;
	
	@Autowired
	private LeaveValidationsService leaveValidationsService;
	
	@Autowired
	private EmployeeLeaveDisplayDao employeeLeaveDisplayDao;

	@RequestMapping(value = "/saveLeaveDetails", method = RequestMethod.POST)
	public ResponseEntity<?> saveLeaveDetails(@RequestBody Map<String, Object> input,HttpServletRequest request,HttpServletResponse response) {
		

		Map responseMap= new LinkedHashMap();
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> validationDetailsMap=null;
		Gson g = new Gson();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		try {

			boolean status = false;
			String dayHalf = null;
			boolean flag = false;
			float nOfdays = 0;
			
			//List<Map<String, Object>> seniorMgrsMailMap = employeeLeaveDisplayDao.getseniorMangersList(Conversion.objectToInteger(input.get("employeeID")));

			EmployeeLeaveTransactionsDTO leaveDto = new EmployeeLeaveTransactionsDTO();
			Integer empId = (Integer) input.get("employeeID");
			List<Map<String, Object>> emailMap = (List<Map<String, Object>>) input.get("DefaultEmails");
			String startDate = (String) input.get("leaveStartDate");
			String endDate = (String) input.get("leaveEndDate");
			Date sDate = formatter.parse(startDate);
			Date eDate = formatter.parse(endDate);
			
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			Date sdate=sf.parse(startDate);
			Date edate=sf.parse(endDate);
			Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = Integer.parseInt((String) input.get("sessionOfEndDate"));
			Integer leaveTypeID=Conversion.objectToInteger(input.get("leaveType"));
			
			Double days=applyLeaveService.countNoOfDays(sdate,edate,startSession,endSession,leaveTypeID);
			Float d= new Float(days);
			
			Integer isApproveBySLT=Conversion.objectToInteger(input.get("isApproveBySLT"));
			
			
			if(isApproveBySLT==1&&days>0.0)
			{
				
				leaveDto.setNoOfDays(d);
				leaveDto.setEmployeeID(empId);
				leaveDto.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
				leaveDto.setLeaveType(Conversion.objectToInteger(input.get("leaveType")));
				leaveDto.setLeaveStartDate(Conversion.objectToTimestamp(input.get("leaveStartDate")));
				leaveDto.setLeaveEndDate(Conversion.objectToTimestamp(input.get("leaveEndDate")));
				leaveDto.setSessionOfStartDate(Conversion.objectToInteger(input.get("sessionOfStartDate")));
				leaveDto.setSessionOfEndDate(Conversion.objectToInteger(input.get("sessionOfEndDate")));
				leaveDto.setReasonForLeave((String) input.get("reasonForLeave"));
				leaveDto.setLeaveStatus(2);
				leaveDto.setIsAutomated(0);
				leaveDto.setIsApproveSLT(Conversion.objectToInteger(input.get("isApproveBySLT")));
				leaveDto.setCreatedDate(new Timestamp(new Date().getTime()));
				leaveDto.setUpdatedDate(new Timestamp(new Date().getTime()));
				leaveDto.setUpdatedBy(empId);
				leaveDto.setCreatedBy(empId);
				
				map = applyLeaveService.saveLeaveDetails(leaveDto, emailMap);
				
				int recipientLeaveId =Conversion.objectToInteger( map.get("id"));
				Map<String, Object> recipientsMap = new HashMap<String, Object>();
				recipientsMap.put("employeeId", empId);
				recipientsMap.put("leaveid", recipientLeaveId);
				recipientsMap.put("teamId", emailMap);
				
				status = applyLeaveService.saveLeaveEmailRecipients(recipientsMap);
			}
			else if(days>0.0){
				
				validationDetailsMap=leaveValidationsService.validationCheckForLeave(empId, days,sDate,eDate, Integer.parseInt((String)input.get("leaveType")),startSession,endSession);
				
				 if((boolean)validationDetailsMap.get("Status")==true){
						//check the availability of leave before requesting the leave
						leaveDto.setNoOfDays(d);
						leaveDto.setEmployeeID(empId);
						leaveDto.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
						leaveDto.setLeaveType(Conversion.objectToInteger(input.get("leaveType")));
						leaveDto.setLeaveStartDate(Conversion.objectToTimestamp(input.get("leaveStartDate")));
						leaveDto.setLeaveEndDate(Conversion.objectToTimestamp(input.get("leaveEndDate")));
						leaveDto.setSessionOfStartDate(Conversion.objectToInteger(input.get("sessionOfStartDate")));
						leaveDto.setSessionOfEndDate(Conversion.objectToInteger(input.get("sessionOfEndDate")));
						leaveDto.setReasonForLeave((String) input.get("reasonForLeave"));
						leaveDto.setLeaveStatus(2);
						leaveDto.setIsAutomated(0);
						leaveDto.setIsApproveSLT(Conversion.objectToInteger(input.get("isApproveBySLT")));
						leaveDto.setCreatedDate(new Timestamp(new Date().getTime()));
						leaveDto.setUpdatedDate(new Timestamp(new Date().getTime()));
						leaveDto.setUpdatedBy(empId);
						leaveDto.setCreatedBy(empId);
						
						map = applyLeaveService.saveLeaveDetails(leaveDto, emailMap);
						
						int recipientLeaveId =Conversion.objectToInteger( map.get("id"));
						Map<String, Object> recipientsMap = new HashMap<String, Object>();
						recipientsMap.put("employeeId", empId);
						recipientsMap.put("leaveid", recipientLeaveId);
						recipientsMap.put("teamId", emailMap);
						
						status = applyLeaveService.saveLeaveEmailRecipients(recipientsMap);
						/*response.addHeader("message", (Integer)validationDetailsMap.get("isApproveBySLT")+"");
						responseMap.put("status", validationDetailsMap.get("Message"));
						responseMap.put("isApproveBySLT", validationDetailsMap.get("isApproveBySLT"));
						responseMap.put("data", input);
						 return new ResponseEntity<>(responseMap,HttpStatus.OK);*/
						}else{
							response.addHeader("message", (Integer)validationDetailsMap.get("isApproveBySLT")+"");
							responseMap.put("status", validationDetailsMap.get("Message"));
							responseMap.put("isApproveBySLT", validationDetailsMap.get("isApproveBySLT"));
							responseMap.put("data", input);
							 return new ResponseEntity<>(responseMap,HttpStatus.NOT_ACCEPTABLE);
							
						}
			}
			
			Loggers.INFO_lOGGER.info(input + " " + status);
			if(days <= 0.0){
				return new ResponseEntity<>("{\"status\":\"The selected dates are either company holidays or weekends!\"}",
						HttpStatus.BAD_REQUEST);
			}
			else if ((boolean)map.get("status")&&status == true) {
				Map statusmap= new LinkedHashMap();
				statusmap.put("status", "Leave request has been submitted for approval.");
				statusmap.put("note", "Please await the leave approval before confirming your plans for the requested leave.");
				
				
				return new ResponseEntity<>(statusmap, HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error While Applying for a Leave details\"}",
						HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while saving Details \"}", HttpStatus.BAD_REQUEST);

		}
	}

	@RequestMapping(value = "/getDropdownLeaves", method = RequestMethod.POST)
	public ResponseEntity<?> getDropdownLeaves(@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = applyLeaveService.getDropdownLeaves(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/getSessionType", method = RequestMethod.GET)
	public ResponseEntity<?> getSession() {
		try {
			List<Map<String, Object>> finalMap = null;
			finalMap = applyLeaveService.getSession();
			return new ResponseEntity<>(finalMap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/getHolidayType", method = RequestMethod.GET)
	public ResponseEntity<?> getHolidayType() {
		try {
			List<Map<String, Object>> finalMap = null;
			finalMap = applyLeaveService.getHolidayType();
			return new ResponseEntity<>(finalMap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	
	@RequestMapping(value = "/bulkApproveOrRejectLeaveRequest", method = RequestMethod.POST)
	public ResponseEntity<?> bulkApproveLeaveRequest(@RequestBody List<Map<String, Object>> input) {
		List<ResponseEntity<?>> list = new ArrayList<ResponseEntity<?>>();
		
		input.forEach(obj ->{
			ResponseEntity<?> response = approveLeaveRequest(obj);
			list.add(response);
		});
		return  new ResponseEntity<>("{\"status\":\"Request completed.\"}", HttpStatus.OK);
	}
	@RequestMapping(value = "/approveOrRejectLeaveRequest", method = RequestMethod.POST)
	public ResponseEntity<?> approveLeaveRequest(@RequestBody Map<String, Object> input) {
		
		try {
			boolean status = false;
			String leaveStatus="";
			boolean flag=false;
			Map<String, Object> map = new HashMap<String, Object>();
			//Integer empid = (Integer) input.get("employeeID");
			//List<Map<String, Object>> finalMap = EmployeeDao.getEmployee(empid);
			//Integer mgrOfleaveAppliedBy = (Integer) finalMap.get(0).get("managerID");
			
			//approving requests stores data in two transaction table  
			
			int leavestatus=Conversion.objectToInteger(input.get("leaveStatus"));
			String data=(String) input.get("leaveType");
			if(data.equalsIgnoreCase("Work From Home")){
				
				status=workFromHomeService.approveWFH(input);
				String employeeName=(String)input.get("employeeName");
				map=workFromHomeService.approveOrRejectNotification(input,employeeName);
			
				
				if (status == true ) {
					if(leavestatus==1){
						return new ResponseEntity<>("{\"status\":\"WFH request has been approved.\"}", HttpStatus.OK);	
					}
					else if(leavestatus==3)
					{
						return new ResponseEntity<>("{\"status\":\"WFH request has been rejected.\"}", HttpStatus.OK);
					}
					
				} else {
					if(leavestatus==1){
						
					return new ResponseEntity<>("{\"status\":\"Error occured while Aprooving details\"}",
							HttpStatus.BAD_REQUEST);
					}
					else if(leavestatus==3)
					{
						return new ResponseEntity<>("{\"status\":\"Error occured while rejecting details\"}",
								HttpStatus.BAD_REQUEST);
						
					}
					
				}
				
				
			}
			else if(data.equalsIgnoreCase("Compensatory Off Credit")){
				status=compOffCreditService.approveCompOff(input);
				String employeeName=(String)input.get("employeeName");
				map=compOffCreditService.approveOrRejectNotification(input,employeeName);
				
				
				if (status == true ) {
					if(leavestatus==1){
						return new ResponseEntity<>("{\"status\":\"Comp-Off request has been approved.\"}", HttpStatus.OK);	
					}
					else if(leavestatus==3)
					{
						return new ResponseEntity<>("{\"status\":\"Comp-Off request has been rejected.\"}", HttpStatus.OK);
					}
					
				} else {
					if(leavestatus==1){
						
					return new ResponseEntity<>("{\"status\":\"Error occured while Aprooving details\"}",
							HttpStatus.BAD_REQUEST);
					}
					else if(leavestatus==3)
					{
						return new ResponseEntity<>("{\"status\":\"Error occured while rejecting details\"}",
								HttpStatus.BAD_REQUEST);
						
					}
					
				}
			}
			else{
			
			EmployeeLeaveTransactionsDTO leaveDto = new EmployeeLeaveTransactionsDTO();
			leaveDto.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
			leaveDto.setEmployeeID(Conversion.objectToInteger(input.get("employeeID")));
			leaveDto.setApprovedORRejectionDate(new Timestamp(new Date().getTime()));
			leaveDto.setReasonForRejectionORRevoke((String)input.get("reasonForRejectionORRevoke"));
			leaveDto.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("approvedByID")));
			leaveDto.setUpdatedBy(Conversion.objectToInteger(input.get("approvedByID")));
			leaveDto.setLeaveStatus(Conversion.objectToInteger(input.get("leaveStatus")));
			
			leaveDto.setLeaveType(Conversion.objectToInteger(input.get("leaveType")));
			leaveDto.setSessionOfStartDate(Conversion.objectToInteger(input.get("sessionOfStartDate")));
			leaveDto.setSessionOfEndDate(Conversion.objectToInteger(input.get("sessionOfEndDate")));
			

			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = (String) input.get("leaveStartDate");
			String endDate = (String) input.get("leaveEndDate");
			Date sdate=sf.parse(startDate);
			Date edate=sf.parse(endDate);
			Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = Integer.parseInt( input.get("sessionOfEndDate")+"");
			Integer leaveTypeID=Conversion.objectToInteger(input.get("leaveType"));
		/*	leaveDto.setLeaveStartDate(Conversion.objectToTimestamp(sdate));
			leaveDto.setLeaveEndDate(Conversion.objectToTimestamp(edate));*/
			
			Double days=applyLeaveService.countNoOfDays(sdate,edate,startSession,endSession,leaveTypeID);
			Float d= Conversion.objectToFloat(input.get("noOfDays"));
			
			leaveDto.setNoOfDays(d);
			
			status = applyLeaveService.approveLeaveRequest(leaveDto);
			
			String employeeName=(String)input.get("employeeName");
			
			map=applyLeaveService.approveOrRejectNotification(input,employeeName);
			 leaveStatus=(String)map.get("status");
			 flag=(boolean)map.get("flag");
			 
			 
			 if (status == true ) {
					if(leavestatus==1){
						return new ResponseEntity<>("{\"status\":\"Leave request has been approved.\"}", HttpStatus.OK);	
					}
					else if(leavestatus==3)
					{
						return new ResponseEntity<>("{\"status\":\"Leave request has been rejected.\"}", HttpStatus.OK);
					}
					
				} else {
					if(leavestatus==1){
						
					return new ResponseEntity<>("{\"status\":\"Error occured while Aprooving details\"}",
							HttpStatus.BAD_REQUEST);
					}
					else if(leavestatus==3)
					{
						return new ResponseEntity<>("{\"status\":\"Error occured while rejecting details\"}",
								HttpStatus.BAD_REQUEST);
						
					}
					
				}
			
			}
		
			
			
			if (status == true ) {
				if(leavestatus==1){
					return new ResponseEntity<>("{\"status\":\"Request approved\"}", HttpStatus.OK);	
				}
				else if(leavestatus==3)
				{
					return new ResponseEntity<>("{\"status\":\"Request rejected\"}", HttpStatus.OK);
				}
				
			} else {
				if(leavestatus==1){
					
				return new ResponseEntity<>("{\"status\":\"Error occured while Aprooving details\"}",
						HttpStatus.BAD_REQUEST);
				}
				else if(leavestatus==3)
				{
					return new ResponseEntity<>("{\"status\":\"Error occured while rejecting details\"}",
							HttpStatus.BAD_REQUEST);
					
				}
				
			}
			
			System.out.println(data);
		
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while Aprooving Details \"}",
					HttpStatus.BAD_REQUEST);

		}
		return new ResponseEntity<>("{\"status\":\"Error occured while aprooving or rejecting details\"}",
				HttpStatus.BAD_REQUEST);

	}

	

	@RequestMapping(value = "/remindLeaveRequest", method = RequestMethod.POST)
	public ResponseEntity<?> remindLeaveRequest(@RequestBody Map<String, Object> input) {

		try {
			boolean status=false;
			float nOfdays=0;
			
			String startDate = (String) input.get("leaveStartDate");
			String endDate = (String) input.get("leaveEndDate");
			List<Map<String, Object>> emailMap = EmployeeDao.getEmployee((Integer)input.get("employeeID"));
			
			/*String startSession =Conversion.objectToString(input.get("sessionOfStartDate"))  ;
			String endSession = Conversion.objectToString(input.get("sessionOfEndDate"));
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("startSession", startSession);
			map.put("endSession", endSession);*/
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			Date sdate=sf.parse(startDate);
			Date edate=sf.parse(endDate);
			Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = new Integer(input.get("sessionOfEndDate") + "");
			Integer leaveTypeID=Conversion.objectToInteger(input.get("leaveType"));
			Double d=applyLeaveService.countNoOfDays(sdate,edate,startSession,endSession,leaveTypeID);
			Float days= Conversion.objectToFloat(input.get("noOfDays"));
		//	nOfdays = DateOperations.countNoOfDays(startDate, endDate,map);
			String leave=Conversion.objectToString(input.get("leaveType"));
			
			if(leave.equals("Compensatory Off Credit"))
			{
				compOffCreditService.remindLeaveRequest(input,days);
				status=true;
			}
			else if(leave.equals("Work from Home")){
				workFromHomeService.remindLeaveRequest(input,days);
				status=true;
				
				
			}
			else{
			applyLeaveService.remindLeaveRequest(input,days);
			status=true;
			}
			
		
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Your manager has been notified about your request.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while Reminding Leave request\"}",
						HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while Reminding Leave request \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	
	/*@RequestMapping(value = "/rejectNotification", method = RequestMethod.POST)
	public ResponseEntity<?> rejectNotification(@RequestBody Map<String, Object> input) {

		try {
			boolean status=false;
		
		
			List<Map<String, Object>> emailMap = (List<Map<String, Object>>) input.get("DefaultEmails");
			
			
			
			 applyLeaveService.rejectNotificationEmail(input, emailMap);
			
				
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Reminding Leave Details\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while Reminding Leave request\"}",
						HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while Reminding Leave request \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	*/
	
	
	@RequestMapping(value = "/requestingFromUnpaidToPaid", method = RequestMethod.POST)
	public ResponseEntity<?> requestingFromUnpaidToPaid(@RequestBody Map<String, Object> input) {
boolean status=false;
		try {
			Timestamp leaveDate = Conversion.objectToTimestamp1(input.get("leaveStartDate"));
			Date d = leaveDate;
			Date today = new Date();
			 long diffInMillies = Math.abs(d.getTime() - today.getTime());
			 long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
			 if(diff<=21&&((boolean)input.get("isSM")==true)) {
		System.out.println("in if block");
			status=applyLeaveService.convertFromUnpaidToPaid(input);
			 }else {
					System.out.println("in else block");
				 status = false;
			 }
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Leave conversion request submitted for approval.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while changing the request.\"}",
						HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while Reminding Leave request \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/approveFromUnpaidToPaid", method = RequestMethod.POST)
	public ResponseEntity<?> approveFromUnpaidToPaid(@RequestBody Map<String, Object> input) {
		boolean status=false;
		try {
						
			status=applyLeaveService.approveFromUnpaidToPaid(input);
		
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Change in leave type has been approved.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while changing the request.\"}",
						HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while approving Leave request \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/rejectFromUnpaidToPaid", method = RequestMethod.POST)
	public ResponseEntity<?> rejectFromUnpaidToPaid(@RequestBody Map<String, Object> input) {
		boolean status=false;
		try {
						
			status=applyLeaveService.rejectFromUnpaidToPaid(input);
		
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Change in leave type has been rejected.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while changing the request.\"}",
						HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while Reminding Leave request \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
}
