package com.merilytics.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.dao.EmployeeLeaveDisplayDao;
import com.merilytics.dto.WorkFromHomeTransactionsDTO;
import com.merilytics.service.LeaveValidationsService;
import com.merilytics.service.WorkFromHomeService;
import com.merilytics.util.Conversion;
import com.merilytics.util.Loggers;

@Controller
public class ApplyWorkFromHomeController {
	@Autowired
	private WorkFromHomeService workFromHomeService;
	
	@Autowired
	private LeaveValidationsService leaveValidationsService;
	
	@Autowired
	private EmployeeLeaveDisplayDao employeeLeaveDisplayDao;

	@RequestMapping(value = "/saveWfhDetails", method = RequestMethod.POST)
	public ResponseEntity<?> saveWfhDetails(@RequestBody Map<String, Object> input,HttpServletRequest request,HttpServletResponse response) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Map<String, Object> validationDetailsMap=null;
		Map<String, Object> map = new HashMap<String, Object>();
		Map responseMap= new LinkedHashMap();
		try {

			boolean status = false;
			boolean flag = false;
			WorkFromHomeTransactionsDTO wfhDto = new WorkFromHomeTransactionsDTO();
			Integer empId = (Integer) input.get("employeeID");
			String startDate = (String) input.get("leaveStartDate");
			String endDate = (String) input.get("leaveEndDate");
			Date sDate = formatter.parse(startDate);
			Date eDate = formatter.parse(endDate);
			Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = Integer.parseInt((String) input.get("sessionOfEndDate"));
			Integer leaveTypeID=0;
			Double days=workFromHomeService.countNoOfDays(sDate,eDate,startSession,endSession,leaveTypeID);
			Float d= new Float(days);
			Integer isApproveBySLT=Conversion.objectToInteger(input.get("isApproveBySLT"));
			/*if (sDate.compareTo(eDate) == 0) {
				
				
				if(startSession==1||endSession==1){
					wfhDto.setNoOfDays(1);
				}
				if(startSession == 2 || startSession == 3 || endSession == 2 || endSession == 3)
				{
					double d = 0.5;
					float f = (float) d;
					nOfdays = f;
					wfhDto.setNoOfDays(nOfdays);

				}
				
			} else {

				Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
				Integer endSession = Integer.parseInt((String) input.get("sessionOfEndDate"));
				if ((startSession == 1 || startSession == 2) && (endSession == 1 || endSession == 3)) {
					nOfdays = DateOperations.diff(sDate, eDate);
					nOfdays = nOfdays + 1;
					wfhDto.setNoOfDays(nOfdays);

				}

				else {
					nOfdays = DateOperations.diff(sDate, eDate);
					if (startSession == 3 && endSession == 2) {
						wfhDto.setNoOfDays(nOfdays);
						}

					else if (startSession == 2 || startSession == 3 || endSession == 2 || endSession == 3) {
						double d = 0.5;
						float f = (float) d;
						nOfdays = nOfdays + f;
						wfhDto.setNoOfDays(nOfdays);

					}
				}
			}*/
		
	//List<Map<String, Object>> seniorMgrsMailMap = employeeLeaveDisplayDao.getseniorMangersList(Conversion.objectToInteger(input.get("employeeID")));	
			
			if(isApproveBySLT==1)
			{
				wfhDto.setWorkFromHomeID((Integer) input.get("workFromHomeID"));
				wfhDto.setEmployeeID(empId);
				List<Map<String, Object>> emailMap = (List<Map<String, Object>>) input.get("DefaultEmails");

				wfhDto.setReasonForWorkFromHome((String) input.get("reasonForWorkFromHome"));
				wfhDto.setStartDate(Conversion.objectToTimestamp(input.get("leaveStartDate")));
				wfhDto.setEndDate(Conversion.objectToTimestamp(input.get("leaveEndDate")));
				wfhDto.setSessionOfStartDate(Conversion.objectToInteger(input.get("sessionOfStartDate")));
				wfhDto.setSessionOfEndDate(Conversion.objectToInteger(input.get("sessionOfEndDate")));
				wfhDto.setWorkFromHomeStatus(2);
				wfhDto.setNoOfDays(d);
				// wfhDto.setApprovedORRejectionDate(approvedORRejectionDate);
				wfhDto.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
				wfhDto.setCreatedDate(new Timestamp(new Date().getTime()));
				wfhDto.setUpdatedDate(new Timestamp(new Date().getTime()));
				wfhDto.setIsApproveSLT(Conversion.objectToInteger(input.get("isApproveBySLT")));
				wfhDto.setIsAutomated(0);

				wfhDto.setUpdatedBy(empId);
				wfhDto.setCreatedBy(empId);
				
				map = workFromHomeService.saveWfhDetails(wfhDto, emailMap);
				
				int recipientLeaveId = (Integer) map.get("id");
				Map<String, Object> recipientsMap = new HashMap<String, Object>();
				recipientsMap.put("employeeId", empId);
				recipientsMap.put("wfhid", recipientLeaveId);
				recipientsMap.put("teamId", emailMap);
				
				status = workFromHomeService.saveLeaveEmailRecipients(recipientsMap);
			}
		else{
			 validationDetailsMap=leaveValidationsService.validationCheckForWFH(empId,sDate, eDate,d,startSession,endSession);
			if((boolean)validationDetailsMap.get("Status")==true){
			wfhDto.setWorkFromHomeID((Integer) input.get("workFromHomeID"));
			wfhDto.setEmployeeID(empId);
			List<Map<String, Object>> emailMap = (List<Map<String, Object>>) input.get("DefaultEmails");

			wfhDto.setReasonForWorkFromHome((String) input.get("reasonForWorkFromHome"));
			wfhDto.setStartDate(Conversion.objectToTimestamp(input.get("leaveStartDate")));
			wfhDto.setEndDate(Conversion.objectToTimestamp(input.get("leaveEndDate")));
			wfhDto.setSessionOfStartDate(Conversion.objectToInteger(input.get("sessionOfStartDate")));
			wfhDto.setSessionOfEndDate(Conversion.objectToInteger(input.get("sessionOfEndDate")));
			wfhDto.setWorkFromHomeStatus(2);
			wfhDto.setNoOfDays(d);
			// wfhDto.setApprovedORRejectionDate(approvedORRejectionDate);
			wfhDto.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
			wfhDto.setCreatedDate(new Timestamp(new Date().getTime()));
			wfhDto.setUpdatedDate(new Timestamp(new Date().getTime()));
			wfhDto.setIsApproveSLT(1);
			wfhDto.setIsAutomated(0);
			wfhDto.setUpdatedBy(empId);
			wfhDto.setCreatedBy(empId);
			
			map = workFromHomeService.saveWfhDetails(wfhDto, emailMap);
			
			int recipientLeaveId = (Integer) map.get("id");
			Map<String, Object> recipientsMap = new HashMap<String, Object>();
			recipientsMap.put("employeeId", empId);
			recipientsMap.put("wfhid", recipientLeaveId);
			recipientsMap.put("teamId", emailMap);
			
			status = workFromHomeService.saveLeaveEmailRecipients(recipientsMap);
			}else{
				response.addHeader("message", (Integer)validationDetailsMap.get("isApproveBySLT")+"");
				responseMap.put("status", validationDetailsMap.get("Message"));
				responseMap.put("isApproveBySLT", validationDetailsMap.get("isApproveBySLT"));
				responseMap.put("data", input);
				 return new ResponseEntity<>(responseMap,HttpStatus.NOT_ACCEPTABLE);
				
			 }
		}
			
			/*flag = (boolean) map.get("status");*/
			Loggers.INFO_lOGGER.info(input + " " + status);

			if ((boolean)map.get("status")&&status == true) {
				return new ResponseEntity<>("{\"status\":\"WFH request has been submitted for approval.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error While Applying WFH\"}",
						HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while saving Details \"}", HttpStatus.BAD_REQUEST);

		}
	}
	
	

}
