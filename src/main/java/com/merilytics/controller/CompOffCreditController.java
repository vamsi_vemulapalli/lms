package com.merilytics.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.dao.EmployeeLeaveDisplayDao;
import com.merilytics.dto.CompOffTransactionsDTO;
import com.merilytics.service.CompOffCreditService;
import com.merilytics.service.LeaveValidationsService;
import com.merilytics.util.Conversion;
import com.merilytics.util.DateOperations;
import com.merilytics.util.Loggers;

@Controller
public class CompOffCreditController {

	@Autowired
	private CompOffCreditService compOffCreditService;
	
	@Autowired
	private LeaveValidationsService leaveValidationsService;
	@Autowired
	private EmployeeLeaveDisplayDao employeeLeaveDisplayDao;
	

	@RequestMapping(value = "/saveCompOffDetails", method = RequestMethod.POST)
	public ResponseEntity<?> saveCompOffDetails(@RequestBody Map<String, Object> input) {
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> validationDetailsMap=null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		//List<Map<String, Object>> seniorMgrsMailMap = employeeLeaveDisplayDao.getseniorMangersList(Conversion.objectToInteger(input.get("employeeID")));	
		try {

			boolean status = false;
			double nOfdays = 0;
			Integer empId = (Integer) input.get("employeeID");
			List<Map<String, Object>> emailMap = (List<Map<String, Object>>) input.get("DefaultEmails");
			CompOffTransactionsDTO compOffDto = new CompOffTransactionsDTO();
			String sDate = (String) input.get("CompOffStartDate");
			String endDate = (String) input.get("CompOffEndDate");
			Date startDate = formatter.parse(sDate);
			Date eDate = formatter.parse(endDate);
			/*Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = Integer.parseInt((String) input.get("sessionOfEndDate"));*/
			//Double days=compOffCreditService.countNoOfDays(startDate,eDate,startSession,endSession);
			//Double d= new Double(days);
			Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = Integer.parseInt((String) input.get("sessionOfEndDate"));
			if (startDate.compareTo(eDate) == 0) {
				
				
				if(startSession==1||endSession==1){
					nOfdays=1.0;
				}
				if(startSession == 2 || startSession == 3 || endSession == 2 || endSession == 3)
				{
					double d = 0.5;
					float f = (float) d;
					nOfdays = f;
					

				}
				
			} else {

				
				if ((startSession == 1 || startSession == 2) && (endSession == 1 || endSession == 3)) {
					nOfdays = DateOperations.diff(startDate, eDate);
					nOfdays = nOfdays + 1;
				}else {
					nOfdays = DateOperations.diff(startDate, eDate);
					if (startSession == 3 && endSession == 2) {
					}
					else if (startSession == 2 || startSession == 3 || endSession == 2 || endSession == 3) {
						double d = 0.5;
						float f = (float) d;
						nOfdays = nOfdays + f;

					}
				}
			}
			validationDetailsMap=leaveValidationsService.validationCheckForCompOff(empId, nOfdays,startDate,eDate,startSession,endSession);
			if((boolean)validationDetailsMap.get("Status")==true){
			compOffDto.setNoofdays(nOfdays);
			compOffDto.setCompOffID((Integer) input.get("compOffID"));
			compOffDto.setEmployeeID(empId);
			compOffDto.setCompOffStartDate(Conversion.objectToTimestamp(input.get("CompOffStartDate")));
			compOffDto.setCompOffEndDate(Conversion.objectToTimestamp(input.get("CompOffEndDate")));
			compOffDto.setCompOffStatus(2);
			compOffDto.setCompOffReason((String) input.get("compOffReason"));

			compOffDto.setSessionOfStartDate(Conversion.objectToInteger(input.get("sessionOfStartDate")));
			compOffDto.setSessionOfEndDate(Conversion.objectToInteger(input.get("sessionOfEndDate")));
		    compOffDto.setReasonForRejectionORRevoke(Conversion.objectToString(input.get("reasonForRejectionORRevoke")));
			compOffDto.setCreatedDate(new Timestamp(new Date().getTime()));
			compOffDto.setUpdatedDate(new Timestamp(new Date().getTime()));
			compOffDto.setUpdatedBy(empId);
			compOffDto.setCreatedBy(empId);
			//map = compOffCreditService.saveCompOffDetails(compOffDto, emailMap);
			//int recipientLeaveId = (Integer) map.get("id");
			Map<String, Object> recipientsMap = new HashMap<String, Object>();
			recipientsMap.put("employeeId", empId);
			//recipientsMap.put("compOffid", recipientLeaveId);
			recipientsMap.put("teamId", emailMap);
			
			//status = compOffCreditService.saveLeaveEmailRecipients(recipientsMap);
			status=compOffCreditService.saveCompOffDetailsWithProc(compOffDto, emailMap, recipientsMap);
			}else{
				return new ResponseEntity<>("{\"status\":\""+validationDetailsMap.get("Message")+"\"}",HttpStatus.NOT_ACCEPTABLE);
			}
			Loggers.INFO_lOGGER.info(input + " " + status);
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Comp-Off request has been submitted for approval.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while Requesting For CompOff Credit \"}",
						HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while saving Details \"}", HttpStatus.BAD_REQUEST);

		}
	}

}
