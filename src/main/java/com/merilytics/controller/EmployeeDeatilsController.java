package com.merilytics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.service.EmployeeDetailsService;
import com.merilytics.util.Conversion;
import com.merilytics.util.Loggers;

@Controller
public class EmployeeDeatilsController {
	@Autowired
	private EmployeeDetailsService EmployeeService;

	@RequestMapping(value = "/getEmployeeDeatils", method = RequestMethod.GET)
	public ResponseEntity<?> getEmployeeDeatils() {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = EmployeeService.getEmployeeDetails();
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/getEmployeeInfo", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployee(@RequestBody Integer empId) {

		Map<String, Object> details = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> finalmap = null;
			List<Map<String, Object>> currentLeaveBalance = null;
			finalmap = EmployeeService.getEmployee(empId);
			currentLeaveBalance = EmployeeService.getCurrentLeaveBalance(empId);
			details.put("employyeInfo", finalmap);
			details.put("currentleaveBalance", currentLeaveBalance);

			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/getEmployeeTeamDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeTeamDetails(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> teamDetails = null;
		try {
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;

			teamDetails = EmployeeService.getEmployeeTeamDetails(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")));
			
			// get no.of pages display
						if(teamDetails.size()!=0){
						Map<String, Object> internalMap=teamDetails.get(0);
						rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
						}
						pagesCount=(int)rowCount/pageSize;
						if(pagesCount==0 || rowCount%pageSize>0)
						 {
							pagesCount++;
						//keep List, pageCount in request attributes
						}
			details.put("teamDetails", teamDetails);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getEmployeeTeamDetailsMobileApproval", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeTeamDetailsMobileApproval(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> teamDetails = null;
		try {
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;

			teamDetails = EmployeeService.getEmployeeTeamDetailsMobApproval(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")),Conversion.objectToInteger(data.get("filter")));
			
			// get no.of pages display
						if(teamDetails.size()!=0){
						Map<String, Object> internalMap=teamDetails.get(0);
						rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
						}
						pagesCount=(int)rowCount/pageSize;
						if(pagesCount==0 || rowCount%pageSize>0)
						 {
							pagesCount++;
						//keep List, pageCount in request attributes
						}
			details.put("teamDetails", teamDetails);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	@RequestMapping(value = "/getEmployeeTeamDetailsMobileReview", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeTeamDetailsMobileReview(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> teamDetails = null;
		try {
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;
			System.out.println(data);
			teamDetails = EmployeeService.getEmployeeTeamDetailsMobReview(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")),Conversion.objectToInteger(data.get("filter")));
			
			// get no.of pages display
						if(teamDetails.size()!=0){
						Map<String, Object> internalMap=teamDetails.get(0);
						rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
						}
						pagesCount=(int)rowCount/pageSize;
						if(pagesCount==0 || rowCount%pageSize>0)
						 {
							pagesCount++;
						//keep List, pageCount in request attributes
						}
			details.put("teamDetails", teamDetails);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	@RequestMapping(value = "/getEmployeeTeamHistoryDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeTeamHistoryDetails(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> teamHistory = null;
		try {
		
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;
			
			teamHistory = EmployeeService.getTeamHistory(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")));
			// get no.of pages display
			if(teamHistory.size()!=0){
			Map<String, Object> internalMap=teamHistory.get(0);
			rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
			}
			pagesCount=(int)rowCount/pageSize;
			if(pagesCount==0 || rowCount%pageSize>0)
			 {
				pagesCount++;
			//keep List, pageCount in request attributes
			}
			details.put("teamHistory", teamHistory);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);

			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	@RequestMapping(value = "/getEmployeeTeamHistoryDetailsMobile", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeTeamHistoryDetailsMobile(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> teamHistory = null;
		try {
		
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;
			
			teamHistory = EmployeeService.getTeamHistoryMobile(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")),Conversion.objectToString(data.get("leaveType")));
			// get no.of pages display
			if(teamHistory.size()!=0){
			Map<String, Object> internalMap=teamHistory.get(0);
			rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
			}
			pagesCount=(int)rowCount/pageSize;
			if(pagesCount==0 || rowCount%pageSize>0)
			 {
				pagesCount++;
			//keep List, pageCount in request attributes
			}
			details.put("teamHistory", teamHistory);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);

			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	

	@RequestMapping(value = "/getEmployeeMeDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeMeDetails(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> myDetails = null;
		try {
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;
			

			myDetails = EmployeeService.getEmployeeMeDetails(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")));
			// get no.of pages display
						if(myDetails.size()!=0){
						Map<String, Object> internalMap=myDetails.get(0);
						rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
						}
						pagesCount=(int)rowCount/pageSize;
						if(pagesCount==0 || rowCount%pageSize>0)
						 {
							pagesCount++;
						//keep List, pageCount in request attributes
						}
			details.put("myDetails", myDetails);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);

			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getEmployeeMeHistoryDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeMeHistoryDetails(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> myHistory = null;
		try {
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;
			myHistory = EmployeeService.getEmployeeHistory(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")));
			// get no.of pages display
			if(myHistory.size()!=0){
			Map<String, Object> internalMap=myHistory.get(0);
			rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
			}
			pagesCount=(int)rowCount/pageSize;
			if(pagesCount==0 || rowCount%pageSize>0)
			 {
				pagesCount++;
			//keep List, pageCount in request attributes
			}
			
			details.put("myHistory", myHistory);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getEmployeeMeHistoryDetailsMobile", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeMeHistoryDetailsMobile(@RequestBody Map<String,Object> data) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> myHistory = null;
		try {
			//pagination logic
			int pageNo=0;
			int startPos=0;
			int pagesCount=0;
			long rowCount=0;
			int pageSize=0;
			//read page no
			pageNo=Conversion.objectToInteger(data.get("pageNo"));
			pageSize=Conversion.objectToInteger(data.get("pageSize"));
			//get STart Position to begin the report
			startPos=(pageNo*pageSize)-pageSize;
			myHistory = EmployeeService.getEmployeeHistoryMobile(Conversion.objectToInteger(data.get("empID")),startPos+1,pageSize+startPos,Conversion.objectToString(data.get("search")),Conversion.objectToString(data.get("leaveType")));
			// get no.of pages display
			if(myHistory.size()!=0){
			Map<String, Object> internalMap=myHistory.get(0);
			rowCount=Conversion.objectToInteger(internalMap.get("totalCount"));
			}
			pagesCount=(int)rowCount/pageSize;
			if(pagesCount==0 || rowCount%pageSize>0)
			 {
				pagesCount++;
			//keep List, pageCount in request attributes
			}
			
			details.put("myHistory", myHistory);
			details.put("pages", pagesCount);
			details.put("totalCount", rowCount);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/loginNotification", method = RequestMethod.POST)
	public ResponseEntity<?> loginNotification(@RequestBody Integer empID) {

		Map<String, Object> details = new HashMap<String, Object>();
		List<Map<String, Object>> teamDetails = null;
		List<Map<String, Object>> myHistory = null;
		try {
			
			teamDetails = EmployeeService.getEmployeeTeamDetails(empID,1,10,null);
			myHistory = EmployeeService.getEmployeeHistory(empID,1,10,null);
			// get no.of pages display
			if(teamDetails.size()!=0){
				Map<String, Object> internalMap=teamDetails.get(0);
				details.put("approvedNotifications", internalMap.get("approvedNotifications"));
				}else{
					details.put("approvedNotifications", null);
				}
			
			if(myHistory.size()!=0){
				Map<String, Object> internalMap=myHistory.get(0);
				details.put("AutomatedNotifications", internalMap.get("AutomatedNotifications"));
				}else{
					details.put("AutomatedNotifications", null);
				}
			
			
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	@RequestMapping(value = "/yearAndMonthToDateStatus", method = RequestMethod.POST)
	public ResponseEntity<?> yearToDateStatus(@RequestBody Integer empId) {

		Map<String, Object> details = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> yearMap = null;
			List<Map<String, Object>> monthMap=null;
			yearMap = EmployeeService.yearToDateStatus(empId);
			monthMap=EmployeeService.monthToDateStatus(empId);
			details.put("yearToDateStatus", yearMap);
			details.put("monthToDateStatus", monthMap);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/yearAndMonthToDateStatusMobile", method = RequestMethod.POST)
	public ResponseEntity<?> yearToDateStatusMobile(@RequestBody Map<String,Object> map) {

		
		try {
		
			List<Map<String, Object>> monthMap=null;
			
			monthMap=EmployeeService.monthToDateStatusMobile(Conversion.objectToInteger( map.get("empId")), Conversion.objectToInteger( map.get("year")), Conversion.objectToInteger( map.get("month")));
			
			return new ResponseEntity<>(monthMap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	@RequestMapping(value = "/getLeaveComments", method = RequestMethod.POST)
	public ResponseEntity<?> getLeaveComments(@RequestBody Map<String,Object> empId) {

		
		try {
			List<Map<String, Object>> data = null;
			
			data = EmployeeService.getLeaveCom(Conversion.objectToInteger( empId.get("leaveID")));
			
			
			return new ResponseEntity<>(data, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/checkAdmin", method = RequestMethod.POST)
	public ResponseEntity<?> checkAdmin(@RequestBody Integer empId) {

		Map<String, Object> details = new HashMap<String, Object>();
		try {
			Map<String, Object> adminList = null;
			adminList = EmployeeService.checkAdmin(empId);
			
			return new ResponseEntity<>(adminList, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	@RequestMapping(value = "/getUserScreensList ", method = RequestMethod.POST)
	public ResponseEntity<?> getUserScreensList (@RequestBody Integer empId) {

		Map<String, Object> details = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> userList = null;
			userList = EmployeeService.getUserScreensList (empId);
			details.put("userList", userList);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getpaidleavesInfo", method = RequestMethod.POST)
	public ResponseEntity<?> getpaidleavesInfo(@RequestBody Integer empId) {

		Map<String, Object> details = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> paidMTDMap = null;
			List<Map<String, Object>> paidYTDMap = null;
			paidMTDMap = EmployeeService.paidLeaveHyperLinkMTD(empId);
			paidYTDMap = EmployeeService.paidLeaveHyperLinkYTD(empId);
			details.put("MonthToDate", paidMTDMap);
			details.put("YearToDate", paidYTDMap);

			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	
	
	
	

}
