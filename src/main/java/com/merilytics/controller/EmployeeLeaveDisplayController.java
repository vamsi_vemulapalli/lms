package com.merilytics.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.dao.UserListDAO;
import com.merilytics.service.EmployeeleaveDispalyService;
import com.merilytics.util.Loggers;

@Controller
public class EmployeeLeaveDisplayController {

	@Autowired
	private EmployeeleaveDispalyService employeeleaveDispalyService;
	
	@Autowired
	private UserListDAO userDao;

	@RequestMapping(value = "/getEmailRecipientsList", method = RequestMethod.POST)
	public ResponseEntity<?> getEmailRecipientsList(@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getEmailRecipientsList(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/getDefaultEmailRecipients", method = RequestMethod.POST)
	public ResponseEntity<?> getDefaultEmailRecipients(@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getDefaultEmailRecipients(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/cancelLeaveByEmployee", method = RequestMethod.POST)
	public ResponseEntity<?> cancelLeaveByEmployee(@RequestBody Map<String, Object> input) {
		
		try {
			boolean status = false;
			String data=(String) input.get("leaveType");
			if(data.equalsIgnoreCase("Work from Home")){
				
				status=employeeleaveDispalyService.cancelLeaveByEmployee(input);	
				if (status == true) {
					return new ResponseEntity<>("{\"status\":\"WFH request has been revoked.\"}", HttpStatus.OK);
				} else {
					return new ResponseEntity<>("{\"status\":\"Error occured while Cancelling Leave details\"}",
							HttpStatus.BAD_REQUEST);
				}
				
			}
			else if(data.equalsIgnoreCase("Compensatory Off Credit")){
				status=employeeleaveDispalyService.cancelLeaveByEmployee(input);
				if (status == true) {
					return new ResponseEntity<>("{\"status\":\"Comp-Off request has been revoked.\"}", HttpStatus.OK);
				} else {
					return new ResponseEntity<>("{\"status\":\"Error occured while Cancelling Leave details\"}",
							HttpStatus.BAD_REQUEST);
				}
			}
			else{
			status=employeeleaveDispalyService.cancelLeaveByEmployee(input);
			if (status == true) {
				return new ResponseEntity<>("{\"status\":\"Leave request has been revoked.\"}", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("{\"status\":\"Error occured while Cancelling Leave details\"}",
						HttpStatus.BAD_REQUEST);
			}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while Cancelling Leave  \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	
	@RequestMapping(value = "/individualEmployeeCompOffCreditList", method = RequestMethod.POST)
	public ResponseEntity<?> individualEmployeeCompOffCreditList(@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.individualEmployeeCompOffCreditList(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	
	@RequestMapping(value = "/getUnpaidleavesInfo", method = RequestMethod.POST)
	public ResponseEntity<?> getUnpaidLeaveHyperLinKYearToDate(@RequestBody Integer empId) {
		Map<String, Object> details = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> unpaidMTDMap = null;
			List<Map<String, Object>> unpaidYTDMap =null;
			
			unpaidYTDMap = employeeleaveDispalyService.getUnpaidLeaveHyperLinKYearToDate(empId);
			unpaidMTDMap = employeeleaveDispalyService.getUnpaidLeaveHyperLinKMonthToDate(empId);
			details.put("MonthToDate", unpaidMTDMap);
			details.put("YearToDate", unpaidYTDMap);
			return new ResponseEntity<>(details, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	/*@RequestMapping(value = "/getUnpaidLeaveHyperLinKMonthToDate", method = RequestMethod.POST)
	public ResponseEntity<?> getUnpaidLeaveHyperLinKMonthToDate(@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getUnpaidLeaveHyperLinKMonthToDate(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}*/
	
	
	@RequestMapping(value = "/getcalendarDaysDisplay", method = RequestMethod.POST)
	public ResponseEntity<?> getcalendarDaysDisplay(@RequestBody Map<String,Object> map) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getcalendarDaysDisplay(map);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	@RequestMapping(value = "/getHolidaysList", method = RequestMethod.POST)
	public ResponseEntity<?> getHolidaysList(@RequestBody Map<String,Object> map) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getholidaysList(map);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	@RequestMapping(value = "/getHolidaysListMobile", method = RequestMethod.POST)
	public ResponseEntity<?> getHolidaysListMobile(@RequestBody Map<String,Object> map) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getholidaysListMobile(map);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	@RequestMapping(value = "/getEmployeeStatusDisplay", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeStatusDisplay(@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getEmployeeStatusDisplay(empId);
			return new ResponseEntity<>(finalmap.get(0), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	@RequestMapping(value = "/updateEmploymentStatus", method = RequestMethod.POST)
	public ResponseEntity<?> updateEmploymentStatus(@RequestBody Map<String,Object> map) {
		try {
			List<Map<String, Object>> finalmap = null;
			boolean status = false;
			status = employeeleaveDispalyService.updateEmploymentStatus(map);
			if(status==true)
			{
				return new ResponseEntity<>("{\"status\":\"Employment Status has been updated\"}", HttpStatus.OK);
			}
			else
			{
				return new ResponseEntity<>("{\"status\":\"Employment Status has not been updated\"}",
						HttpStatus.NOT_ACCEPTABLE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while updating Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getStatusPeriods", method = RequestMethod.POST)
	public ResponseEntity<?> getStatusPeriods(@RequestBody Integer empId) {
		try {
			List finalmap = null;
			/*List list1 = null;
			List list2 = null;*/
			finalmap = employeeleaveDispalyService.getStatusPeriods(empId);
			/*list2=employeeleaveDispalyService.getEmpStatus();
			finalmap.add(list1);
			finalmap.add(list2);*/
			
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	
	@RequestMapping(value = "/getUserScreens", method = RequestMethod.POST)
	public ResponseEntity<?> getUserScreens (@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getUserScreens(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getseniorMangersList", method = RequestMethod.POST)
	public ResponseEntity<?> getseniorMangersList (@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getseniorMangersList(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getImmediateSeniorManger", method = RequestMethod.POST)
	public ResponseEntity<?> getImmediateSeniorManger (@RequestBody Integer empId) {
		try {
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getImmediateSeniorManger(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getConversionTypes", method = RequestMethod.GET)
	public ResponseEntity<?> getConversionTypes (HttpServletRequest request , HttpServletResponse response) {
		String token = request.getHeader("token");
		
		try {
			int empID = userDao.userListwithToken(token);
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getConversionTypes(empID);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}

	}
	
	@RequestMapping(value = "/getLoginNotification", method = RequestMethod.POST)
	public ResponseEntity<?> getLoginNotification (@RequestBody Integer empId) {
		try {
		Map<String, Object> finalmap = null;
			finalmap = employeeleaveDispalyService.getLoginNotification(empId);
			return new ResponseEntity<>(finalmap, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
					HttpStatus.BAD_REQUEST);

		}
	}
		@RequestMapping(value = "/getMonthPunchDetails", method = RequestMethod.POST)
		public ResponseEntity<?> getMonthPunchDetails (@RequestBody Map data) {
			try {
			List<Map<String, Object>> finalmap = null;
				finalmap = employeeleaveDispalyService.getpunchDetails(data);
				return new ResponseEntity<>(finalmap, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				Loggers.ERROR_LOGGER.error(e);
				return new ResponseEntity<>("{\"status\":\"Error occured while fetching Details \"}",
						HttpStatus.BAD_REQUEST);

			}
	}
	
	
	
		
	
	
	
	
	
	
	 

}
