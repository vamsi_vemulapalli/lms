package com.merilytics.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.merilytics.dto.FileUploadCommand;



@Controller
public class LeavePolicyDocController {
	@RequestMapping(value = "/centerSalesFileUpload", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> npsFileUpload(@ModelAttribute("fileUploadCmd") FileUploadCommand cmd,
			 @ModelAttribute("createdBy") String createdBy,
			@ModelAttribute("updatedBy") String updatedBy,HttpServletResponse response) throws Exception {
		//Integer commissionID = id;
		String createdBY = createdBy;
		String updatedBY = updatedBy;
		Map<String, Object> map = new HashMap<String, Object>();
		Context ctx = new InitialContext();
		Context env = (Context) ctx.lookup("java:comp/env");
		final String destDir = (String) env.lookup("uploadDirectory");
		File fileDirectory = null;
		MultipartFile uploadFile = null;
		String uploadFileName = null;
		String filePath = null;
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		int successCount = 0;
		List<Map<Object, Object>> successList = null;
		int uploadStatus;
		String recordsProcessed;

		// create Dest directory if not available
		fileDirectory = new File(destDir);
		if (!fileDirectory.exists()) {
			fileDirectory.mkdir();
		}

		// recive uploadedfile details
		uploadFile = cmd.getFileUploadCmd();
		uploadFileName = uploadFile.getOriginalFilename();
		inputStream = uploadFile.getInputStream();
		// create OutputStream pointing to Dest file on Server machine
		outputStream = new FileOutputStream(destDir+ uploadFileName);
		// checking the file type

		// complete fileUploading
		IOUtils.copy(inputStream, outputStream);

		// close streams
		inputStream.close();
		outputStream.close();

		// add uploaded filename to model attribute
		filePath = destDir + uploadFileName;


		return map;
	}
}
