package com.merilytics.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.service.MobileAPIService;
import com.merilytics.util.Loggers;

@Controller
public class MobileAPIController {
	@Autowired
	private MobileAPIService MobileAPIService;
	
	
	
	@RequestMapping(value = "/fetchUserPunchTime", method = RequestMethod.POST)
	public ResponseEntity<?> DeleteHolidays(@RequestBody Map<String, Object> input) {
		try {
			
			Map map = MobileAPIService.getPunchTime(input);
				return new ResponseEntity<>(map,
						HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error occured while deleting Details \"}",
					HttpStatus.BAD_REQUEST);

		}
	}
	
}
