package com.merilytics.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.merilytics.dao.UserListDAO;
import com.merilytics.service.OrgStrucutreService;
import com.merilytics.util.Loggers;

@Controller
public class OrgStrucutreController {

	@Resource
	private OrgStrucutreService orgStrucutreService;
	@Resource
	private UserListDAO dao;
	@RequestMapping(value = "/getEmployeeOrgStrucute", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeOrgStrucute(@RequestBody Integer dapID, HttpServletRequest request,
			HttpServletResponse response) {
		response.addHeader("token", request.getHeader("token"));
		Map<String, Object> status = null;
		int empID = 0;
		String token=request.getHeader("token");
		try {
			empID = dao.userListwithToken(token);
			status = orgStrucutreService.getEmployeeOrgStrucute(empID);
			Loggers.INFO_lOGGER.info(dapID + " " + status);
			return new ResponseEntity<>(status, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"org Structure is not avalible for this employee\"}",
					HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/getEmployeeOrgStrucuteforCEO", method = RequestMethod.POST)
	public ResponseEntity<?> getEmployeeOrgStrucuteforCEO(@RequestBody Integer empID, HttpServletRequest request,
			HttpServletResponse response) {
		//response.addHeader("token", request.getHeader("token"));
		Map<String, Object> status = null;
		try {
			status = orgStrucutreService.getEmployeeOrgStrucute( empID);
			Loggers.INFO_lOGGER.info( status);
			return new ResponseEntity<>(status, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"org Structure is not avalible for this employee\"}",
					HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/getEmployeeOrgStrucuteDropDown", method = RequestMethod.GET)
	public ResponseEntity<?> getEmployeeOrgStrucuteDropDown(HttpServletRequest request, HttpServletResponse response) {
		response.addHeader("token", request.getHeader("token"));
		List<Map<String, Object>> status = null;
		int empID = 0;
		String token=request.getHeader("token");
		try {
			empID = dao.userListwithToken(token);
			status = orgStrucutreService.getEmployeeOrgStrucuteDropDown(empID);
			Loggers.INFO_lOGGER.info(empID + " " + status);
			return new ResponseEntity<>(status, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			return new ResponseEntity<>("{\"status\":\"Error In StoredProc\"}", HttpStatus.BAD_REQUEST);
		}

	}

}
