package com.merilytics.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.merilytics.dao.UserListDAO;
import com.merilytics.mail.EmailTemplate2;

@Controller
public class TestController {
	
	@Resource
	private UserListDAO dao;
	
	@Resource
	private EmailTemplate2 EmailTemplate2;
	
	
	
	
		
	@RequestMapping(value="/getStaticMessages",method=RequestMethod.GET)
	public @ResponseBody Map getMessages(){
		EmailTemplate2.sendMail("sameer_sa", "test", "test");
		return dao.getStaticMessages();
	}
	
	
}//class
