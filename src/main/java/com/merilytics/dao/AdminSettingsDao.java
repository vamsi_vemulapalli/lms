package com.merilytics.dao;

import java.util.List;
import java.util.Map;

import com.merilytics.bo.EmploymentStatusBO;
import com.merilytics.bo.HolidaysBO;
import com.merilytics.bo.LeavesTypesBO;
import com.merilytics.bo.MobileCommentsBO;

public interface AdminSettingsDao {
	public List<Map<String, Object>> getStatusSettingsDetails() throws Exception;
	
	public List<Map<String, Object>> getUpcomingHolidaysList() throws Exception;

	public List<Map<String, Object>> getLeaveTypeDetails() throws Exception;

	public List<Map<String, Object>> getHolidays() throws Exception;

	public boolean SaveorUpdateLeaveSettings(LeavesTypesBO bo, Integer id) throws Exception;

	public boolean saveOrUpdateProbationAndNoticePeriod(EmploymentStatusBO bo, Integer Id) throws Exception;

	public boolean updateHolidays(HolidaysBO bo) throws Exception;

	public boolean saveHolidays(HolidaysBO bo) throws Exception;

	public boolean DeleteHolidays(HolidaysBO bo) throws Exception;

	public List<Map<String, Object>> tempMethod() throws Exception;
	
	public List<Map<String, Object>>  getAdminHistory(int startPos,int PAGE_SIZE,String search);
	public List<Map<String, Object>>  getAdminTeamHistory(int startPos,int PAGE_SIZE,String search);

	public Long getAdminTeamHistoryCount();

	boolean saveComments(MobileCommentsBO bo) throws Exception;
	

}
