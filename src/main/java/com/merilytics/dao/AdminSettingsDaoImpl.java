package com.merilytics.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.merilytics.bo.EmploymentStatusBO;
import com.merilytics.bo.HolidaysBO;
import com.merilytics.bo.LeavesTypesBO;
import com.merilytics.bo.MobileCommentsBO;
import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;

@Repository
public class AdminSettingsDaoImpl implements AdminSettingsDao {
	@Autowired()
	protected HibernateTemplate ht;

	@Override
	public List<Map<String, Object>> getStatusSettingsDetails() throws Exception {
		Session session = null;

		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("select * from tbl_static_employment_status_settings");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}
	
	@Override
	public List<Map<String, Object>> getUpcomingHolidaysList() throws Exception {
		Session session = null;

		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("EXEC LMS_sp_getUpcomingHolidaysList");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}
	
	
	@Override
	public List<Map<String, Object>> getLeaveTypeDetails() throws Exception {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("select * from tbl_static_leave_types ");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> tempMethod() throws Exception {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery(
				"select leaveTypeID,leaveType,totalLeavesAllowed,maxLeavesAllowedAtOnce,'lt' as status,createdBy "
						+ "from tbl_static_leave_types " + "union ALL "
						+ "select employmentStatusID,employmentStatus,[durationInMonths],null,'es' status,createdBy from tbl_static_employment_status_settings ");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getHolidays() throws Exception {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("select th.*,tsh.holidayType" + " from tbl_holidays th"
				+ " join tbl_static_holiday_types tsh" + " on tsh.holidayTypeID=th.holidayTypeID"

		);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			Loggers.INFO_lOGGER.info("Holidays Retreived");

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	@Transactional
	public boolean SaveorUpdateLeaveSettings(LeavesTypesBO bo, Integer Id) throws Exception {

		int idval = (Integer) bo.getLeaveTypeID();
		boolean flag = false;
		LeavesTypesBO leavesbo = null;
		try {
			leavesbo = ht.get(LeavesTypesBO.class, idval);
			leavesbo.setMaxLeavesAllowedAtOnce(bo.getMaxLeavesAllowedAtOnce());
			leavesbo.setTotalLeavesAllowed(bo.getTotalLeavesAllowed());
			leavesbo.setUpdatedBy(Id);
			/*
			 * leavesbo.setCreatedBy(bo.getCreatedBy());
			 * leavesbo.setCreatedDate(bo.getCreatedDate());
			 */
			leavesbo.setUpdatedDate(bo.getUpdatedDate());
			flag = true;
			Loggers.INFO_lOGGER.info(" Leaves Type  Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		}

		return flag;

	}

	@Override
	@Transactional
	public boolean saveOrUpdateProbationAndNoticePeriod(EmploymentStatusBO bo, Integer empId) throws Exception {
		boolean flag = false;
		EmploymentStatusBO empbo = null;
		int idval = (Integer) bo.getEmploymentStatusID();
		try {
			empbo = ht.get(EmploymentStatusBO.class, idval);
			empbo.setEmploymentStatus(bo.getEmploymentStatus());
			empbo.setUpdatedBy(empId);
			empbo.setDurationInMonths(bo.getDurationInMonths());
			empbo.setUpdatedDate(bo.getUpdatedDate());
			flag = true;
			Loggers.INFO_lOGGER.info("Probation Period Updated");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}

		return flag;
	}

	@Override
	@Transactional
	public boolean updateHolidays(HolidaysBO bo) throws Exception {
		boolean flag = false;
		HolidaysBO hbo = null;
		int val = (Integer) bo.getHolidayID();
		try {
			hbo = ht.get(HolidaysBO.class, val);
			hbo.setHolidayName(bo.getHolidayName());
			hbo.setHolidayTypeID(bo.getHolidayTypeID());
			hbo.setDayOfTheWeek(bo.getDayOfTheWeek());
			hbo.setHolidayDate(bo.getHolidayDate());
			hbo.setUpdatedDate(bo.getUpdatedDate());
			hbo.setUpdatedBy(bo.getUpdatedBy());
			flag = true;
			Loggers.INFO_lOGGER.info("Holidays Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;

	}

	@Override
	@Transactional
	public boolean DeleteHolidays(HolidaysBO bo) throws Exception {

		boolean flag = false;
		try {
			ht.delete(bo);
			Loggers.INFO_lOGGER.info("selected Holidays Deleted");
			flag = true;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;

	}

	@Override
	@Transactional
	public boolean saveHolidays(HolidaysBO bo) throws Exception {
		boolean flag = false;
		try {
			ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("Holidays saved");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;
	}

	@Override
	public List<Map<String, Object>> getAdminHistory(int startPos,int PAGE_SIZE,String search) {
		Session session = null;

		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("{call LMS_sp_EmployeeLeaveMessagesForAdminHistory (:startPos,:pageSize,:search)}");
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getAdminTeamHistory(int startPos,int PAGE_SIZE,String search) {
		Session session = null;

		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("{call LMS_sp_EmployeeRequestingLeaveMessagesForAdminTeam (:startPos,:pageSize,:search)}");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			query.setParameter("startPos", startPos);
			query.setParameter("pageSize", PAGE_SIZE);
			query.setParameter("search", search);
			/*query.setFirstResult(startPos);
			query.setMaxResults(PAGE_SIZE);*/
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public Long getAdminTeamHistoryCount() {
		Session session = null;
		Long count=null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("{call LMS_sp_EmployeeRequestingLeaveMessagesForAdminTeam (:startPos,:pageSize)}");
		query.setParameter("startPos", 0);
		query.setParameter("pageSize", 0);
		try {
			
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return (long) list.size();
	}
	
	@Override
	@Transactional
	public boolean saveComments(MobileCommentsBO bo) throws Exception {
		boolean flag = false;
		try {
			ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("Holidays saved");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;
	}
}
