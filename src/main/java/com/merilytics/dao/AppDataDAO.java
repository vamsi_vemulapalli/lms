package com.merilytics.dao;

import java.util.List;
import java.util.Map;

public interface AppDataDAO {
	public List getApps(String userToken) throws Exception;

	public List<Map<String, Object>> getLMSMTDReports(int empId) throws Exception;

	public List<Map<String, Object>> getLMSSILOReports(int empId) throws Exception;

	public List<Map<String, Object>> getLMSYTDReports(int empId) throws Exception;
}
