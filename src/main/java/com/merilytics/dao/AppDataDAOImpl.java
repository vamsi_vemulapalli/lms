package com.merilytics.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;
@Named
public class AppDataDAOImpl implements AppDataDAO {
	@Resource
	private HibernateTemplate LMSauthenticationTemplate;
	@Override
	public List getApps(String userToken) throws Exception {

		List list = null;
		Session session = HibernateUtil.getSession(LMSauthenticationTemplate);
		try {
			NativeQuery query = session.createNativeQuery("select * from employee.tbl_user_Applications");
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

			
				list = query.getResultList();

			return list;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	
	
	@Override
	public List<Map<String, Object>>  getLMSMTDReports(int  empId) throws Exception {

		List list = null;
		Session session = HibernateUtil.getSession(LMSauthenticationTemplate);
		try {
			NativeQuery query = session.createNativeQuery("EXEC EMPDB_sp_lmsSILO_MTDReport :employeeID");
			query.setParameter("employeeID", empId);
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

			
				list = query.getResultList();

			return list;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	@Override
	public List<Map<String, Object>>  getLMSYTDReports(int  empId) throws Exception {

		List list = null;
		Session session = HibernateUtil.getSession(LMSauthenticationTemplate);
		try {
			NativeQuery query = session.createNativeQuery("EXEC EMPDB_sp_lmsSILO_YTDReport :employeeID");
			query.setParameter("employeeID", empId);
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

			
				list = query.getResultList();

			return list;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	@Override
	public List<Map<String, Object>>  getLMSSILOReports(int  empId) throws Exception {

		List list = null;
		Session session = HibernateUtil.getSession(LMSauthenticationTemplate);
		try {
			NativeQuery query = session.createNativeQuery("EXEC EMPDB_sp_lmsSILOCurrentLeavesReport :employeeID");
			query.setParameter("employeeID", empId);
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

			
				list = query.getResultList();

			return list;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

}
