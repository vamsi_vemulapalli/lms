package com.merilytics.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.merilytics.bo.EmployeeLeaveTransactionsBO;
import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;


public interface ApplyLeaveDao {
	public Map<String, Object> saveLeaveDetails(EmployeeLeaveTransactionsBO bo) throws Exception;
	public Map<String, Object> updateLeaveDetails(EmployeeLeaveTransactionsBO bo) throws Exception;
	public List<Map<String, Object>> getDropdownLeaves(Integer empId );
	public List<Map<String, Object>> getSession();
	public List<Map<String, Object>> getHolidayType();
	public boolean saveLeaveEmailRecipients(LeaveEmailRecipientsTransactionsBO bo);
	public boolean approveLeaveRequest(EmployeeLeaveTransactionsBO bo);
	public Double countNoOfDays(Date sdate,Date edate,Integer startSession,Integer endSession,Integer leaveTypeID);
	public boolean convertFromUnpaidToPaid(EmployeeLeaveTransactionsBO bo) throws Exception;
	public Integer getTheAvailableLeaveType(int empID,int leaveID, int approvedOrRejectedBY) throws Exception;
	public boolean approveUnpaidToPaid(EmployeeLeaveTransactionsBO bo) throws Exception;
	public boolean rejectUnpaidToPaid(EmployeeLeaveTransactionsBO bo) throws Exception;
	/*public boolean rejectLeaveRequest(EmployeeLeaveTransactionsBO bo);
*/
	public Integer saveWorkFromHomeDetails(WorkFromHomeTransactionsBO bo) throws Exception ;
	public boolean updateEmailReciepienst(int leaveID, int WFHID) throws Exception;
	public boolean deleteLeaveDetails(Integer leaveID) throws Exception;
	
	public void changeCompOffStatus(Integer empID) throws Exception;
}
