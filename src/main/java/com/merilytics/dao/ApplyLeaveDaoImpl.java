package com.merilytics.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.merilytics.bo.EmployeeLeaveTransactionsBO;
import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;
import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;

@Repository
public class ApplyLeaveDaoImpl implements ApplyLeaveDao {
	@Autowired
	protected HibernateTemplate ht;

	@Override
	@Transactional
	public Map<String, Object> saveLeaveDetails(EmployeeLeaveTransactionsBO bo) throws Exception {

		boolean flag = false;
		int leaveId = 0;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			leaveId = (Integer) ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("Leaves Details saved");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		map.put("status", flag);
		map.put("id", leaveId);

		return map;
	}

	@Override
	@Transactional
	public Map<String, Object> updateLeaveDetails(EmployeeLeaveTransactionsBO bo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = false;
		EmployeeLeaveTransactionsBO empbo = null;
		int val = (Integer) bo.getLeaveID();

		try {
			empbo = ht.get(EmployeeLeaveTransactionsBO.class, val);
			empbo.setEmployeeID(bo.getEmployeeID());
			empbo.setLeaveType(bo.getLeaveType());
			empbo.setNoOfDays(bo.getNoOfDays());
			empbo.setReasonForLeave(bo.getReasonForLeave());
			empbo.setLeaveStartDate(bo.getLeaveStartDate());
			empbo.setLeaveEndDate(bo.getLeaveEndDate());
			empbo.setLeaveStatus(bo.getLeaveStatus());
			empbo.setSessionOfStartDate(bo.getSessionOfStartDate());
			empbo.setSessionOfEndDate(bo.getSessionOfEndDate());
			empbo.setUpdatedBy(bo.getUpdatedBy());
			empbo.setUpdatedDate(bo.getUpdatedDate());
			empbo.setIsAutomated(bo.getIsAutomated());
			empbo.setIsApproveSLT(bo.getIsApproveSLT());
			flag = true;
			Loggers.INFO_lOGGER.info("leaves Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		map.put("status", flag);
		map.put("id", val);
		return map;
	}

	@Override
	public List<Map<String, Object>> getDropdownLeaves(Integer empId) {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC LMS_sp_displayLeaveTypesBasedOnEmployeeGender :empId ");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return list;

	}

	@Override
	public List<Map<String, Object>> getSession() {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("Exec LMS_sp_GetStaticDaySessions");

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return list;
	}

	@Override
	public List<Map<String, Object>> getHolidayType() {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("select holidayTypeID,holidayType from .tbl_static_holiday_types");

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return list;
	}

	@Override
	@Transactional
	public boolean saveLeaveEmailRecipients(LeaveEmailRecipientsTransactionsBO bo) {
		boolean flag = false;
		try {
			ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("Team Details saved");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;
	}

	@Override
	@Transactional
	public boolean approveLeaveRequest(EmployeeLeaveTransactionsBO bo) {

		EmployeeLeaveTransactionsBO etbo = new EmployeeLeaveTransactionsBO();

		int val = (Integer) bo.getLeaveID();
		boolean flag = false;

		try {
			etbo = ht.get(EmployeeLeaveTransactionsBO.class, val);

		   
			etbo.setApprovedORRejectionDate(bo.getApprovedORRejectionDate());
			 etbo.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			etbo.setLeaveStatus(bo.getLeaveStatus());
			etbo.setUpdatedBy(bo.getApprovedByORRejectedBy());
			etbo.setUpdatedDate(new Timestamp(new Date().getTime()));
			etbo.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			flag = true;
			Loggers.INFO_lOGGER.info("leaves Aprroved");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;
	}

	@Override
	public Double countNoOfDays(Date sdate, Date edate, Integer startSession, Integer endSession, Integer leaveTypeID) {

		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		Double days = 0.0;
		List list = null;

		try {
			query = session.createNativeQuery(
					"exec [dbo].[LMS_sp_excludeWeekendsAndHolidays]  :sdate, :edate, :startSession, :endSession, :leaveTypeID");
			query.setParameter("sdate", sdate);
			query.setParameter("edate", edate);
			query.setParameter("startSession", startSession);
			query.setParameter("endSession", endSession);
			query.setParameter("leaveTypeID", leaveTypeID);

			list = query.getResultList();
			/*
			 * float var=(float) list.get(0); System.out.println(var+"var");
			 */
			days = (Double) list.get(0);

			// System.out.println(days);
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return days;
	}

	@Override
	@Transactional
	public boolean convertFromUnpaidToPaid(EmployeeLeaveTransactionsBO bo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = false;
		EmployeeLeaveTransactionsBO empbo = null;
		int val = (Integer) bo.getLeaveID();

		try {
			empbo = ht.get(EmployeeLeaveTransactionsBO.class, val);
			// empbo.setEmployeeID(bo.getEmployeeID());
			if(bo.getSessionOfEndDate()!=null) {
				empbo.setSessionOfEndDate(bo.getSessionOfEndDate());
			}
			if(bo.getSessionOfStartDate()!=null) {
				empbo.setSessionOfStartDate(bo.getSessionOfStartDate());
			}
			if(bo.getNoOfDays()<1.0&&bo.getNoOfDays()!=0.0) {
				empbo.setNoOfDays(bo.getNoOfDays());
			}
			empbo.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			empbo.setLeaveStatus(bo.getLeaveStatus());
			empbo.setUpdatedBy(bo.getUpdatedBy());
			empbo.setUpdatedDate(bo.getUpdatedDate());
			empbo.setCreatedDate(bo.getCreatedDate());
			empbo.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());

			flag = true;
			Loggers.INFO_lOGGER.info("leaves Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		map.put("status", flag);
		map.put("id", val);
		System.out.println("map"+map);
		return flag;
	}

	@Override
	public Integer getTheAvailableLeaveType(int empID, int leaveID, int approvedOrRejectedBY) throws Exception {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Integer> list = null;

		query = session.createNativeQuery("Exec LMS_sp_currentLeaveBalanceCheck :empID,:leaveID,:approvedOrRejectedBY");

		try {
			query.setParameter("empID", empID);
			query.setParameter("leaveID", leaveID);
			query.setParameter("approvedOrRejectedBY", approvedOrRejectedBY);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		if (list != null && list.size() != 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	@Override
	@Transactional
	public boolean approveUnpaidToPaid(EmployeeLeaveTransactionsBO bo) throws Exception {
		boolean flag = false;
		EmployeeLeaveTransactionsBO empbo = null;
		int val = (Integer) bo.getLeaveID();

		try {
			empbo = ht.get(EmployeeLeaveTransactionsBO.class, val);
			empbo.setLeaveStatus(bo.getLeaveStatus());

			empbo.setLeaveType(bo.getLeaveType());
			empbo.setUpdatedBy(bo.getUpdatedBy());
			empbo.setUpdatedDate(bo.getUpdatedDate());
			empbo.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			empbo.setApprovedORRejectionDate(bo.getApprovedORRejectionDate());
			flag = true;
			Loggers.INFO_lOGGER.info("leaves Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}

		return flag;
	}

	@Override
	@Transactional
	public boolean rejectUnpaidToPaid(EmployeeLeaveTransactionsBO bo) throws Exception {
		boolean flag = false;
		EmployeeLeaveTransactionsBO empbo = null;
		int val = (Integer) bo.getLeaveID();

		try {
			empbo = ht.get(EmployeeLeaveTransactionsBO.class, val);
			empbo.setLeaveStatus(bo.getLeaveStatus());
			empbo.setLeaveType(bo.getLeaveType());
			empbo.setBiometricIssueReasonForRejectionOrRevoke(bo.getBiometricIssueReasonForRejectionOrRevoke());
			//empbo.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			empbo.setUpdatedBy(bo.getUpdatedBy());
			empbo.setUpdatedDate(bo.getUpdatedDate());
			empbo.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			empbo.setApprovedORRejectionDate(bo.getApprovedORRejectionDate());
			flag = true;
			Loggers.INFO_lOGGER.info("leaves Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}

		return flag;
	}

	@Override
	@Transactional
	public Integer saveWorkFromHomeDetails(WorkFromHomeTransactionsBO bo) throws Exception {
		boolean flag = false;
		Integer id = null;
		try {
			id = (Integer) ht.save(bo);
		} catch (Exception e) {

			e.printStackTrace();
			throw e;

		}

		return id;
	}

	@Override
	public boolean updateEmailReciepienst(int leaveID, int WFHID) throws Exception {
		Session session = null;
		SessionFactory sf = null;
		Transaction tx = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Integer> list = null;
		boolean flag = false;

		query = session.createNativeQuery(
				"update tbl_leave_email_recipients_transactions set workFromHomeID=:WFHID,leaveID=:leaveID1 where leaveID=:leaveID2");

		try {
			query.setParameter("WFHID", WFHID);
			query.setParameter("leaveID1", null);
			query.setParameter("leaveID2", leaveID);
			tx = session.beginTransaction();
			query.executeUpdate();
			tx.commit();
			flag = true;
		} catch (Exception e) {
			tx.rollback();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return flag;
	}

	@Override
	@Transactional
	public boolean deleteLeaveDetails(Integer leaveID) throws Exception {
		boolean flag = false;
		EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
		bo.setLeaveID(leaveID);
		Integer id = null;
		try {
			ht.delete(bo);
			flag = true;
		} catch (Exception e) {

			e.printStackTrace();
			throw e;

		}

		return flag;
	}

	@Override
	public void changeCompOffStatus(Integer empID) throws Exception {
		Session session = null;

		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("EXEC LMS_sp_changeStatusForApprovedCompoff :empId ");
		query.setParameter("empId", empID);
		try {
			query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return;

	}

	/*
	 * @Override
	 * 
	 * @Transactional public boolean
	 * rejectLeaveRequest(EmployeeLeaveTransactionsBO bo) {
	 * EmployeeLeaveTransactionsBO etbo = new EmployeeLeaveTransactionsBO(); int
	 * val = (Integer) bo.getLeaveID(); boolean flag = false;
	 * 
	 * try { etbo = ht.get(EmployeeLeaveTransactionsBO.class, val);
	 * 
	 * etbo.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
	 * etbo.setApprovedORRejectionDate(bo.getApprovedORRejectionDate());
	 * etbo.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
	 * etbo.setUpdatedBy(bo.getApprovedByORRejectedBy());
	 * etbo.setLeaveStatus(bo.getLeaveStatus()); etbo.setUpdatedDate(new
	 * Timestamp(new Date().getTime()));
	 * 
	 * flag = true; Loggers.INFO_lOGGER.info("leaves Rejected"); } catch
	 * (Exception e) { e.printStackTrace(); Loggers.ERROR_LOGGER.error(e); throw
	 * e; } return flag; }
	 */
}
