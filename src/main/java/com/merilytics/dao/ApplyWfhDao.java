package com.merilytics.dao;

import java.util.Date;
import java.util.Map;

import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;

public interface ApplyWfhDao {
	public Map<String, Object> saveWfhDetails (WorkFromHomeTransactionsBO bo) throws Exception;
	public Map<String, Object> updateWfhDetails(WorkFromHomeTransactionsBO bo) throws Exception;
	public boolean saveLeaveEmailRecipients(LeaveEmailRecipientsTransactionsBO bo);
	
	public boolean approveWFH(WorkFromHomeTransactionsBO bo);
	public Double countNoOfDays(Date sdate,Date edate,Integer startSession,Integer endSession,Integer leaveTypeID);

}
