package com.merilytics.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;
import com.merilytics.util.Loggers;
@Repository
public class ApplyWfhDaoImpl implements ApplyWfhDao {
	
	@Autowired
	protected HibernateTemplate ht;
	
	@Override
	@Transactional
	public Map<String, Object> saveWfhDetails(WorkFromHomeTransactionsBO bo) throws Exception {
		int leaveId=0;
		Map<String, Object> map=new HashMap<String,Object>();
		boolean flag = false;
		try {
			leaveId=(Integer)ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("wfh Details saved");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		map.put("status", flag);
		map.put("id", leaveId);
	
		return map;
	}

	@Override
	@Transactional
	public Map<String, Object> updateWfhDetails(WorkFromHomeTransactionsBO bo) throws Exception {
		Map<String, Object> map=new HashMap<String,Object>();
		boolean flag=false;
		WorkFromHomeTransactionsBO wfhbo=null;
		int val=(Integer) bo.getWorkFromHomeID();
		
		try {
			wfhbo = ht.get(WorkFromHomeTransactionsBO.class, val);
			wfhbo.setWorkFromHomeID(bo.getWorkFromHomeID());
			wfhbo.setEmployeeID(bo.getEmployeeID());
			wfhbo.setNoOfDays(bo.getNoOfDays());
			wfhbo.setReasonForWorkFromHome(bo.getReasonForWorkFromHome());
			wfhbo.setStartDate(bo.getStartDate());
			wfhbo.setSessionOfStartDate(bo.getSessionOfStartDate());
			wfhbo.setEndDate(bo.getEndDate());
			wfhbo.setSessionOfEndDate(bo.getSessionOfEndDate());
			wfhbo.setWorkFromHomeStatus(bo.getWorkFromHomeStatus());
			wfhbo.setUpdatedDate(bo.getUpdatedDate());
			wfhbo.setUpdatedBy(bo.getUpdatedBy());
			wfhbo.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			wfhbo.setIsApproveSLT(bo.getIsApproveSLT());
			wfhbo.setIsAutomated(bo.getIsAutomated());
			
			flag = true;
			Loggers.INFO_lOGGER.info("leaves Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		map.put("status", flag);
		map.put("id", val);
		return map;
	}
	
	@Override
	@Transactional
	public boolean saveLeaveEmailRecipients(LeaveEmailRecipientsTransactionsBO bo) {
		boolean flag = false;
		try {
			ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("Team Details saved");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;
	}

	@Override
	@Transactional
	public boolean approveWFH(WorkFromHomeTransactionsBO bo) {
		WorkFromHomeTransactionsBO loadedBO = null;
		boolean flag = false;

		try {
			loadedBO = ht.get(WorkFromHomeTransactionsBO.class, bo.getWorkFromHomeID());
			loadedBO.setWorkFromHomeStatus(bo.getWorkFromHomeStatus());
			loadedBO.setUpdatedBy(bo.getUpdatedBy());
			loadedBO.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			loadedBO.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			loadedBO.setApprovedORRejectionDate(bo.getApprovedORRejectionDate());
			loadedBO.setUpdatedDate(bo.getUpdatedDate());
			 flag = true;
			Loggers.INFO_lOGGER.info("leaves Aprroved");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;

	}
	@Override
	public Double countNoOfDays(Date sdate, Date edate,Integer startSession,Integer endSession,Integer leaveTypeID) {
		
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		Double days=0.0;
		List list = null;

		try {
			query = session.createNativeQuery("exec [dbo].[LMS_sp_excludeWeekendsAndHolidays]  :sdate, :edate, :startSession, :endSession, :leaveTypeID");
			query.setParameter("sdate", sdate);
			query.setParameter("edate", edate);
			query.setParameter("startSession", startSession);
			query.setParameter("endSession", endSession);
			query.setParameter("leaveTypeID", leaveTypeID);
			
			
			
			list = query.getResultList();
			days=(Double)list.get(0);
			
		
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return days;
	}


	

}
