package com.merilytics.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.merilytics.bo.CompOffTransactionsBO;
import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;

public interface CompOffCreditDao {
	public  Map<String, Object> saveCompOffDetails(CompOffTransactionsBO bo) throws Exception;
	public  Map<String, Object> updateCompOffDetails(CompOffTransactionsBO bo) throws Exception;
	public boolean saveLeaveEmailRecipients(LeaveEmailRecipientsTransactionsBO bo);
	public boolean approveConmOff(CompOffTransactionsBO bo);
	public Double countNoOfDays(Date sdate,Date edate,Integer startSession,Integer endSession);
	public boolean saveCompOffData(List data, List emailRecipients);

}
