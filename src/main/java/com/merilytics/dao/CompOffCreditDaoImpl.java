package com.merilytics.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.merilytics.bo.CompOffTransactionsBO;
import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.util.Loggers;

@Repository
public class CompOffCreditDaoImpl implements CompOffCreditDao {

	@Autowired
	protected HibernateTemplate ht;
	
	

	@Override
	@Transactional
	public  Map<String, Object>  saveCompOffDetails(CompOffTransactionsBO bo) throws Exception {
		
		Map<String, Object> map=new HashMap<String,Object>();
		int leaveId=0;
		boolean flag = false;
		try {
			 leaveId=(Integer)ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("compOff Details saved");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		map.put("status", flag);
		map.put("id", leaveId);
		
		return map;
	}


	@Override
	@Transactional
	public Map<String, Object> updateCompOffDetails(CompOffTransactionsBO bo) throws Exception {
		Map<String, Object> map=new HashMap<String,Object>();
		boolean flag=false;
		CompOffTransactionsBO cmpbo=null;
		int val=(Integer)bo.getCompOffID();
		
		try {
			cmpbo = ht.get(CompOffTransactionsBO.class, val);
			cmpbo.setEmployeeID(bo.getEmployeeID());
			cmpbo.setCompOffID(bo.getCompOffID());
			//cmpbo.setCompOffStartDate(bo.getCompOffStartDate());
			//cmpbo.setCompOffEndDate(bo.getCompOffEndDate());
			//cmpbo.setSessionOfStartDate(bo.getSessionOfStartDate());
			//cmpbo.setSessionOfEndDate(bo.getSessionOfEndDate());
			cmpbo.setCompOffStatus(bo.getCompOffStatus());
			cmpbo.setCompOffReason(bo.getCompOffReason());
			//cmpbo.setNoofdays(bo.getNoofdays());
			cmpbo.setUpdatedDate(bo.getUpdatedDate());
			cmpbo.setUpdatedBy(bo.getUpdatedBy());
			
		
			flag = true;
			Loggers.INFO_lOGGER.info("compOffs Updated");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		
		map.put("status", flag);
		map.put("id", val);
		return map;
	}
	
	@Override
	@Transactional
	public boolean saveLeaveEmailRecipients(LeaveEmailRecipientsTransactionsBO bo) {
		boolean flag = false;
		try {
			ht.save(bo);
			flag = true;
			Loggers.INFO_lOGGER.info("Team Details saved");
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;
	}


	
	@Override
	@Transactional
	public boolean approveConmOff(CompOffTransactionsBO bo) {
		CompOffTransactionsBO loadedBO = null;
		boolean flag = false;

		try {
			loadedBO = ht.get(CompOffTransactionsBO.class, bo.getCompOffID());
			loadedBO.setCompOffStatus(bo.getCompOffStatus());
			loadedBO.setUpdatedBy(bo.getUpdatedBy());
			loadedBO.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			loadedBO.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			loadedBO.setUpdatedDate(bo.getUpdatedDate());
			 flag = true;
			Loggers.INFO_lOGGER.info("leaves Aprroved");
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		}
		return flag;

	}


	@Override
	public Double countNoOfDays(Date sdate, Date edate, Integer startSession, Integer endSession) {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		Double days=0.0;
		List list = null;

		try {
			query = session.createNativeQuery("exec [dbo].[LMS_sp_excludeWeekendsAndHolidays]  :sdate, :edate, :startSession, :endSession");
			query.setParameter("sdate", sdate);
			query.setParameter("edate", edate);
			query.setParameter("startSession", startSession);
			query.setParameter("endSession", endSession);
			
			
			
			list = query.getResultList();
			days=(Double)list.get(0);
			
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return days;
	}


	@Override
	public boolean saveCompOffData(List data, List emailRecipients) {
		List<Boolean> list = null;
		boolean flag= false;

		SessionFactory sf = ht.getSessionFactory();
		Session session = sf.openSession();
		Gson gs = new Gson();
		/*System.out.println(	gs.toJson(data));
		System.out.println(	gs.toJson(emailRecipients));*/
	
		NativeQuery query = session
				.createNativeQuery("{call LMS_sp_insertCompOff (:id,:mail)}");
		query.setParameter("id", gs.toJson(data));
		query.setParameter("mail", gs.toJson(emailRecipients));

		try {
			//query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			flag=list.get(0);

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.INFO_lOGGER.error("cfhguklhghf" + e);
		} finally {
			session.close();
		}
		return flag;
	}
	
}
