package com.merilytics.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;

@Repository
public class EmployeeDetailsDaoImpl implements EmployeeDetailsDao {
	@Autowired
	protected HibernateTemplate ht;

	@Override
	public List<Map<String, Object>> getEmployeeDetails() {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("LMS_sp_employeeDetailsDisplay");

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}

		return list;
	}

	@Override
	public List<Map<String, Object>> getEmployee(Integer empId) {
		
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC LMS_sp_individualEmployeeDetailsDisplay :empId");
		query.setParameter("empId", empId);
		
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
    // System.out.println("indao "+list);
		return list;
	}

	@Override
	public List<Map<String, Object>> getCurrentLeaveBalance(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		System.out.println("in proc"+empId);
		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeCurrentLeaveBalance] :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getEmployeeTeamDetails(Integer empId,int startPos,int PAGE_SIZE,String search) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesTeamRequesting]  :empId,:startPos,:pageSize,:search");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getEmployeeTeamDetailsMobileApproval(Integer empId,int startPos,int PAGE_SIZE,String search,Integer filter) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesTeamRequesting_mobApproval]  :empId,:startPos,:pageSize,:search,:filter");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		query.setParameter("filter", filter);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getEmployeeTeamDetailsMobileReview(Integer empId,int startPos,int PAGE_SIZE,String search,Integer isCompleted) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesTeamRequesting_mobReview]  :empId,:startPos,:pageSize,:search,:isCompleted");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		query.setParameter("isCompleted", isCompleted);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getEmployeeMeDetails(Integer empId,int startPos,int PAGE_SIZE,String search) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesMeRequesting] :empId,:startPos,:pageSize,:search");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getEmployeeHistory(Integer empId,int startPos,int PAGE_SIZE,String search) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesMeHistory]  :empId,:startPos,:pageSize,:search");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getEmployeeHistoryMobileFilter(Integer empId,int startPos,int PAGE_SIZE,String search,String leaveType) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesMeHistoryMobile]  :empId,:startPos,:pageSize,:search,:leaveType");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		query.setParameter("leaveType", leaveType);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	
	@Override
	public List<Map<String, Object>> getleaeveReciepeints(Integer leaveId,String leaveType) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_getLeaveReciepients]  :leaveId,:leaveType");
		query.setParameter("leaveId", leaveId);
		query.setParameter("leaveType", leaveType);
		
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getleaeveComments(Integer leaveId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_getLeaveComments]  :leaveId");
		query.setParameter("leaveId", leaveId);
		
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getTeamHistory(Integer empId,int startPos,int PAGE_SIZE,String search) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesTeamHistory]  :empId,:startPos,:pageSize,:search");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getTeamHistoryMobile(Integer empId,int startPos,int PAGE_SIZE,String search,String leaveType) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_individualEmployeeLeaveMessagesTeamHistoryMobile]  :empId,:startPos,:pageSize,:search,:leaveType");
		query.setParameter("empId", empId);
		query.setParameter("startPos", startPos);
		query.setParameter("pageSize", PAGE_SIZE);
		query.setParameter("search", search);
		query.setParameter("leaveType", leaveType);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> yearToDateStatus(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC LMS_sp_employeeYearToDateStatus  :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> monthToDateStatus(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC LMS_sp_employeeMonthToDateStatus  :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> monthToDateStatusMobile(Integer empId,Integer year,Integer month) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC LMS_sp_employeeMonthToDateStatus_Mobile  :empId,:year,:month");
		query.setParameter("empId", empId);
		query.setParameter("year", year);
		query.setParameter("month", month);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public Map<String, Object> checkAdmin(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_getEmployeeIsAdminList]   :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list.get(0);
	}

	@Override
	public List<Map<String, Object>> getUserScreensList(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC LMS_sp_getUserScreens   :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> paidLeaveHyperLinkMTD(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_paidLeaveHyperLinkMTD]    :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> paidLeaveHyperLinkYTD(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC [dbo].[LMS_sp_paidLeaveHyperLinkYTD]    :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public Map<String, Object> getManagerDetails(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> managerData=null;

		query = session.createNativeQuery("select pd1.EmployeeID,cast(isnull(p.firstName,'')+' '+isnull(p.middleName,'')+' '+isnull(p.LastName,'') as varchar(250)) as Name, cast(ld.email  as varchar(250)) as Email from EmployeeDB_Live.employee.tbl_professional_details pd1" 
 +" left join EmployeeDB_Live.employee.tbl_personal_details p on p.employeeID=pd1.employeeID"
 +" left join EmployeeDB_Live.employee.tbl_login_details ld on ld.employeeID=pd1.employeeID"
 +" where pd1.EmployeeID in (select pd.ManagerID from EmployeeDB_Live.employee.tbl_professional_details pd" 
 +" where pd.EmployeeID = :empID) "); 

		query.setParameter("empID", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			if(list!=null&&list.size()!=0){
				managerData=list.get(0);
			}
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return managerData;
	}
}
