package com.merilytics.dao;

import java.util.List;
import java.util.Map;

import com.merilytics.bo.CompOffTransactionsBO;
import com.merilytics.bo.EmployeeLeaveTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;

public interface EmployeeLeaveDisplayDao {
public List<Map<String, Object>>  getEmailRecipientsList(Integer empId);
public List<Map<String, Object>> getDefaultEmailRecipients(Integer empId);

public boolean cancelLeaveByEmployee(EmployeeLeaveTransactionsBO bo);
public boolean cancelWfhByEmployee(WorkFromHomeTransactionsBO bo);
public boolean cancelCompoffEmployee(CompOffTransactionsBO bo);

public List<Map<String, Object>> individualEmployeeCompOffCreditList(Integer empId);
public List<Map<String, Object>> getUnpaidLeaveHyperLinKYearToDate(Integer empId);
public List<Map<String, Object>> getUnpaidLeaveHyperLinKMonthToDate(Integer empId);
public List<Map<String, Object>> getcalendarDaysDisplay(Map<String,Object> map);
public List<Map<String, Object>> getHolidaysList(Map<String, Object> map);
public List<Map<String, Object>> getEmployeeStatusDisplay(Integer empId);
public boolean updateEmploymentStatus(Map<String, Object> map);
public List<Map<String, Object>> getStatusPeriods(Integer empId);
public List<Map<String, Object>> getUserScreens(Integer empId);
public List<Map<String, Object>> getseniorMangersList(Integer empId);
public List<Map<String, Object>> getImmediateSeniorManger(Integer empId);
public List<Map<String, Object>> getConversionTypes(Integer empID);
public Map<String, Object> getLoginNotification(Integer empId);
public List<Map<String, Object>> getEmployementStatus();
List<Map<String, Object>> getHolidaysListMobile(Map<String, Object> map);
List<Map<String, Object>> getpunchDetails(Map data);


}
