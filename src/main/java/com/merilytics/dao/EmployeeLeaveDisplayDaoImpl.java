package com.merilytics.dao;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.merilytics.bo.CompOffTransactionsBO;
import com.merilytics.bo.EmployeeLeaveTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;
import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;

@Repository
public class EmployeeLeaveDisplayDaoImpl implements EmployeeLeaveDisplayDao {
	@Autowired
	protected HibernateTemplate ht;

	@Override
	public List<Map<String, Object>> getEmailRecipientsList(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery(" EXEC LMS_sp_getEmployeeList :empId ");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	@Transactional
	public boolean cancelLeaveByEmployee(EmployeeLeaveTransactionsBO bo) {

		EmployeeLeaveTransactionsBO loadedBO = null;

		boolean flag = false;
		try {
			loadedBO = ht.get(EmployeeLeaveTransactionsBO.class, bo.getLeaveID());
			loadedBO.setLeaveStatus(bo.getLeaveStatus());
			loadedBO.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			loadedBO.setUpdatedBy(bo.getEmployeeID());
			loadedBO.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			loadedBO.setUpdatedDate(new Timestamp(new Date().getTime()));
			/*
			 * session = HibernateUtil.getSession(ht);
			 * 
			 * session.getTransaction().begin();
			 * 
			 * int empid = bo.getEmployeeID(); int leaveid = bo.getLeaveID();
			 * int leavestatus = bo.getLeaveStatus();
			 * 
			 * NativeQuery query = null; query = session.createNativeQuery(
			 * "update tbl_employee_leave_transactions set leaveStatus=4 where employeeID=:empid and leaveId=:leaveid and leaveStatus=:leavestatus"
			 * ); query.setParameter("empid", empid);
			 * query.setParameter("leaveid", leaveid);
			 * query.setParameter("leavestatus", leavestatus);
			 * 
			 * query.executeUpdate();
			 */

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);

		}

		return flag;
	}

	@Override
	public List<Map<String, Object>> getDefaultEmailRecipients(Integer empId) {
		Session session = null;

		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec [dbo].[LMS_sp_emailRecipientsList]  :empId ");

		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	@Transactional
	public boolean cancelWfhByEmployee(WorkFromHomeTransactionsBO bo) {
		WorkFromHomeTransactionsBO loadedBO = null;

		boolean flag = false;
		try {
			loadedBO = ht.get(WorkFromHomeTransactionsBO.class, bo.getWorkFromHomeID());
			loadedBO.setWorkFromHomeStatus((bo.getWorkFromHomeStatus()));
			loadedBO.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			loadedBO.setUpdatedBy(bo.getEmployeeID());
			loadedBO.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			loadedBO.setUpdatedDate(new Timestamp(new Date().getTime()));

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);

		}

		return flag;
	}

	@Override
	@Transactional
	public boolean cancelCompoffEmployee(CompOffTransactionsBO bo) {
		CompOffTransactionsBO loadedBO = null;

		boolean flag = false;
		try {
			loadedBO = ht.get(CompOffTransactionsBO.class, bo.getCompOffID());
			loadedBO.setCompOffStatus(bo.getCompOffStatus());
			loadedBO.setReasonForRejectionORRevoke(bo.getReasonForRejectionORRevoke());
			loadedBO.setUpdatedBy(bo.getEmployeeID());
			loadedBO.setApprovedByORRejectedBy(bo.getApprovedByORRejectedBy());
			loadedBO.setUpdatedDate(new Timestamp(new Date().getTime()));

			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);

		}

		return flag;
	}

	@Override
	public List<Map<String, Object>> individualEmployeeCompOffCreditList(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_individualEmployeeCompOffCreditList  :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getUnpaidLeaveHyperLinKYearToDate(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_UnpaidLeaveHyperLinkYTD   :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getUnpaidLeaveHyperLinKMonthToDate(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_UnpaidLeaveHyperLinkMTD    :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getcalendarDaysDisplay(Map<String, Object> map) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		int year = (int) map.get("Year");
		int month = (int) map.get("Month");
		int empID = (int) map.get("empID");
		int isAdmin = (int) map.get("isAdmin");
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_calendarDaysDisplay :year , :month,:empID,:isAdmin");
		query.setParameter("year", year);
		query.setParameter("month", month);
		query.setParameter("empID", empID);
		query.setParameter("isAdmin", isAdmin);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getHolidaysList(Map<String, Object> map) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		int year = (int) map.get("Year");
		int month = (int) map.get("Month");
		int empID = (int) map.get("empID");
		int isAdmin = (int) map.get("isAdmin");
		Integer weekNumber=0;
		if(map.get("week_number")!=null){
			weekNumber=(int) map.get("week_number");
		}else{
			weekNumber=null;
		}
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session
				.createNativeQuery("exec LMS_sp_calendarAndTeamMemberLeavesDisplay :year , :month,:empID,:isAdmin,:weekNumber");
		query.setParameter("year", year);
		query.setParameter("month", month);
		query.setParameter("empID", empID);
		query.setParameter("isAdmin", isAdmin);
		query.setParameter("weekNumber", weekNumber);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getHolidaysListMobile(Map<String, Object> map) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		/*
		 * int year = (int) map.get("Year"); int month = (int) map.get("Month");
		 */
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		int empID = (int) map.get("empID");
		int isAdmin = (int) map.get("isAdmin");
		Date sDate = null;
		Date eDate = null;
		try {
			sDate = formatter.parse( (String) map.get("startDate"));
			 eDate = formatter.parse( (String) map.get("endDate"));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Integer weekNumber=0;
		if(map.get("week_number")!=null){
			weekNumber=(int) map.get("week_number");
		}else{
			weekNumber=null;
		}
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session
				.createNativeQuery("exec LMS_sp_calendarAndTeamMemberLeavesDisplay_mobile :sDate , :eDate,:empID,:isAdmin,:weekNumber");
		query.setParameter("sDate", sDate);
		query.setParameter("eDate", eDate);
		query.setParameter("empID", empID);
		query.setParameter("isAdmin", isAdmin);
		query.setParameter("weekNumber", weekNumber);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getEmployeeStatusDisplay(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec [dbo].[LMS_sp_employementStatus]     :empId");
		query.setParameter("empId", empId);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override

	public boolean updateEmploymentStatus(Map<String, Object> map) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		Transaction tx = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List list = null;
		// SimpleDateFormat d = new
		// SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		boolean status = false;
		int employeeID = (int) map.get("EmployeeID");
		int employmentStatusID = 0;
		if (map.get("employmentStatusID") == null) {

			employmentStatusID = 0;

		} else {
			employmentStatusID = Integer.parseInt(map.get("employmentStatusID").toString());
		}

		String startDate = (String) map.get("StatusStartDate");
		String endDate = (String) map.get("StatusEndDate");

		try {
			Date sDate = null;
			Date eDate = null;
			if (startDate != null) {
				sDate = formatter.parse(startDate);
			}
			if (startDate == null) {
				sDate = new Date();
			}
			if (endDate != null) {
				eDate = formatter.parse(endDate);
			}

			if (employmentStatusID == 1) {
				
				query = session.createNativeQuery(
						"exec [dbo].[LMS_sp_updateEmployeeStatus] :employeeID , :employmentStatusID,  :sDate,  :eDate");
				query.setParameter("employeeID", employeeID);
				query.setParameter("employmentStatusID", employmentStatusID);
				query.setParameter("sDate", null);
				query.setParameter("eDate", null);
				int count = 0;

				query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
				list = query.getResultList();
				/*
				 * try { tx = session.beginTransaction(); count =
				 * query.executeUpdate(); tx.commit(); } catch (Exception e) {
				 * tx.rollback(); }
				 */
				if (list.size() > 0) {
					status = true;
				} else {
					status = false;
				}

			} else if (map.get("employmentStatusID") == null) {
			/*	System.out.println("in2" + map.get("employmentStatusID"));*/
				employmentStatusID = 1;

				
				query = session.createNativeQuery("exec LMS_sp_updateEmployeeStatus :employeeID, :employmentStatusID, :sDate, :eDate");
				
				  query.setParameter("employeeID", employeeID);
				  query.setParameter("employmentStatusID", employmentStatusID);
				 
				query.setParameter("sDate", sDate);
				query.setParameter("eDate", null);

			/*	System.out.println(employeeID);
				System.out.println(employmentStatusID);
				System.out.println(sDate);*/

				query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
				list = query.getResultList();
				if (list.size() > 0) {
					status = true;
				} else {
					status = false;
				}

			}

			else {
			
				query = session.createNativeQuery(
						"exec LMS_sp_updateEmployeeStatus   :employeeID , :employmentStatusID,  :sDate,  :eDate");
				query.setParameter("employeeID", employeeID);
				query.setParameter("employmentStatusID", employmentStatusID);
				query.setParameter("sDate", sDate);
				query.setParameter("eDate", eDate);
			

				query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
				list = query.getResultList();
				if (list.size() > 0) {
					status = true;
				} else {
					status = false;
				}

			}
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();

		} finally {
			session.close();

		}
		return status;
	}

	@Override
	public List<Map<String, Object>> getStatusPeriods(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_employeeStatusDisplay :empId");
		query.setParameter("empId", empId);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			Loggers.INFO_lOGGER.info("Holidays Retreived");

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getUserScreens(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec  LMS_sp_getUserScreens  :empId");
		query.setParameter("empId", empId);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getseniorMangersList(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_seniorMangersList  :empId");
		query.setParameter("empId", empId);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getImmediateSeniorManger(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_immediateMangersList   :empId");
		query.setParameter("empId", empId);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getpunchDetails(Map data) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_EmployeePunchDetails   :empId , :type , :year , :month");
		query.setParameter("empId", data.get("empID"));
		query.setParameter("type", data.get("type"));
		query.setParameter("year", data.get("year"));
		query.setParameter("month", data.get("month"));

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}
	@Override
	public List<Map<String, Object>> getConversionTypes(Integer empID) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("EXEC LMS_sp_leaveConversionOptions  :employeeID");
		query.setParameter("employeeID", empID);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
		} finally {
			session.close();

		}
		return list;
		
	}
	@Override
	public List<Map<String, Object>> getEmployementStatus() {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("select employmentStatusID,employmentStatus,durationInMonths from tbl_static_employment_status_settings");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		
		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
		} finally {
			session.close();


		}
		return list;
		
	}


	@Override
	public Map<String, Object> getLoginNotification(Integer empId) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_immediateMangersList   :empId");
		query.setParameter("empId", empId);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			

		} catch (Exception e) {
			e.printStackTrace();
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list.get(0);
	}


}
