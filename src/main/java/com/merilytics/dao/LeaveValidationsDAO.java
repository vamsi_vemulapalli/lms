package com.merilytics.dao;

import java.util.Date;
import java.util.Map;

public interface LeaveValidationsDAO {
public Map<String,Object> validationCheckForLeave(int empID,double noOFDays,int leaveTypeID,Date eDate,Date sDate,Integer startSession,Integer endSession);
public Map<String,Object> validationCheckForCompOff(int empID,double noOFDays,Date sDate,Date eDate,Integer startSession,Integer endSession);
public Map<String, Object> validationCheckForWFH(int empId,Date sDate, Date eDate,Float days,Integer startSession,Integer endSession);
}
