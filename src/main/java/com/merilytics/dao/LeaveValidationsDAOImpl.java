package com.merilytics.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;
@Repository
public class LeaveValidationsDAOImpl implements LeaveValidationsDAO{
	@Autowired
	protected HibernateTemplate ht;
	@Override
	public Map<String, Object> validationCheckForLeave(int empID, double noOFDays, int leaveTypeID,Date eDate,Date sDate,Integer startSession,Integer endSession) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> map= null;
	
		query = session.createNativeQuery(" EXEC LMS_sp_employeeLeaveValidations :empId , :noOfDays,:leaveTypeID,:sDate,:eDate,:startSession,:endSession");
		query.setParameter("empId", empID);
		query.setParameter("noOfDays", noOFDays);
		query.setParameter("leaveTypeID", leaveTypeID);
		query.setParameter("sDate", sDate);
		query.setParameter("eDate", eDate);
		query.setParameter("startSession", startSession);
		query.setParameter("endSession", endSession);
		
		
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			if(list!=null){
				map=list.get(0);
			}

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return map;
	}
	@Override
	public Map<String, Object> validationCheckForCompOff(int empID, double noOFDays,Date sDate,Date eDate,Integer startSession,Integer endSession) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> map= null;
		//System.out.println(noOFDays+"   "+empID+"     "+sDate+"       "+eDate);
		query = session.createNativeQuery(" EXEC LMS_sp_compOffValidation :empId , :noOfDays,:sDate,:eDate,:startSession,:endSession");
		query.setParameter("empId", empID);
		query.setParameter("noOfDays", noOFDays);
		query.setParameter("sDate", sDate);
		query.setParameter("eDate", eDate);
		query.setParameter("startSession", startSession);
		query.setParameter("endSession", endSession);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			if(list!=null){
				map=list.get(0);
			}

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return map;
	}
	
	@Override
	public Map<String, Object> validationCheckForWFH(int empId,Date sDate,Date eDate,Float days,Integer startSession,Integer endSession) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> map= null;
			
		query = session.createNativeQuery("{call  dbo.LMS_sp_employeeWorkFromHomeValidations(:empId,:sDate,:eDate,:days,:startSession,:endSession)}");
		query.setParameter("empId", empId);
		query.setParameter("sDate", sDate);
		query.setParameter("eDate", eDate);
		query.setParameter("days", days);
		query.setParameter("startSession", startSession);
		query.setParameter("endSession", endSession);
		
		
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			
			if(list!=null){
				map=list.get(0);
			}

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return map;
	}

}
