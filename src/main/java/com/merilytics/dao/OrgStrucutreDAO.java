package com.merilytics.dao;

import java.util.List;
import java.util.Map;

public interface OrgStrucutreDAO {

	List<Map<String, Object>> getEmployeeOrgStrucute( int empID) throws Exception;

	public List<Map<String, Object>> getEmployeeOrgStrucuteDropDown(int empID ) throws Exception;

}
