package com.merilytics.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;

@Named
public class OrgStrucutreDAOImpl implements OrgStrucutreDAO {

	@Resource
	private HibernateTemplate ht;

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	@Override
	public List<Map<String, Object>> getEmployeeOrgStrucute( int empID) throws Exception {
		Session session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("LMS_sp_siloView :empID");

		query.setParameter("empID", empID);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			Loggers.INFO_lOGGER.info("Employee Organisation Structure for particular Dap");

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	@Override
	public List<Map<String, Object>> getEmployeeOrgStrucuteDropDown(int empID ) throws Exception {
		Session session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("exec LMS_sp_siloView  :empID");

		query.setParameter("empID", empID);

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;

		} finally {
			session.close();

		}
		return list;
	}

}
