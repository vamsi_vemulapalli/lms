package com.merilytics.dao;

import java.util.ArrayList;

import com.merilytics.bo.SystemParametersBO;

/**
 * @author 21258
 *
 */
public interface SystemParametersDAO {
	public ArrayList<SystemParametersBO> getSystemParameters();
}
