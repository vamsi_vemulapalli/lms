package com.merilytics.dao;

import java.util.ArrayList;

import javax.inject.Named;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.merilytics.bo.SystemParametersBO;

/**
 * @author 21258
 *
 */
@Named
public class SystemParametersDAOImpl implements SystemParametersDAO {
	@Autowired(required = true)
	private HibernateTemplate ht;

	@Override
	public ArrayList<SystemParametersBO> getSystemParameters() {

		return (ArrayList<SystemParametersBO>) ht.findByCriteria(
				DetachedCriteria.forClass(SystemParametersBO.class).add(Restrictions.eq("status", "active")));
	}

}
