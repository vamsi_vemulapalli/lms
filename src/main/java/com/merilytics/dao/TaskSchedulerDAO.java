package com.merilytics.dao;

import java.util.List;
import java.util.Map;

public interface TaskSchedulerDAO {
	public List<Map<String, Object>> getAutomationdetails() throws Exception;

	public Map<String, Object> getEmailCredentials();

	public List<Map<String, Object>> getSiloManager(int empID);
	
	public Map<String, Object> changeEmployeeMentStatusAuto();
}
