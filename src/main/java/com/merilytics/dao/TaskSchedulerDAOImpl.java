package com.merilytics.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.merilytics.util.Loggers;

@Repository
public class TaskSchedulerDAOImpl implements TaskSchedulerDAO {
	@Autowired
	protected HibernateTemplate ht;

	@Override
	public List<Map<String, Object>> getAutomationdetails() throws Exception {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Map<String, Object>> list = null;

		query = session.createNativeQuery("EXEC  AutomationLeaveCalculation");

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			throw e;
		} finally {
			session.close();

		}
		return list;
	}

	@Override
	public Map<String, Object> getEmailCredentials() {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("select * from tbl_system_parameters");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			// System.out.println(list);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();

		}
		System.out.println(list.size());
		return list.get(0);

	}

	@Override
	public List<Map<String, Object>> getSiloManager(int empID) {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("EXEC LMS_sp_emailRecipientsList :empId");
		query.setParameter("empId", empID);
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			// System.out.println(list);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();

		}
		return list;

	}

	@Override
	public Map<String, Object> changeEmployeeMentStatusAuto() {
		Session session = null;
		SessionFactory sf = null;
		sf = ht.getSessionFactory();
		session = sf.openSession();
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		query = session.createNativeQuery("EXEC  changeEmployeeMentStatus");
		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			// System.out.println(list);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();

		}
		return list.get(0);
	}

}
