package com.merilytics.dao;

import java.util.List;
import java.util.Map;

public interface UserListDAO {
	public Map getUsersList(String userToken, int empID) throws Exception;
	public int userListwithToken(String userToken) throws Exception;
	public List getApps(String userToken) throws Exception;
	public Map getStaticMessages();
	Map<String, Object> MobileAPIPunchTimings(Map data);
	

}
