package com.merilytics.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.merilytics.util.HibernateUtil;
import com.merilytics.util.Loggers;

@Named
public class UserListDAOImpl implements UserListDAO {

	@Resource
	private HibernateTemplate LMSauthenticationTemplate;

	@Resource
	private HibernateTemplate ht;

	@Override
	public Map getUsersList(String userToken, int empID) throws Exception {

		Map data = null;
		Session session = HibernateUtil.getSession(ht);
		try {
			NativeQuery query = session.createNativeQuery("SELECT * FROM tbl_DAP_employee WHERE ID=:empID");
			query.setParameter("empID", empID);

			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			data = (Map) query.getResultList().get(0);
			return data;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public int userListwithToken(String userToken) throws Exception {
		int id = 0;
		List<Integer> listID = null;
		Session session = HibernateUtil.getSession(LMSauthenticationTemplate);
		try {
			NativeQuery query = session
					.createNativeQuery("select employeeID from employee.tbl_login_details where userToken =:token");
			query.setParameter("token", userToken);
			Loggers.WARN_LOGGER.warn("UserDetails Not Valid");
			listID = query.getResultList();
			if (listID != null) {
				id = listID.get(0);
			}
			return id;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			return 0;
		} finally {
			session.close();
		}
	}

	@Override
	public List getApps(String userToken) throws Exception {

		List list = null;
		Session session = HibernateUtil.getSession(LMSauthenticationTemplate);
		try {
			NativeQuery query = session.createNativeQuery("select * from employee.tbl_user_Applications");
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

			list = query.getResultList();

			return list;
		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public Map getStaticMessages() {
		List<Map> list = null;
		Map setMap = null;
		Session session = null;
		try {
			setMap = new HashMap();

			session = HibernateUtil.getSession(ht);
			NativeQuery query = session
					.createNativeQuery("select umId,message_code,user_message from tbl_static_user_messages");
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();
			for (Map innerMap : list) {
				Map map = new HashMap();
				map.put("message_code", innerMap.get("message_code"));
				map.put("user_message", innerMap.get("user_message"));

				setMap.put(innerMap.get("umId"), map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return setMap;
	}

	@Override
	public Map<String, Object> MobileAPIPunchTimings(Map data) {
		Session session = null;
		session = HibernateUtil.getSession(ht);
		NativeQuery query = null;
		List<Map<String, Object>> list = null;
		Map<String, Object> map = null;

		query = session.createNativeQuery("{call  dbo.MobileApp_firstINandLasOut(:empId)}");
		query.setParameter("empId", data.get("empId"));

		try {
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			list = query.getResultList();

			if (list != null) {
				map = list.get(0);
			}

		} catch (Exception e) {
			Loggers.ERROR_LOGGER.error(e);
			e.printStackTrace();
			throw e;

		} finally {
			session.close();

		}
		return map;
	}
}
