package com.merilytics.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class CompOffTransactionsDTO implements Serializable  {
	private Integer compOffID;
	private Integer employeeID;
	private Timestamp compOffStartDate;
	private Timestamp compOffEndDate;
	private Integer sessionOfStartDate;
	private Integer sessionOfEndDate;
	 private String compOffReason;
	private Integer compOffStatus;
	private Integer approvedByORRejectedBy;
    private String reasonForRejectionORRevoke;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	private Double noofdays;
	
	public Integer getCompOffStatus() {
		return compOffStatus;
	}
	public Integer getCompOffID() {
		return compOffID;
	}
	public void setCompOffID(Integer compOffID) {
		this.compOffID = compOffID;
	}
	public Integer getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}
	
	public Integer getSessionOfStartDate() {
		return sessionOfStartDate;
	}
	public void setSessionOfStartDate(Integer sessionOfStartDate) {
		this.sessionOfStartDate = sessionOfStartDate;
	}
	public Integer getSessionOfEndDate() {
		return sessionOfEndDate;
	}
	public void setSessionOfEndDate(Integer sessionOfEndDate) {
		this.sessionOfEndDate = sessionOfEndDate;
	}
	
	public Integer getApprovedByORRejectedBy() {
		return approvedByORRejectedBy;
	}
	public void setApprovedByORRejectedBy(Integer approvedByORRejectedBy) {
		this.approvedByORRejectedBy = approvedByORRejectedBy;
	}
	public String getReasonForRejectionORRevoke() {
		return reasonForRejectionORRevoke;
	}
	public void setReasonForRejectionORRevoke(String reasonForRejectionORRevoke) {
		this.reasonForRejectionORRevoke = reasonForRejectionORRevoke;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer isCompOffStatus() {
		return compOffStatus;
	}
	public void setCompOffStatus(Integer compOffStatus) {
		this.compOffStatus = compOffStatus;
	}
	public String getCompOffReason() {
		return compOffReason;
	}
	public void setCompOffReason(String compOffReason) {
		this.compOffReason = compOffReason;
	}
	public Double getNoofdays() {
		return noofdays;
	}
	public void setNoofdays(Double noofdays) {
		this.noofdays = noofdays;
	}
	public Timestamp getCompOffStartDate() {
		return compOffStartDate;
	}
	public void setCompOffStartDate(Timestamp compOffStartDate) {
		this.compOffStartDate = compOffStartDate;
	}
	public Timestamp getCompOffEndDate() {
		return compOffEndDate;
	}
	public void setCompOffEndDate(Timestamp compOffEndDate) {
		this.compOffEndDate = compOffEndDate;
	}
	
}
