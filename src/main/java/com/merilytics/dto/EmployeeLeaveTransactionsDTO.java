package com.merilytics.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class EmployeeLeaveTransactionsDTO implements Serializable {
	private Integer leaveID;
	private Integer employeeID;
    private Integer	leaveType;
	private float noOfDays;
	private String reasonForLeave;
	private Timestamp leaveStartDate;
	private Timestamp 	leaveEndDate;
	private Integer leaveStatus;
	private Integer sessionOfStartDate;
	private Integer sessionOfEndDate;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	private Integer approvedByORRejectedBy;
	private Timestamp approvedORRejectionDate;
	private String  reasonForRejectionORRevoke;
	private Integer isAutomated;
	private Integer isApproveSLT;
	private String biometricIssueReasonForRejectionOrRevoke;
	
	
	public String getBiometricIssueReasonForRejectionOrRevoke() {
		return biometricIssueReasonForRejectionOrRevoke;
	}
	public void setBiometricIssueReasonForRejectionOrRevoke(String biometricIssueReasonForRejectionOrRevoke) {
		this.biometricIssueReasonForRejectionOrRevoke = biometricIssueReasonForRejectionOrRevoke;
	}
	public Integer getLeaveID() {
		return leaveID;
	}
	public void setLeaveID(Integer leaveID) {
		this.leaveID = leaveID;
	}
	public Integer getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}
	public Integer getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(Integer leaveType) {
		this.leaveType = leaveType;
	}
	
	public String getReasonForLeave() {
		return reasonForLeave;
	}
	public void setReasonForLeave(String reasonForLeave) {
		this.reasonForLeave = reasonForLeave;
	}
	
	public Integer getLeaveStatus() {
		return leaveStatus;
	}
	public void setLeaveStatus(Integer leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	public Integer getSessionOfStartDate() {
		return sessionOfStartDate;
	}
	public void setSessionOfStartDate(Integer sessionOfStartDate) {
		this.sessionOfStartDate = sessionOfStartDate;
	}
	public Integer getSessionOfEndDate() {
		return sessionOfEndDate;
	}
	public void setSessionOfEndDate(Integer sessionOfEndDate) {
		this.sessionOfEndDate = sessionOfEndDate;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getApprovedByORRejectedBy() {
		return approvedByORRejectedBy;
	}
	public void setApprovedByORRejectedBy(Integer approvedByORRejectedBy) {
		this.approvedByORRejectedBy = approvedByORRejectedBy;
	}
	
	public String getReasonForRejectionORRevoke() {
		return reasonForRejectionORRevoke;
	}
	public void setReasonForRejectionORRevoke(String reasonForRejectionORRevoke) {
		this.reasonForRejectionORRevoke = reasonForRejectionORRevoke;
	}
	public float getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(float noOfDays) {
		this.noOfDays = noOfDays;
	}
	public Timestamp getApprovedORRejectionDate() {
		return approvedORRejectionDate;
	}
	public void setApprovedORRejectionDate(Timestamp approvedORRejectionDate) {
		this.approvedORRejectionDate = approvedORRejectionDate;
	}
	public Timestamp getLeaveStartDate() {
		return leaveStartDate;
	}
	public void setLeaveStartDate(Timestamp leaveStartDate) {
		this.leaveStartDate = leaveStartDate;
	}
	public Timestamp getLeaveEndDate() {
		return leaveEndDate;
	}
	public void setLeaveEndDate(Timestamp leaveEndDate) {
		this.leaveEndDate = leaveEndDate;
	}
	public Integer getIsAutomated() {
		return isAutomated;
	}
	public void setIsAutomated(Integer isAutomated) {
		this.isAutomated = isAutomated;
	}
	public Integer getIsApproveSLT() {
		return isApproveSLT;
	}
	public void setIsApproveSLT(Integer isApproveSLT) {
		this.isApproveSLT = isApproveSLT;
	}
}