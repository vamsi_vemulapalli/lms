package com.merilytics.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class HolidaysDTO implements Serializable{
	private Integer holidayID;
	private String holidayName;
	private Integer holidayTypeID;
	private Date holidayDate;
	private String dayOfTheWeek;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	
	public String getHolidayName() {
		return holidayName;
	}
	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}
	
	public Date getHolidayDate() {
		return holidayDate;
	}
	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}
	public String getDayOfTheWeek() {
		return dayOfTheWeek;
	}
	public void setDayOfTheWeek(String dayOfTheWeek) {
		this.dayOfTheWeek = dayOfTheWeek;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getHolidayID() {
		return holidayID;
	}
	public void setHolidayID(Integer holidayID) {
		this.holidayID = holidayID;
	}
	public Integer getHolidayTypeID() {
		return holidayTypeID;
	}
	public void setHolidayTypeID(Integer holidayTypeID) {
		this.holidayTypeID = holidayTypeID;
	}
	
	
}
