package com.merilytics.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class LeaveEmailRecipientsTransactionsDTO implements Serializable {
	private Integer emailNo;
	private Integer teamMemberID;
	private Integer leaveID;
	private Integer compOffID;
	private Integer workFromHomeID;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	public Integer getEmailNo() {
		return emailNo;
	}
	public void setEmailNo(Integer emailNo) {
		this.emailNo = emailNo;
	}
	public Integer getTeamMemberID() {
		return teamMemberID;
	}
	public void setTeamMemberID(Integer teamMemberID) {
		this.teamMemberID = teamMemberID;
	}
	public Integer getLeaveID() {
		return leaveID;
	}
	public void setLeaveID(Integer leaveID) {
		this.leaveID = leaveID;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getCompOffID() {
		return compOffID;
	}
	public void setCompOffID(Integer compOffID) {
		this.compOffID = compOffID;
	}
	public Integer getWorkFromHomeID() {
		return workFromHomeID;
	}
	public void setWorkFromHomeID(Integer workFromHomeID) {
		this.workFromHomeID = workFromHomeID;
	}

}
