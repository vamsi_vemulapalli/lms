package com.merilytics.dto;

import java.io.Serializable;

public class LeaveStatusDTO implements Serializable {
	private Integer  leaveStatusID;
	private String leaveStatus;
	
	public String getLeaveStatus() {
		return leaveStatus;
	}
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	public Integer getLeaveStatusID() {
		return leaveStatusID;
	}
	public void setLeaveStatusID(Integer leaveStatusID) {
		this.leaveStatusID = leaveStatusID;
	}

}
