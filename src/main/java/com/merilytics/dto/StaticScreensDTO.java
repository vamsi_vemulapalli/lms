package com.merilytics.dto;

public class StaticScreensDTO {
	private Integer Screen_Id;
	private String  screen_name;
	private boolean status;
	private String  url;
	private String icon;
	private Integer parent_id;
	private Integer display_order;
	public Integer getScreen_Id() {
		return Screen_Id;
	}
	public void setScreen_Id(Integer screen_Id) {
		Screen_Id = screen_Id;
	}
	public String getScreen_name() {
		return screen_name;
	}
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Integer getParent_id() {
		return parent_id;
	}
	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}
	public Integer getDisplay_order() {
		return display_order;
	}
	public void setDisplay_order(Integer display_order) {
		this.display_order = display_order;
	}
}
