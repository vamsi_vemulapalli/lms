package com.merilytics.dto;

public class StaticUserMessagesDTO {
	private Integer umId;
	private String message_code;
	private String user_message;
	private String serviceName;
	public Integer getUmId() {
		return umId;
	}
	public void setUmId(Integer umId) {
		this.umId = umId;
	}
	public String getMessage_code() {
		return message_code;
	}
	public void setMessage_code(String message_code) {
		this.message_code = message_code;
	}
	public String getUser_message() {
		return user_message;
	}
	public void setUser_message(String user_message) {
		this.user_message = user_message;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
