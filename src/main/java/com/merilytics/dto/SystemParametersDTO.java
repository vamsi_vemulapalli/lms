package com.merilytics.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class SystemParametersDTO implements Serializable {
	private Integer ID;
	private String companyCode;
	private String	adminContactEmail;
	private String itSupportEmail;
	private String  itSupportPassword;
	public Integer	smtpPort;
	private String smtpServerIp;
	private Timestamp creationDate;
	private String status;
	private Timestamp updationDate;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getAdminContactEmail() {
		return adminContactEmail;
	}
	public void setAdminContactEmail(String adminContactEmail) {
		this.adminContactEmail = adminContactEmail;
	}
	public String getItSupportEmail() {
		return itSupportEmail;
	}
	public void setItSupportEmail(String itSupportEmail) {
		this.itSupportEmail = itSupportEmail;
	}
	public String getItSupportPassword() {
		return itSupportPassword;
	}
	public void setItSupportPassword(String itSupportPassword) {
		this.itSupportPassword = itSupportPassword;
	}
	public Integer getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getSmtpServerIp() {
		return smtpServerIp;
	}
	public void setSmtpServerIp(String smtpServerIp) {
		this.smtpServerIp = smtpServerIp;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getUpdationDate() {
		return updationDate;
	}
	public void setUpdationDate(Timestamp updationDate) {
		this.updationDate = updationDate;
	}
}
