package com.merilytics.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class WorkFromHomeTransactionsDTO implements Serializable  {
	private Integer workFromHomeID;
	private Integer employeeID;
	private float noOfDays;
	private String reasonForWorkFromHome;
	private Timestamp StartDate;
	private Integer sessionOfStartDate;
	private Timestamp EndDate;
	private Integer sessionOfEndDate;
	private Integer workFromHomeStatus;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	private Integer createdBy;
	private Integer updatedBy;
	private Integer approvedByORRejectedBy;
	private Date approvedORRejectionDate;
	private String  reasonForRejectionORRevoke;
	private Integer isApproveSLT;
	private Integer isAutomated;
	public Integer getIsAutomated() {
		return isAutomated;
	}
	public void setIsAutomated(Integer isAutomated) {
		this.isAutomated = isAutomated;
	}
	private Integer conversionTypeId;
	public Integer getConversionTypeId() {
		return conversionTypeId;
	}
	public void setConversionTypeId(Integer conversionTypeId) {
		this.conversionTypeId = conversionTypeId;
	}
	public Integer getIsApproveSLT() {
		return isApproveSLT;
	}
	public void setIsApproveSLT(Integer isApproveSLT) {
		this.isApproveSLT = isApproveSLT;
	}
	public Integer getWorkFromHomeID() {
		return workFromHomeID;
	}
	public void setWorkFromHomeID(Integer workFromHomeID) {
		this.workFromHomeID = workFromHomeID;
	}
	public Integer getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}
	
	public String getReasonForWorkFromHome() {
		return reasonForWorkFromHome;
	}
	public void setReasonForWorkFromHome(String reasonForWorkFromHome) {
		this.reasonForWorkFromHome = reasonForWorkFromHome;
	}

	public Integer getSessionOfStartDate() {
		return sessionOfStartDate;
	}
	public void setSessionOfStartDate(Integer sessionOfStartDate) {
		this.sessionOfStartDate = sessionOfStartDate;
	}
	
	public Integer getSessionOfEndDate() {
		return sessionOfEndDate;
	}
	public void setSessionOfEndDate(Integer sessionOfEndDate) {
		this.sessionOfEndDate = sessionOfEndDate;
	}
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getApprovedByORRejectedBy() {
		return approvedByORRejectedBy;
	}
	public void setApprovedByORRejectedBy(Integer approvedByORRejectedBy) {
		this.approvedByORRejectedBy = approvedByORRejectedBy;
	}
	public Date getApprovedORRejectionDate() {
		return approvedORRejectionDate;
	}
	public void setApprovedORRejectionDate(Date approvedORRejectionDate) {
		this.approvedORRejectionDate = approvedORRejectionDate;
	}
	public String getReasonForRejectionORRevoke() {
		return reasonForRejectionORRevoke;
	}
	public void setReasonForRejectionORRevoke(String reasonForRejectionORRevoke) {
		this.reasonForRejectionORRevoke = reasonForRejectionORRevoke;
	}
	public Integer getWorkFromHomeStatus() {
		return workFromHomeStatus;
	}
	public void setWorkFromHomeStatus(Integer workFromHomeStatus) {
		this.workFromHomeStatus = workFromHomeStatus;
	}
	public float getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(float noOfDays) {
		this.noOfDays = noOfDays;
	}
	public Timestamp getStartDate() {
		return StartDate;
	}
	public void setStartDate(Timestamp startDate) {
		StartDate = startDate;
	}
	public Timestamp getEndDate() {
		return EndDate;
	}
	public void setEndDate(Timestamp endDate) {
		EndDate = endDate;
	}
}
