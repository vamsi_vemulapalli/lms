package com.merilytics.mail;

import java.util.Base64;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
@EnableAsync
public class EmailAlert {

	public static String email = null;
	public static String pwd = null;
	@Async
	public static void sendEmailAlert(String text, String to, String[] cc, String subject,
			Map<String, Object> emailCredentials) {
		email = (String) emailCredentials.get("it_support_email");
		pwd = new String(Base64.getDecoder().decode((String) emailCredentials.get("it_support_password")));

		// mail properties outgoing server (gmail.com)
		Properties props = new Properties();
		props.put("mail.smtp.host", emailCredentials.get("smtp_server_ip"));
		props.put("mail.smtp.port", emailCredentials.get("smtp_port"));
		props.put("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.starttls.enable", "true");

		try {
			// create Session obj
			Authenticator auth = new SMTPAuthenticator();

			Session session = Session.getInstance(props, auth);

			// prepare mail msg
			MimeMessage msg = new MimeMessage(session);
			// set header values
			msg.setSubject(subject);
		/*	msg.setFrom(new InternetAddress(email));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress("anamitra_dhar@merilytics.com"));
*/
			/*InternetAddress[] address = new InternetAddress[cc.length];
			for (int i = 0; i < cc.length; i++) {
				address[i] = new InternetAddress(cc[i]);
			}

			msg.setRecipients(Message.RecipientType.CC, address);*/
			/*System.out.println(to);
			for(int i = 0; i <cc.length; i++){
				System.out.println("CC_--"+cc[i]);
			}
			System.out.println();*/
			// msg text
			msg.setContent(text, "text/html; charset=utf-8");
			// msg.setText(text);

			Transport.send(msg);
		} // try
		catch (Exception ex) {
			ex.printStackTrace();
		} // catch
	}// constructor

	private static class SMTPAuthenticator extends javax.mail.Authenticator {
		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(email, pwd);
		}// method
	}// inner class

	public static void main(String[] args) {
		// EmailAlert mail=new EmailAlert();

		// sendEmailAlert("error in ily");
		System.out.println("mail has been delivered");
	}// main

}
