package com.merilytics.mail;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Properties;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;

import com.merilytics.bo.SystemParametersBO;
import com.merilytics.dao.SystemParametersDAO;

/**
 * @project CommissionSubmission
 * @author 21258
 * @timeAndYear 5:40:03 PM 01-Aug-2017
 * @description
 * 
 */
@Configuration("emailTemplete")
@EnableAsync
public class EmailTemplate2 {
	@Autowired
	private JavaMailSenderImpl mailSender;

	@Autowired
	private SystemParametersDAO dao;

	@Async
	public void sendMail(String to, String subject, String text) {
		try {

			String adminEmail = null;
			String itSupportEmail = null;
			String itSupportPassword = null;
			Integer SMTPPort = null;
			String SMTPServerIP = null;

			ArrayList<SystemParametersBO> list = dao.getSystemParameters();
			for (SystemParametersBO dto : list) {
				adminEmail = dto.getAdminEmail();
				itSupportEmail = dto.getItSupportEmail();
				itSupportPassword = new String(Base64.getDecoder().decode(dto.getItSupportPassword()));
				SMTPPort = dto.getSMTPPort();
				SMTPServerIP = dto.getSMTPServerIp();
			}

			Properties p = new Properties();
			p.setProperty("mail.smtp.auth", "true");
			p.setProperty("mail.smtp.starttls.enable", "true");
			p.put("mail.transport.protocol", "smtp");
			mailSender.setHost(SMTPServerIP);
			mailSender.setPort(SMTPPort);
			mailSender.setUsername(itSupportEmail);
			mailSender.setPassword(itSupportPassword);
			mailSender.setJavaMailProperties(p);

			String it = itSupportEmail;

			mailSender.send(new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
					message.setFrom(it);
					message.setTo(to);
					message.setSubject(subject);
					message.setText(text, true);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	@Async
	public void sendMail(String[] to, String subject, String text) {
		try {

			String adminEmail = null;
			String itSupportEmail = null;
			String itSupportPassword = null;
			Integer SMTPPort = null;
			String SMTPServerIP = null;

			ArrayList<SystemParametersBO> list = dao.getSystemParameters();
			for (SystemParametersBO dto : list) {
				adminEmail = dto.getAdminEmail();
				itSupportEmail = dto.getItSupportEmail();
				itSupportPassword = new String(Base64.getDecoder().decode(dto.getItSupportPassword()));
				SMTPPort = dto.getSMTPPort();
				SMTPServerIP = dto.getSMTPServerIp();
			}

			Properties p = new Properties();
			p.setProperty("mail.smtp.auth", "true");
			p.setProperty("mail.smtp.starttls.enable", "true");
			p.put("mail.transport.protocol", "smtp");
			mailSender.setHost(SMTPServerIP);
			mailSender.setPort(SMTPPort);
			mailSender.setUsername(itSupportEmail);
			mailSender.setPassword(itSupportPassword);
			mailSender.setJavaMailProperties(p);

			String it = itSupportEmail;

			mailSender.send(new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
					message.setFrom(it);
					message.setTo(to);
					message.setSubject(subject);
					message.setText(text, true);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Async
	public void sendMail(String to,String[] cc, String subject, String text) {
		try {

			String adminEmail = null;
			String itSupportEmail = null;
			String itSupportPassword = null;
			Integer SMTPPort = null;
			String SMTPServerIP = null;

			ArrayList<SystemParametersBO> list = dao.getSystemParameters();
			for (SystemParametersBO dto : list) {
				adminEmail = dto.getAdminEmail();
				itSupportEmail = dto.getItSupportEmail();
				itSupportPassword = new String(Base64.getDecoder().decode(dto.getItSupportPassword()));
				SMTPPort = dto.getSMTPPort();
				SMTPServerIP = dto.getSMTPServerIp();
			}

			Properties p = new Properties();
			p.setProperty("mail.smtp.auth", "true");
			p.setProperty("mail.smtp.starttls.enable", "true");
			p.put("mail.transport.protocol", "smtp");
			mailSender.setHost(SMTPServerIP);
			mailSender.setPort(SMTPPort);
			mailSender.setUsername(itSupportEmail);
			mailSender.setPassword(itSupportPassword);
			mailSender.setJavaMailProperties(p);

			String it = itSupportEmail;

			mailSender.send(new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
					message.setFrom(it);
					message.setTo(to);
					message.setSubject(subject);
					message.setText(text, true);
					InternetAddress[] address = new InternetAddress[cc.length];
					for (int i = 0; i < cc.length; i++) {
						address[i] = new InternetAddress(cc[i]);
					}

					message.setCc(address);
					/*System.out.println(to);
					for(int i = 0; i <cc.length; i++){
						System.out.println("CC_--"+cc[i]);
					}
					System.out.println();*/
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
