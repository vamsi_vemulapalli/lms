package com.merilytics.service;

import java.util.List;
import java.util.Map;

import com.merilytics.dto.EmploymentStatusDTO;
import com.merilytics.dto.HolidaysDTO;
import com.merilytics.dto.LeavesTypesDTO;
import com.merilytics.dto.MobileCommentsDTO;

public interface AdminSettingsService {
	
	public Map<String, Object> getAdminsettings() throws Exception;
	
	public List<Map<String, Object>> getUpcomingHolidays() throws Exception;

	public boolean SaveorUpdateLeaveSettings(LeavesTypesDTO dto, Integer empId) throws Exception;

	public boolean saveOrUpdateProbationAndNoticePeriod(EmploymentStatusDTO dto, Integer empId) throws Exception;

	public boolean SaveorUpdateHolidays(HolidaysDTO input, Integer empId) throws Exception;

	public boolean DeleteHolidays(Map<String, Object> input) throws Exception;
	
	public List<Map<String, Object>>  getAdminHistory(int startPos,int PAGE_SIZE,String search);
	public List<Map<String, Object>>  getAdminTeamHistory(int startPos,int PAGE_SIZE,String search);

	public Long getAdminTeamHistoryCount();

	boolean SaveorUpdateCommetns(MobileCommentsDTO dto) throws Exception;

}
