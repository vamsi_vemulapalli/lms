package com.merilytics.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.bo.EmploymentStatusBO;
import com.merilytics.bo.HolidaysBO;
import com.merilytics.bo.LeavesTypesBO;
import com.merilytics.bo.MobileCommentsBO;
import com.merilytics.dao.AdminSettingsDao;
import com.merilytics.dto.EmploymentStatusDTO;
import com.merilytics.dto.HolidaysDTO;
import com.merilytics.dto.LeavesTypesDTO;
import com.merilytics.dto.MobileCommentsDTO;

@Service
public class AdminSettingsServiceImpl implements AdminSettingsService {
	@Autowired
	private AdminSettingsDao settingsServiceDao;

	@Override
	public Map<String, Object> getAdminsettings() throws Exception {
		Map<String, Object> leavepolicyMap = new HashMap<String, Object>();
		Map<String, Object> holidayListMap = new HashMap<String, Object>();
		Map<String, Object> finalMap = new HashMap<String, Object>();

		List<Map<String, Object>> statusMap = settingsServiceDao.getStatusSettingsDetails();
		List<Map<String, Object>> leavesMap = settingsServiceDao.getLeaveTypeDetails();
		List<Map<String, Object>> holidayMap = settingsServiceDao.getHolidays();
		List<Map<String, Object>> temp = settingsServiceDao.tempMethod();

		for (Map<String, Object> leavesDetailsMap : leavesMap) {
			Integer id = (Integer) leavesDetailsMap.get("id");
			String type = (String) leavesDetailsMap.get("leaveType");
			if (!type.equals("Unpaid")) {

				finalMap.put(type, leavesDetailsMap);
			}

		}

		for (Map<String, Object> statusDetailsMap : statusMap) {
			String statustype = (String) statusDetailsMap.get("employmentType");
			finalMap.put(statustype, statusDetailsMap);
		}

		List itemsToRemove = new ArrayList();
		for (Map<String, Object> leavesettingMap : temp) {
			String type = (String) leavesettingMap.get("leaveType");
			if (type.equals("Unpaid")) {
				itemsToRemove.add(leavesettingMap);
			}

		}
		temp.removeAll(itemsToRemove);

		/*
		 * for (Map<String, Object> holidayDetailsMap : holidayMap) { String
		 * holidaytype = (String) holidayDetailsMap.get("holidayName");
		 * holidayListMap.put(holidaytype, holidayDetailsMap); }
		 */

		leavepolicyMap.put("leavePolicySettings", temp);
		leavepolicyMap.put("Holidays", holidayMap);

		return leavepolicyMap;
	}
	
	@Override
	public List<Map<String, Object>> getUpcomingHolidays() throws Exception {
		
		return  settingsServiceDao.getUpcomingHolidaysList();
		
		
	}

	@Override
	public boolean SaveorUpdateLeaveSettings(LeavesTypesDTO dto, Integer empId) throws Exception {

		LeavesTypesBO bo = new LeavesTypesBO();
		bo.setLeaveTypeID(dto.getLeaveTypeID());
		bo.setMaxLeavesAllowedAtOnce(dto.getMaxLeavesAllowedAtOnce());
		bo.setTotalLeavesAllowed(dto.getTotalLeavesAllowed());
		bo.setLeaveType(dto.getLeaveType());
		bo.setUpdatedBy(empId);
		bo.setUpdatedDate(dto.getUpdatedDate());

		boolean status = settingsServiceDao.SaveorUpdateLeaveSettings(bo, empId);

		return status;
	}

	@Override
	public boolean saveOrUpdateProbationAndNoticePeriod(EmploymentStatusDTO dto, Integer empId) throws Exception {
		EmploymentStatusBO bo = new EmploymentStatusBO();

		bo.setEmploymentStatus(dto.getEmploymentStatus());
		bo.setDurationInMonths(dto.getDurationInMonths());
		bo.setEmploymentStatusID(dto.getEmploymentStatusID());
		bo.setUpdatedBy(empId);
		bo.setUpdatedDate(new Timestamp(new Date().getTime()));

		boolean status = settingsServiceDao.saveOrUpdateProbationAndNoticePeriod(bo, empId);

		return status;

	}

	@Override
	public boolean SaveorUpdateHolidays(HolidaysDTO dto, Integer empId) throws Exception {
		boolean status = false;
		HolidaysBO bo = new HolidaysBO();
		if (dto.getHolidayID() == null) {

			bo.setHolidayName(dto.getHolidayName());
			bo.setHolidayTypeID(dto.getHolidayTypeID());
			bo.setDayOfTheWeek(dto.getDayOfTheWeek());
			bo.setHolidayDate(dto.getHolidayDate());
			bo.setUpdatedBy(empId);
			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			bo.setCreatedBy(empId);
			bo.setCreatedDate(new Timestamp(new Date().getTime()));

			status = settingsServiceDao.saveHolidays(bo);
		} else {
			bo.setHolidayID(dto.getHolidayID());
			bo.setHolidayName(dto.getHolidayName());
			bo.setHolidayTypeID(dto.getHolidayTypeID());
			bo.setDayOfTheWeek(dto.getDayOfTheWeek());
			bo.setHolidayDate(dto.getHolidayDate());
			bo.setUpdatedBy(empId);

			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			status = settingsServiceDao.updateHolidays(bo);

		}

		return status;

	}

	@Override
	public boolean DeleteHolidays(Map<String, Object> input) throws Exception {
		HolidaysBO bo = new HolidaysBO();
		boolean status = false;

		bo.setHolidayID((Integer) input.get("holidayID"));
		status = settingsServiceDao.DeleteHolidays(bo);
		return status;

	}

	@Override
	public List<Map<String, Object>> getAdminHistory(int startPos,int PAGE_SIZE,String search) {
		List<Map<String, Object>> listofData=new ArrayList<Map<String, Object>>();
		listofData=settingsServiceDao.getAdminHistory(startPos,PAGE_SIZE,search);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getAdminTeamHistory(int startPos,int PAGE_SIZE,String search) {
		List<Map<String, Object>> listofData=new ArrayList<Map<String, Object>>();
		listofData=settingsServiceDao.getAdminTeamHistory(startPos,PAGE_SIZE,search);
		return listofData;
	}
	
	@Override
	public Long getAdminTeamHistoryCount() {
		return settingsServiceDao.getAdminTeamHistoryCount();
	}
	@Override
	public boolean SaveorUpdateCommetns(MobileCommentsDTO dto) throws Exception {
		boolean status = false;
		MobileCommentsBO bo = new MobileCommentsBO();
		

			bo.setComment(dto.getComment());
			bo.setCommentProvider(dto.getCommentProvider());
			bo.setLeaveID(dto.getLeaveID());
			
			bo.setUpdatedBy(dto.getCommentProvider());
			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			bo.setCreatedBy(dto.getCommentProvider());
			bo.setCreatedDate(new Timestamp(new Date().getTime()));
System.out.println(bo);
			status = settingsServiceDao.saveComments(bo);
		

		return status;

	}
}
