package com.merilytics.service;

import java.util.List;

public interface AppDataService {
	public List getAllApps(String userToken) throws Exception ;

	public List getLMSReports(int empId) throws Exception;
}
