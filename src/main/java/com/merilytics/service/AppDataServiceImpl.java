package com.merilytics.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;

import com.merilytics.dao.AppDataDAO;

@Named
public class AppDataServiceImpl implements AppDataService {
	@Resource
	private AppDataDAO dao;
	@Inject
	private ServletContext servletContext;

	@Override
	public List getAllApps(String userToken) throws Exception {
		return dao.getApps(userToken);

	}

	@Override
	public List getLMSReports(int empId) throws Exception {
		List<String> listexcelNames = new LinkedList<String>();
		Map<String, Object> finalMap = new LinkedHashMap<String, Object>();
		Map<String, Object> finalMap2 = new LinkedHashMap<String, Object>();
		Map<String, Object> finalMap3 = new LinkedHashMap<String, Object>();
		List finalData = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		String month = new SimpleDateFormat("MMMM").format(cal.getTime());
		String year = new SimpleDateFormat("YYYY").format(cal.getTime());

		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

		// calculate monday week ago (moves cal 7 days back)
		cal.add(Calendar.DATE, -7);
		Date firstDateOfPreviousWeek = cal.getTime();

		// calculate sunday last week (moves cal 6 days fwd)
		cal.add(Calendar.DATE, 6);
		Date lastDateOfPreviousWeek = cal.getTime();

		SimpleDateFormat formatter = new SimpleDateFormat("MMM");

		String mondayMonth = formatter.format(firstDateOfPreviousWeek);
		String sundayMonth = formatter.format(lastDateOfPreviousWeek);

		Integer nextYear = Integer.parseInt(year);

		String excelNameSiloMTD = "Week(" + firstDateOfPreviousWeek.getDate() + " " + mondayMonth + " - "
				+ lastDateOfPreviousWeek.getDate() + " " + sundayMonth + ")";

		String excelNameSiloYTD = "YTD(Apr " + nextYear + " - Mar " + (nextYear + 1) + ")";
		String excelNameLeavesReport = "Leaves History";

		listexcelNames.add(excelNameSiloMTD);
		listexcelNames.add(excelNameSiloYTD);
		listexcelNames.add(excelNameLeavesReport);

		List<String> headersOfSiloMTD = null;
		List<String> headersOfSiloYTD = null;
		List<String> headersOfLeavesReport = null;
		List<Map<String, Object>> listOfSiloMTD = null;
		List<Map<String, Object>> listOfSiloYTD = null;
		List<Map<String, Object>> listOfLeavesReport = null;

		List listOfObjects = null;

		List<List<String>> listOfHeaders = null;
		listOfObjects = new ArrayList<>();

		listOfHeaders = new LinkedList<List<String>>();

		String siloMTDForHeaders = "{call EmployeeDB_Live.dbo.EMPDB_sp_lmsSILO_MTDReport(?)}";
		String siloYTDForHeaders = "{call EmployeeDB_Live.dbo.EMPDB_sp_lmsSILO_YTDReport(?)}";
		String siloLeavesReportForHeaders = "{call EmployeeDB_Live.dbo.EMPDB_sp_lmsSILOCurrentLeavesReport(?)}";
		listOfSiloMTD = dao.getLMSMTDReports(empId);
		listOfSiloYTD = dao.getLMSYTDReports(empId);
		listOfLeavesReport = dao.getLMSSILOReports(empId);
		headersOfSiloMTD = getHeaders(siloMTDForHeaders, empId);
		headersOfSiloYTD = getHeaders(siloYTDForHeaders, empId);
		headersOfLeavesReport = getHeaders(siloLeavesReportForHeaders, empId);
//		listOfObjects.add(listOfSiloMTD);
//		listOfObjects.add(listOfSiloYTD);
//		listOfObjects.add(listOfLeavesReport);
//
//		listOfHeaders.add(headersOfSiloMTD);
//		listOfHeaders.add(headersOfSiloYTD);
//		listOfHeaders.add(headersOfLeavesReport);
		List<Map<String, Object>> redisnedList = null;
		redisnedList = new ArrayList<Map<String, Object>>();
		for(Map<String,Object> redisgn:listOfSiloMTD) {
			Map<String, Object> newMap= null;
			newMap = new LinkedHashMap<String ,Object>();
		for(String key : headersOfSiloMTD) {
			//redisnedList.add(arg0)
			newMap.put(key, redisgn.get(key));
		}
		redisnedList.add(newMap);
		}
		finalMap.put("body", redisnedList);
		finalMap.put("header", headersOfSiloMTD);
		finalMap.put("name", excelNameSiloMTD);
		finalData.add(finalMap);
		
		List<Map<String, Object>> redisnedList2 = null;
		redisnedList2 = new ArrayList<Map<String, Object>>();
		for(Map<String,Object> redisgn:listOfSiloYTD) {
			Map<String, Object> newMap= null;
			newMap = new LinkedHashMap<String ,Object>();
		for(String key : headersOfSiloYTD) {
			//redisnedList.add(arg0)
			newMap.put(key, redisgn.get(key));
		}
		redisnedList2.add(newMap);
		}
		
		finalMap2.put("body", redisnedList2);
		finalMap2.put("header", headersOfSiloYTD);
		finalMap2.put("name", excelNameSiloYTD);
		finalData.add(finalMap2);
		
		List<Map<String, Object>> redisnedList3 = null;
		redisnedList3 = new ArrayList<Map<String, Object>>();
		for(Map<String,Object> redisgn:listOfLeavesReport) {
			Map<String, Object> newMap= null;
			newMap = new LinkedHashMap<String ,Object>();
		for(String key : headersOfLeavesReport) {
			//redisnedList.add(arg0)
			newMap.put(key, redisgn.get(key));
		}
		redisnedList3.add(newMap);
		}

		finalMap3.put("body", redisnedList3);
		finalMap3.put("header", headersOfLeavesReport);
		finalMap3.put("name", excelNameLeavesReport);

		finalData.add(finalMap3);
		return finalData;

	}

	public List<String> getHeaders(String query, Integer empID) {

		Connection con = null;

		Properties props = new Properties();
		FileInputStream in;
		try {
			String applicationContext = servletContext.getRealPath("/WEB-INF/classes/DB.properties");

			in = new FileInputStream(applicationContext);
			props.load(in);
			in.close();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Class.forName(props.getProperty("jdbc.driver"));
			con = DriverManager.getConnection(props.getProperty("jdbc.url"), props.getProperty("jdbc.user"),
					props.getProperty("jdbc.pwd"));
			try {
				PreparedStatement pstmt = con.prepareStatement(query);
				pstmt.setInt(1, empID);
				ResultSet rs = pstmt.executeQuery();
				ResultSetMetaData md = rs.getMetaData();
				int col = md.getColumnCount();
				List<String> headersList = new LinkedList<String>();
				for (int i = 1; i <= col; i++) {
					String col_name = md.getColumnName(i);
					headersList.add(col_name);

				}
				return headersList;
			} catch (SQLException s) {

				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
