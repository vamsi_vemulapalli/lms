package com.merilytics.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.merilytics.dto.EmployeeLeaveTransactionsDTO;

public interface ApplyLeaveService {
	public Map<String, Object> saveLeaveDetails(EmployeeLeaveTransactionsDTO dto, List<Map<String, Object>> input)
			throws Exception;

	public List<Map<String, Object>> getDropdownLeaves(Integer empId) throws Exception;

	public List<Map<String, Object>> getSession() throws Exception;

	public List<Map<String, Object>> getHolidayType() throws Exception;

	public boolean saveLeaveEmailRecipients(Map<String, Object> input);

	public boolean approveLeaveRequest(EmployeeLeaveTransactionsDTO dto);
	
	public boolean convertFromUnpaidToPaid(Map<String, Object> input)throws Exception;

	/*public boolean rejectLeaveRequest(EmployeeLeaveTransactionsDTO dto);*/

	public void remindLeaveRequest(Map<String, Object> input,float nOfdays) throws Exception;
	
	public Map<String, Object> approveOrRejectNotification(Map<String, Object> input,String employeeName);
	
	public Double countNoOfDays(Date sdate,Date edate,Integer startSession,Integer endSession,Integer leaveTypeID);
	
	public boolean approveFromUnpaidToPaid(Map<String, Object> input)throws Exception;
	
	public boolean rejectFromUnpaidToPaid(Map<String, Object> input)throws Exception;
	
}
