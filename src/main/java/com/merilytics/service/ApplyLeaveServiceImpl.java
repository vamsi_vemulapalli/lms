package com.merilytics.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.bo.EmployeeLeaveTransactionsBO;
import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;
import com.merilytics.dao.AdminSettingsDao;
import com.merilytics.dao.ApplyLeaveDao;
import com.merilytics.dao.ApplyWfhDao;
import com.merilytics.dao.EmployeeDetailsDao;
import com.merilytics.dao.EmployeeLeaveDisplayDao;
import com.merilytics.dto.EmployeeLeaveTransactionsDTO;
import com.merilytics.mail.EmailTemplate2;
import com.merilytics.util.Conversion;
import com.merilytics.util.EmailTemplate;

@Service
public class ApplyLeaveServiceImpl implements ApplyLeaveService {
	@Autowired
	private ApplyLeaveDao applyLeaveDao;

	@Autowired
	private EmployeeLeaveDisplayDao empDao;

	@Autowired
	private EmployeeDetailsDao EmployeeDao;

	@Autowired
	private ApplyWfhDao applyWfhdao;

	
	@Resource
	private EmailTemplate2 emailer;
	
	@Autowired
	private AdminSettingsDao settingsServiceDao;
	
	@Autowired
	private EmployeeLeaveDisplayDao employeeLeaveDisplayDao;
	
	@Autowired
	private EmployeeleaveDispalyService employeeleaveDispalyService;
	
	

	@Override
	public Map<String, Object> saveLeaveDetails(EmployeeLeaveTransactionsDTO dto,List<Map<String, Object>> input) throws Exception {

		EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
		boolean status = false;
		boolean flag=true;
		Map<String, Object> map = new HashMap<String, Object>();
		
	
		
		if (dto.getLeaveID() == null) {
			
			bo.setEmployeeID((Integer) dto.getEmployeeID());
			bo.setLeaveType((Integer) dto.getLeaveType());
			bo.setNoOfDays((float) dto.getNoOfDays());
			bo.setReasonForLeave((String) dto.getReasonForLeave());
			bo.setLeaveStartDate(dto.getLeaveStartDate());
			bo.setLeaveEndDate(dto.getLeaveEndDate());
			bo.setCreatedDate(dto.getCreatedDate());
			bo.setUpdatedDate(dto.getUpdatedDate());
			bo.setLeaveStatus(dto.getLeaveStatus());
			bo.setSessionOfStartDate(dto.getSessionOfStartDate());
			bo.setSessionOfEndDate(dto.getSessionOfEndDate());
			bo.setIsAutomated(dto.getIsAutomated());
			bo.setIsApproveSLT(dto.getIsApproveSLT());
			bo.setUpdatedBy(dto.getUpdatedBy());
			bo.setCreatedBy(dto.getCreatedBy());

			map = applyLeaveDao.saveLeaveDetails(bo);
			
			
			//sending email code
			emailInvoke(dto,input);
			
			
		} else {
			
			bo.setLeaveID(dto.getLeaveID());
			bo.setEmployeeID((Integer) dto.getEmployeeID());
			bo.setLeaveType((Integer) dto.getLeaveType());
			bo.setNoOfDays((float) dto.getNoOfDays());
			bo.setReasonForLeave((String) dto.getReasonForLeave());
			bo.setLeaveStartDate(dto.getLeaveStartDate());
			bo.setLeaveEndDate(dto.getLeaveEndDate());
			bo.setUpdatedDate(dto.getUpdatedDate());
			bo.setLeaveStatus(dto.getLeaveStatus());
			bo.setIsAutomated(dto.getIsAutomated());
			bo.setIsApproveSLT(dto.getIsApproveSLT());
			bo.setSessionOfStartDate(dto.getSessionOfStartDate());
			bo.setSessionOfEndDate(dto.getSessionOfEndDate());
			bo.setUpdatedBy(dto.getUpdatedBy());
			map = applyLeaveDao.updateLeaveDetails(bo);
			//sending email code
			emailInvoke(dto,input);
			
			
		}
		return map;
	}

	

	public void emailInvoke(EmployeeLeaveTransactionsDTO dto,List<Map<String, Object>> input){
		List<String> managerEmailList= new ArrayList<String>();
		String template=null;
		Map<String,Object> managerDetails=EmployeeDao.getManagerDetails(dto.getEmployeeID());
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee(dto.getEmployeeID());
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		for(Map<String, Object> email:input)
		{
			String leaveType=" for a Leave ";
			Map<String, Object> detail=new HashMap<String, Object>();
			detail.put("name", managerDetails.get("Name"));
			detail.put("sdate", dto.getLeaveStartDate());
			detail.put("edate", dto.getLeaveEndDate());
			detail.put("reason", dto.getReasonForLeave());
			detail.put("leaveType", dto.getLeaveType());
			detail.put("duration", dto.getNoOfDays());
			detail.put("leave", leaveType);
			detail.put("firstsession", dto.getSessionOfStartDate());
			detail.put("secondsession", dto.getSessionOfEndDate());
			/*Integer listIDS=Conversion.objectToInteger();
			Integer managerID=Conversion.objectToInteger();*/
			if((int)email.get("employeeID")!=(int)managerDetails.get("EmployeeID")){
				managerEmailList.add((String)email.get("emailID"));
			}
			
			
			template = EmailTemplate.submittedMailToManager(detail,leaveAppliedBy);
		}
		String[] CCList = managerEmailList.toArray(new String[managerEmailList.size()]);
		emailer.sendMail((String)managerDetails.get("Email"),CCList, leaveAppliedBy  +" has applied for a Leave ", template);
		
		
	}

	@Override
	public List<Map<String, Object>> getDropdownLeaves(Integer empId ) throws Exception {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = applyLeaveDao.getDropdownLeaves(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getSession() throws Exception {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = applyLeaveDao.getSession();
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getHolidayType() throws Exception {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = applyLeaveDao.getHolidayType();
		return listofData;
	}

	@Override
	public boolean saveLeaveEmailRecipients(Map<String, Object> input) {
		LeaveEmailRecipientsTransactionsBO bo = new LeaveEmailRecipientsTransactionsBO();
		boolean status = false;
		Integer id = 0;
		List<Map<String, Object>> email = (List<Map<String, Object>>) input.get("teamId");
		for (Map<String, Object> empId : email) {
			bo.setLeaveID(Conversion.objectToInteger(input.get("leaveid")));
			bo.setTeamMemberID(Conversion.objectToInteger(empId.get("employeeID")));
			id = (Integer) input.get("employeeId");
			bo.setCreatedBy(id);
			bo.setUpdatedBy(id);
			bo.setCreatedDate(new Timestamp(new Date().getTime()));
			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			status = applyLeaveDao.saveLeaveEmailRecipients(bo);
		}

		return status;
	}

	@Override
	public boolean approveLeaveRequest(EmployeeLeaveTransactionsDTO dto ) {
		
		EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
		boolean status = false;
		bo.setLeaveID(dto.getLeaveID());
		bo.setEmployeeID(dto.getEmployeeID());
		bo.setApprovedByORRejectedBy(dto.getApprovedByORRejectedBy());
		bo.setApprovedORRejectionDate(dto.getApprovedORRejectionDate());
		bo.setReasonForRejectionORRevoke(dto.getReasonForRejectionORRevoke());
		bo.setLeaveStatus(dto.getLeaveStatus());
		bo.setReasonForRejectionORRevoke(dto.getReasonForRejectionORRevoke());
		bo.setUpdatedBy(dto.getUpdatedBy());
		status = applyLeaveDao.approveLeaveRequest(bo);
		//call the proc
		try {
			applyLeaveDao.changeCompOffStatus(dto.getEmployeeID());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return status;
	}
	
	
	/*@Override
	public boolean rejectLeaveRequest(EmployeeLeaveTransactionsDTO dto ) {
		
		EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
		boolean status = false;
		bo.setLeaveID(dto.getLeaveID());
		bo.setEmployeeID(dto.getEmployeeID());
		bo.setApprovedByORRejectedBy(dto.getApprovedByORRejectedBy());
		bo.setApprovedORRejectionDate(dto.getApprovedORRejectionDate());
		bo.setReasonForRejectionORRevoke(dto.getReasonForRejectionORRevoke());
		bo.setLeaveStatus(dto.getLeaveStatus());
		status = applyLeaveDao.rejectLeaveRequest(bo);
		return status;
	}
*/
	@Override
	public void remindLeaveRequest(Map<String, Object> input,float nOfdays) throws Exception {
	
			String leaveType="Leave";
			Map<String, Object> detail=new HashMap<String, Object>();
			detail.put("name", input.get("employeeName"));
			detail.put("sdate", input.get("leaveStartDate"));
			detail.put("edate", input.get("leaveEndDate"));
			detail.put("reason", input.get("reasonForLeave"));
			detail.put("leaveType", input.get("leaveType"));
			detail.put("duration",nOfdays );
			detail.put("leave", leaveType);
			detail.put("firstsession", input.get("sessionOfStartDate"));
			detail.put("secondsession", input.get("sessionOfEndDate"));
		
			List<Map<String, Object>> finalmap = EmployeeDao.getEmployee((Integer)input.get("employeeID"));
		
			
			Map<String,Object> mailMap=new HashMap<String,Object>();
			Map<String,Object> mailMapName=new HashMap<String,Object>();
		
			mailMap.put("l1manager", (String)finalmap.get(0).get("managerEmail"));
			mailMap.put("seniormgr", (String)finalmap.get(0).get("seniorMangerEmail"));
			mailMapName.put("mgrName", (String)finalmap.get(0).get("managerName"));
			mailMapName.put("snrMgrName", (String)finalmap.get(0).get("seniorManagerName"));
			
			String leaveAppliedBy=(String)finalmap.get(0).get("empName");
			String managerEmail=(String)finalmap.get(0).get("managerEmail");
			String managerName=(String)finalmap.get(0).get("managerName");
			String srManager=(String)mailMap.get("snrMgrName");
			
			int gender=(int) finalmap.get(0).get("gender");
			
			String genderChar="";
			if(gender==1){
				genderChar="his";
			}else{
				genderChar="her";
			}
			
			detail.put("gender", genderChar);
			int isApproveBySLT=(Integer)input.get("isApproveSLT");
			if(isApproveBySLT==1)
			{
				List<Map<String, Object>> seniorMgrsMailMap = employeeLeaveDisplayDao.getImmediateSeniorManger(Conversion.objectToInteger(input.get("employeeID")));
				for(Map<String,Object> names:seniorMgrsMailMap)
				{
					String srMgrName=(String)names.get("empName");
					String srMgremail=(String)names.get("email");
					String template = EmailTemplate.followUPMailByManagerToRemindEmployee(detail,leaveAppliedBy,srMgrName);
					emailer.sendMail(srMgremail, "Reminder � "+leaveAppliedBy  +"'s "+leaveType+" application."  , template);
				}
				
			}
			else{
				String template = EmailTemplate.followUPMailByManagerToRemindEmployee(detail,leaveAppliedBy,managerName);
				emailer.sendMail(managerEmail, "Reminder � "+leaveAppliedBy  +"'s "+leaveType+" application." , template);
			}
			
			
		
				
			
			
			
		
		
	}



	@Override
	public Map<String, Object> approveOrRejectNotification(Map<String, Object> input,String employeeName) {
		boolean flag=false;
		String typeOfLeave="for Leave";
		Map<String,Object> map=new HashMap<String,Object>();
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee(Conversion.objectToInteger(input.get("employeeID")));
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		String employeeEmail=(String)finalmap.get(0).get("emailID");
		String managerName=(String)finalmap.get(0).get("managerName");
		List<Map<String, Object>> managermap = EmployeeDao.getEmployee(Conversion.objectToInteger(input.get("approvedByID")));
		String Name=(String)managermap.get(0).get("empName");
		map.put("name", employeeName);
		map.put("employeeID", input.get("employeeID"));
		map.put("leaveType", input.get("leaveType"));
		map.put("reason", input.get("reasonForRejectionORRevoke"));
		map.put("managerName", managerName);
		map.put("typeOfLeave", typeOfLeave);
		map.put("aprovedBy", Name);
		
		/*map.put("startDate", input.get("leaveStartDate"));
		map.put("endDate", input.get("leaveEndDate"));*/
		map.put("firstsession", input.get("sessionOfStartDate"));
		map.put("secondsession",input.get("sessionOfEndDate"));
		int status=Conversion.objectToInteger(input.get("leaveStatus"));
		try{
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = (String) input.get("leaveStartDate");
			String endDate = (String) input.get("leaveEndDate");
			Date sdate=sf.parse(startDate);
			Date edate=sf.parse(endDate);
			map.put("startDate", sdate);
			map.put("endDate", edate);
			Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = Integer.parseInt(input.get("sessionOfEndDate")+"");
			Integer leaveTypeID=Conversion.objectToInteger(input.get("leaveType"));
			Double days=applyWfhdao.countNoOfDays(sdate,edate,startSession,endSession,leaveTypeID);
			Float d= Conversion.objectToFloat(input.get("noOfDays"));
			
			map.put("duration", d);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		
		Map<String, Object> statusMap = new HashMap<String, Object>();
		String leavestatus="";
		if(status==1)
		{
			leavestatus="approved";
			map.put("leaveStatus", leavestatus);
			String template = EmailTemplate.approveMail(map,leaveAppliedBy,managerName);
			emailer.sendMail(employeeEmail, Name.trim() +" has "+leavestatus+" your application "+typeOfLeave  , template);
			statusMap.put("status", leavestatus);
			statusMap.put("flag", flag);
			//flag=true;
		}
		else{
		
			leavestatus="rejected";
			map.put("leaveStatus", leavestatus);
			String template = EmailTemplate.approveOrRejectMail(map,leaveAppliedBy,managerName);
			emailer.sendMail(employeeEmail, Name.trim() +" has "+leavestatus+" your application "+typeOfLeave  , template);
			//flag=true;
			statusMap.put("status", leavestatus);
			statusMap.put("flag", flag);
		}
		return statusMap;
		
		
	
	}



	@Override
	public Double countNoOfDays(Date sdate, Date edate,Integer startSession,Integer endSession,Integer leaveTypeID) {
		Double days=applyLeaveDao.countNoOfDays(sdate,edate,startSession,endSession,leaveTypeID);
		return days;
	}



	@Override
	public boolean convertFromUnpaidToPaid(Map<String, Object> input) throws Exception {
		boolean flag= false;
		if((input.get("conversionTypeId")!=null)||(input.get("firsthalfconversionTypeId").toString().equals(input.get("secondhalfconversionTypeId")))) {
	System.out.println("in if block");
			EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
		bo.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
		if(input.get("conversionTypeId")!=null) {			
			bo.setLeaveStatus(Conversion.objectToInteger(input.get("conversionTypeId")));
		}else {
			bo.setLeaveStatus(Conversion.objectToInteger(input.get("firsthalfconversionTypeId")));
		}
		bo.setUpdatedBy(Conversion.objectToInteger(input.get("employeeID")));
		bo.setUpdatedDate(new Timestamp(new Date().getTime()));
		bo.setCreatedDate(new Timestamp(new Date().getTime()));
		bo.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("employeeID")));
		bo.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
		
		flag=applyLeaveDao.convertFromUnpaidToPaid(bo);
		emailFromConversionToUnpaid(input);
		}else {
			//converting first half
			EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
			bo.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
			bo.setLeaveStatus(Conversion.objectToInteger(input.get("firsthalfconversionTypeId")));
			bo.setUpdatedBy(Conversion.objectToInteger(input.get("employeeID")));
			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			bo.setCreatedDate(new Timestamp(new Date().getTime()));
			bo.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("employeeID")));
			bo.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
			bo.setSessionOfStartDate(2);
			bo.setSessionOfEndDate(1);		
			bo.setNoOfDays(new Float(0.5));
			flag=applyLeaveDao.convertFromUnpaidToPaid(bo);
			//adding the record for second half
			
			EmployeeLeaveTransactionsBO saveLeaveBO = new EmployeeLeaveTransactionsBO();
			//saveLeaveBO.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
			saveLeaveBO.setEmployeeID(Conversion.objectToInteger(input.get("employeeID")));
			saveLeaveBO.setLeaveType(Conversion.objectToInteger(input.get("LeaveTypeID")));
			saveLeaveBO.setNoOfDays(new Float(0.5));
			saveLeaveBO.setReasonForLeave((String) input.get("reasonForLeave"));
			saveLeaveBO.setLeaveStartDate(Conversion.objectToTimestamp1(input.get("leaveStartDate")));
			saveLeaveBO.setLeaveEndDate(Conversion.objectToTimestamp1(input.get("leaveEndDate")));
			saveLeaveBO.setSessionOfStartDate(3);
			saveLeaveBO.setSessionOfEndDate(1);
			saveLeaveBO.setLeaveStatus(Conversion.objectToInteger(input.get("secondhalfconversionTypeId")));
			saveLeaveBO.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("employeeID")));
			saveLeaveBO.setApprovedORRejectionDate(new Timestamp(new Date().getTime()));
			saveLeaveBO.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
			saveLeaveBO.setCreatedBy(Conversion.objectToInteger(input.get("employeeID")));
			saveLeaveBO.setUpdatedBy(Conversion.objectToInteger(input.get("employeeID")));
			saveLeaveBO.setCreatedDate(new Timestamp(new Date().getTime()));
			saveLeaveBO.setUpdatedDate(new Timestamp(new Date().getTime()));
			saveLeaveBO.setIsAutomated(1);
			saveLeaveBO.setIsApproveSLT(1);
			Map<String, Object> result=null;
			result=applyLeaveDao.saveLeaveDetails(saveLeaveBO);
			
			List<Map<String, Object>> finalmap = null;
			finalmap = employeeleaveDispalyService.getDefaultEmailRecipients(Conversion.objectToInteger(input.get("employeeID")));
			
			int recipientLeaveId =Conversion.objectToInteger( result.get("id"));
			Map<String, Object> recipientsMap = new HashMap<String, Object>();
			recipientsMap.put("employeeId", Conversion.objectToInteger(input.get("employeeID")));
			recipientsMap.put("leaveid", recipientLeaveId);
			recipientsMap.put("teamId", finalmap);
			
			flag = saveLeaveEmailRecipients(recipientsMap);
			
			//have to send email from here

		}
		return flag;
				
		
	}

	public void emailFromConversionToUnpaid(Map<String, Object> input){
		SimpleDateFormat myFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
		String inputString1 = (String)input.get("leaveStartDate");
		 Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString1);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}  
		  
	String output = myFormat.format(date);
	
		
		List<Map<String, Object>> mangerDataList=employeeLeaveDisplayDao.getImmediateSeniorManger((Integer) input.get("employeeID"));
		Map<String,Object> mangerData=mangerDataList.get(0);
		Map<String,Object> emailContent= new LinkedHashMap<String,Object>();
		emailContent.put("empName", input.get("employeeName"));
		emailContent.put("managerName", mangerData.get("empName"));
		emailContent.put("leaveDate", output);
		emailContent.put("conversionType", input.get("conversionType"));
		emailContent.put("reason", input.get("reasonForRejectionORRevoke"));
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee((Integer)input.get("employeeID"));
		int gender=(int) finalmap.get(0).get("gender");
		
		String genderChar="";
		if(gender==1){
			genderChar="his";
		}else{
			genderChar="her";
		}
		
		emailContent.put("gender", genderChar);
		
		String emailContent1=EmailTemplate.convertionMailToManager(emailContent);
		emailer.sendMail((String)mangerData.get("email"), input.get("employeeName")+" has requested for a leave change for an paid leave on "+output, emailContent1);
	}

	@Override
	public boolean approveFromUnpaidToPaid(Map<String, Object> input) throws Exception {
		
		boolean flag=false;
		if((Integer)input.get("conversionTypeId")==7){
		Integer availableLeaveTypeID=applyLeaveDao.getTheAvailableLeaveType(Conversion.objectToInteger(input.get("employeeID")),Conversion.objectToInteger(input.get("leaveID")),Conversion.objectToInteger(input.get("approvedByORRejectedBy")));
		if(availableLeaveTypeID==null){
			return true;
		}
		EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
		bo.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
		bo.setLeaveStatus(1);
		bo.setLeaveType(availableLeaveTypeID);
		bo.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("approvedByORRejectedBy")));
		bo.setUpdatedBy(Conversion.objectToInteger(input.get("approvedByORRejectedBy")));
		bo.setUpdatedDate(new Timestamp(new Date().getTime()));
		bo.setApprovedORRejectionDate(new Timestamp(new Date().getTime()));
		
		flag=applyLeaveDao.approveUnpaidToPaid(bo);
		try {
			applyLeaveDao.changeCompOffStatus(Conversion.objectToInteger(input.get("employeeID")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		emailUnpaidApproveOrReject(input,"approved");
		return flag;
		}else{
			Integer pk= null;
			WorkFromHomeTransactionsBO bo = new WorkFromHomeTransactionsBO();
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = (String) input.get("leaveStartDate");
			String endDate = (String) input.get("leaveEndDate");
			Date sdate=sf.parse(startDate);
			Date edate=sf.parse(endDate);
			bo.setEmployeeID(Conversion.objectToInteger(input.get("employeeID")));
			bo.setNoOfDays(Conversion.objectToFloat(input.get("noOfDays")));
			bo.setReasonForWorkFromHome((String) input.get("reasonForLeave"));
			bo.setStartDate(new Timestamp(Conversion.objectToLong(input.get("orginalStartDate"))));
			bo.setEndDate(new Timestamp(Conversion.objectToLong(input.get("orginalEndDate"))));
			bo.setSessionOfStartDate(Conversion.objectToInteger(input.get("sessionOfStartDate")));
			bo.setSessionOfEndDate(Conversion.objectToInteger(input.get("sessionOfEndDate")));
			bo.setWorkFromHomeStatus(1);
			bo.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
			bo.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("approvedByORRejectedBy")));
			bo.setCreatedBy(Conversion.objectToInteger(input.get("employeeID")));
			bo.setCreatedDate(new Timestamp(new Date().getTime()));
			bo.setUpdatedBy(Conversion.objectToInteger(input.get("approvedByORRejectedBy")));
			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			bo.setApprovedORRejectionDate(new Timestamp(new Date().getTime()));
			bo.setIsApproveSLT(1);
			bo.setIsAutomated(1);
			bo.setConversionTypeId(Conversion.objectToInteger(input.get("conversionTypeId")));
			pk=applyLeaveDao.saveWorkFromHomeDetails(bo);
			applyLeaveDao.updateEmailReciepienst(Conversion.objectToInteger(input.get("leaveID")),pk);
			flag=applyLeaveDao.deleteLeaveDetails(Conversion.objectToInteger(input.get("leaveID")));
			emailUnpaidApproveOrReject(input,"approved");
			return flag;
			
			
			 
		}
	}



	@Override
	public boolean rejectFromUnpaidToPaid(Map<String, Object> input) throws Exception {
		
		boolean flag= false;
		EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
		bo.setLeaveID(Conversion.objectToInteger(input.get("leaveID")));
		bo.setLeaveStatus(1);
		bo.setLeaveType(6);
		bo.setBiometricIssueReasonForRejectionOrRevoke((String)input.get("biometricIssueReasonForRejectionOrRevoke"));
		bo.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("approvedByORRejectedBy")));
		bo.setUpdatedBy(Conversion.objectToInteger(input.get("employeeID")));
		bo.setUpdatedDate(new Timestamp(new Date().getTime()));
		bo.setApprovedORRejectionDate(new Timestamp(new Date().getTime()));
		flag=applyLeaveDao.rejectUnpaidToPaid(bo);
		emailUnpaidApproveOrReject(input,"rejected");
		return flag;
	}

public void emailUnpaidApproveOrReject(Map<String, Object> input,String status){
	SimpleDateFormat myFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
	String inputString1 = (String)input.get("leaveStartDate");
	 Date date = null;
	try {
		date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString1);
	} catch (ParseException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}  
	  
String output = myFormat.format(date);
		
		Map<String,Object> emailContent= new LinkedHashMap<String,Object>();
		emailContent.put("empName", input.get("employeeName"));
		emailContent.put("leaveDate", output);
		emailContent.put("status", status);
		emailContent.put("reason", input.get("reasonForRejectionORRevoke"));
		emailContent.put("conversionType", input.get("conversionType"));
		String emailContent1=EmailTemplate.approveRejectFromUnpaidToPaid(emailContent);
		emailer.sendMail((String)input.get("emailId"), "Request for a leave change from paid to "+input.get("conversionType")+" has been "+status, emailContent1);
	}

	/*@Override
	public void rejectNotificationEmail(Map<String, Object> input, List<Map<String, Object>> emailMap) {
		for(Map<String, Object> email:emailMap)
		{
			String leaveType="leave";
			Map<String, Object> detail=new HashMap<String, Object>();
			detail.put("name", email.get("employeeName"));
			
			detail.put("reason", input.get("reasonForLeave"));
			detail.put("leaveType", input.get("leaveType"));
			
			detail.put("leave", leaveType);
		
			String emailid=(String)email.get("emailID");
			List<Map<String, Object>> finalmap = EmployeeDao.getEmployee((Integer)input.get("employeeID"));
			String leaveAppliedBy=(String)finalmap.get(0).get("empName");
			
			String template = EmailTemplate.followUPMailByManagerToRemindEmployee(detail,leaveAppliedBy);
			emailer.sendMail(emailid, leaveAppliedBy  +" wants to remind you that his  ", template);
		}
	}*/

}
