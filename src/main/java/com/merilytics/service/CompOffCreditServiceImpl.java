package com.merilytics.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.bo.CompOffTransactionsBO;
import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.dao.CompOffCreditDao;
import com.merilytics.dao.EmployeeDetailsDao;
import com.merilytics.dao.EmployeeLeaveDisplayDao;
import com.merilytics.dto.CompOffTransactionsDTO;
import com.merilytics.mail.EmailTemplate2;
import com.merilytics.util.Conversion;
import com.merilytics.util.EmailTemplate;

@Service
public class CompOffCreditServiceImpl implements CompOffCreditService {
	@Autowired
	private CompOffCreditDao compOffCreditDao;
	@Autowired
	private EmployeeDetailsDao EmployeeDao;

	@Resource
	private EmailTemplate2 emailer;
	
	@Autowired
	private EmployeeLeaveDisplayDao employeeLeaveDisplayDao;


	@Override
	public Map<String, Object> saveCompOffDetails(CompOffTransactionsDTO dto, List<Map<String, Object>> input)
			throws Exception {
		CompOffTransactionsBO bo = new CompOffTransactionsBO();
		Map<String, Object> map = new HashMap<String, Object>();
		boolean status = false;
		

		if (dto.getCompOffID() == null) {

			bo.setEmployeeID(dto.getEmployeeID());
			//bo.setCompOffStartDate(dto.getCompOffStartDate());
			//bo.setCompOffEndDate(dto.getCompOffEndDate());
			//bo.setSessionOfStartDate(dto.getSessionOfStartDate());
			//bo.setSessionOfEndDate(dto.getSessionOfEndDate());
			bo.setCompOffReason(dto.getCompOffReason());
			//bo.setNoofdays(dto.getNoofdays());
			bo.setCompOffStatus(dto.isCompOffStatus());
			bo.setCreatedDate(dto.getCreatedDate());
			bo.setUpdatedDate(dto.getUpdatedDate());
			bo.setUpdatedBy(dto.getUpdatedBy());
			bo.setCreatedBy(dto.getCreatedBy());
			map = compOffCreditDao.saveCompOffDetails(bo);
			//sending email code
			emailInvoke(dto,input);
		} else {
			bo.setCompOffID(dto.getCompOffID());
			bo.setEmployeeID(dto.getEmployeeID());
			//bo.setCompOffStartDate(dto.getCompOffStartDate());
			//bo.setCompOffEndDate(dto.getCompOffEndDate());
			//bo.setSessionOfStartDate(dto.getSessionOfStartDate());
			//bo.setSessionOfEndDate(dto.getSessionOfEndDate());
			bo.setCompOffReason(dto.getCompOffReason());
			//bo.setNoofdays(dto.getNoofdays());
			bo.setCompOffStatus(dto.isCompOffStatus());
			bo.setUpdatedDate(dto.getUpdatedDate());
			bo.setUpdatedBy(dto.getUpdatedBy());
			map = compOffCreditDao.updateCompOffDetails(bo);
			//sending email code
			emailInvoke(dto,input);

		}
		return map;
	}
	
	public void emailInvoke(CompOffTransactionsDTO dto,List<Map<String, Object>> input){
		List<String> managerEmailList= new ArrayList<String>();
		String template=null;
		Map<String,Object> managerDetails=EmployeeDao.getManagerDetails(dto.getEmployeeID());
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee(dto.getEmployeeID());
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		double d=(double)dto.getNoofdays();
		float days=(float)d;
		for (Map<String, Object> email : input) {
			String leaveType = "a Compensatory-Off-Credit ";
			Map<String, Object> detail = new HashMap<String, Object>();
			detail.put("name", managerDetails.get("Name"));
			detail.put("sdate", dto.getCompOffStartDate());
			detail.put("edate", dto.getCompOffEndDate());
			detail.put("reason", dto.getCompOffReason());
			detail.put("duration", days);
			detail.put("leave", leaveType);
			detail.put("firstsession", dto.getSessionOfStartDate());
			detail.put("secondsession", dto.getSessionOfEndDate());
			String emailid = (String) email.get("emailID");
			if((int)email.get("employeeID")!=(int)managerDetails.get("EmployeeID")){
					managerEmailList.add((String)email.get("emailID"));
				}

			 template = EmailTemplate.submittedMailToManagerforWfh(detail, leaveAppliedBy);

		}
		String[] CCList = managerEmailList.toArray(new String[managerEmailList.size()]);

		emailer.sendMail((String)managerDetails.get("Email"),CCList, leaveAppliedBy + " has requested a Compensatory Off Credit", template);
	}
	
	

	@Override
	public boolean saveLeaveEmailRecipients(Map<String, Object> input) {
		LeaveEmailRecipientsTransactionsBO bo = new LeaveEmailRecipientsTransactionsBO();
		boolean status = false;
		Integer id = 0;
		List<Map<String, Object>> email = (List<Map<String, Object>>) input.get("teamId");
		for (Map<String, Object> empId : email) {
			id = (Integer) input.get("employeeId");
			bo.setCompOffID(Conversion.objectToInteger(input.get("compOffid")));
			bo.setTeamMemberID(Conversion.objectToInteger(empId.get("employeeID")));
			bo.setCreatedBy(id);
			bo.setUpdatedBy(id);
			bo.setCreatedDate(new Timestamp(new Date().getTime()));
			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			status = compOffCreditDao.saveLeaveEmailRecipients(bo);
		}

		return status;
	}

	@Override
	public boolean approveCompOff(Map<String, Object> input) {
		CompOffTransactionsBO bo = null;
		boolean status=false;
		
		bo= new CompOffTransactionsBO();
		
		bo.setCompOffID(Conversion.objectToInteger(input.get("leaveID")));
		//handle with input status ID
		bo.setCompOffStatus(Conversion.objectToInteger(input.get("leaveStatus")));
		
		bo.setUpdatedBy(Conversion.objectToInteger(input.get("approvedByID")));
		bo.setReasonForRejectionORRevoke((String)input.get("reasonForRejectionORRevoke"));
		bo.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("approvedByID")));
		bo.setUpdatedDate(new Timestamp(new Date().getTime()));
		status=compOffCreditDao.approveConmOff(bo);
		return status;
	}

	@Override
	public Map<String, Object> approveOrRejectNotification(Map<String, Object> input, String employeeName) {
		boolean flag=false;
		 Double nOfdays=0.0;
		String typeOfLeave="Compensatory-Off-Credit";
		//int d=(int)input.get("noOfDays");
		float days=Conversion.objectToFloat(input.get("noOfDays"));
		Map<String,Object> map=new HashMap<String,Object>();
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee(Conversion.objectToInteger(input.get("employeeID")));
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		String employeeEmail=(String)finalmap.get(0).get("emailID");
		String managerName=(String)finalmap.get(0).get("managerName");
		List<Map<String, Object>> managermap = EmployeeDao.getEmployee(Conversion.objectToInteger(input.get("approvedByID")));
		String Name=(String)managermap.get(0).get("empName");
		map.put("name", employeeName);
		map.put("employeeID", input.get("employeeID"));
		//map.put("leaveStatus", dto.getLeaveStatus());
		map.put("leaveType", input.get("leaveType"));
		map.put("reason", input.get("reasonForRejectionORRevoke"));
		map.put("managerName", managerName);
		map.put("typeOfLeave", typeOfLeave);
		map.put("aprovedBy", Name);
		map.put("firstsession", input.get("sessionOfStartDate"));
		map.put("secondsession",input.get("sessionOfEndDate"));
		try{
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = (String) input.get("leaveStartDate");
			String endDate = (String) input.get("leaveEndDate");
			Date sdate=sf.parse(startDate);
			Date edate=sf.parse(endDate);
			map.put("startDate", sdate);
			map.put("endDate", edate);
			
	     //nOfdays = (double) DateOperations.countNoOfDays(startDate, endDate,map);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		map.put("duration",days);
		int status=Conversion.objectToInteger(input.get("leaveStatus"));
		String leavestatus="";
		Map<String, Object> statusMap = new HashMap<String, Object>();
		if(status==1)
		{
			leavestatus="approved";
			map.put("leaveStatus", leavestatus);
			String template = EmailTemplate.approveMail(map,leaveAppliedBy,managerName);
			emailer.sendMail(employeeEmail,Name.trim()  +" has "+leavestatus+" your application "+typeOfLeave  , template);
			//flag=true;
			statusMap.put("status", leavestatus);
			statusMap.put("flag", flag);
		
			/*leavestatus="aproved";
			map.put("leaveStatus", leavestatus);*/
		}
		else{
			leavestatus="rejected";
			map.put("leaveStatus", leavestatus);
			String template = EmailTemplate.approveOrRejectMail(map,leaveAppliedBy,managerName);
			emailer.sendMail(employeeEmail,Name.trim()   +" has "+leavestatus+" your application "+typeOfLeave  , template);
			//flag=true;
			statusMap.put("status", leavestatus);
			statusMap.put("flag", flag);
			/*leavestatus="rejected";
			map.put("leaveStatus", leavestatus);*/
		}
		return map;
		/*String template = EmailTemplate.approveOrRejectMail(map,leaveAppliedBy,managerName);
		emailer.sendMail(managerEmail, managerName  +" has "+leavestatus+" your application for "+typeOfLeave  , template);*/
		
		
	}

	@Override
	public void remindLeaveRequest(Map<String, Object> input, float nOfdays) throws Exception {
		String leaveType="Compensatory-Off-Credit";
		Map<String, Object> detail=new HashMap<String, Object>();
		//int d=(int)input.get("noOfDays");
		float days=Conversion.objectToFloat(input.get("noOfDays"));
		detail.put("name", input.get("employeeName"));
		detail.put("sdate", input.get("leaveStartDate"));
		detail.put("edate", input.get("leaveEndDate"));
		detail.put("reason", input.get("reasonForLeave"));
		detail.put("leaveType", input.get("leaveType"));
		detail.put("duration",days);
		detail.put("leave", leaveType);
		detail.put("firstsession", input.get("sessionOfStartDate"));
		detail.put("secondsession", input.get("sessionOfEndDate"));
		
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee((Integer)input.get("employeeID"));
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		String managerEmail=(String)finalmap.get(0).get("managerEmail");
		String managerName=(String)finalmap.get(0).get("managerName");
		int gender=(int) finalmap.get(0).get("gender");
		
		String genderChar="";
		if(gender==1){
			genderChar="his";
		}else{
			genderChar="her";
		}
		detail.put("gender", genderChar);
		List<Map<String, Object>> seniorMgrsMailMap = employeeLeaveDisplayDao.getImmediateSeniorManger(Conversion.objectToInteger(input.get("employeeID")));
		for(Map<String,Object> names:seniorMgrsMailMap)
		{
			String srMgrName=(String)names.get("empName");
			String srMgremail=(String)names.get("email");
			String template = EmailTemplate.followUPMailByManagerToRemindEmployee(detail,leaveAppliedBy,srMgrName);
			emailer.sendMail(srMgremail, "Reminder � "+leaveAppliedBy  +"'s "+leaveType+" application."  , template);
		}
		
		
		/*String template = EmailTemplate.followUPMailByManagerToRemindEmployee(detail,leaveAppliedBy,managerName);
		emailer.sendMail(managerEmail, leaveAppliedBy  +" wants to remind you that "+genderChar+" "+ leaveType+" application is still pending."  , template);*/
		
	}

	@Override
	public Double countNoOfDays(Date sdate, Date edate, Integer startSession, Integer endSession) {
		
		Double days=compOffCreditDao.countNoOfDays(sdate,edate,startSession,endSession);
			return days;
		
	}

	@Override
	public boolean saveCompOffDetailsWithProc(CompOffTransactionsDTO dto, List<Map<String, Object>> input,
			Map<String, Object> saveEmail) throws Exception {
		
		CompOffTransactionsBO bo = new CompOffTransactionsBO();
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> dataMapForEmails=null;
		List emailRecipeintsList=new ArrayList<>();
		List finalEmailRecipeintsList=new ArrayList<>();
		List compOffDataList=new ArrayList<>();
		Map emailMap=null;
		Map compOffMap=null;
		boolean status = false;
		Integer id = 0;
		List<Map<String, Object>> email = (List<Map<String, Object>>) saveEmail.get("teamId");
		for (Map<String, Object> empId : input) {
			id = (Integer) empId.get("employeeId");
			emailMap= new LinkedHashMap<>();
			emailMap.put("teamMemberID", Conversion.objectToInteger(empId.get("employeeID")));
			dataMapForEmails = new HashMap<String, Object>();
			dataMapForEmails.put("data", emailMap);
			finalEmailRecipeintsList.add(dataMapForEmails);
			//emailRecipeintsList.add(emailMap);
			
		}

		if (dto.getCompOffID() == null) {

		
			compOffMap= new LinkedHashMap<>();
			compOffMap.put("employeeID", dto.getEmployeeID());
			compOffMap.put("compOffStartDate", dto.getCompOffStartDate());
			compOffMap.put("compOffEndDate", dto.getCompOffEndDate());
			compOffMap.put("compOffreason", dto.getCompOffReason());
			compOffMap.put("sessionOfStartDate", dto.getSessionOfStartDate());
			compOffMap.put("sessionOfendDate", dto.getSessionOfEndDate());
			compOffMap.put("createdBy", dto.getUpdatedBy());
			compOffMap.put("updatedBy", dto.getCreatedBy());
			Map finalMap= new LinkedHashMap();
			finalMap.put("data", compOffMap);
			compOffDataList.add(finalMap);
			//map = compOffCreditDao.saveCompOffDetails(bo);
			//sending email code
			status=compOffCreditDao.saveCompOffData(compOffDataList,finalEmailRecipeintsList);
			emailInvoke(dto,input);
		} else {
		
			compOffMap= new LinkedHashMap<>();
			compOffMap.put("employeeID", dto.getEmployeeID());
			compOffMap.put("compOffStartDate", dto.getCompOffStartDate());
			compOffMap.put("compOffEndDate", dto.getCompOffEndDate());
			compOffMap.put("compOffreason", dto.getCompOffReason());
			compOffMap.put("sessionOfStartDate", dto.getSessionOfStartDate());
			compOffMap.put("sessionOfendDate", dto.getSessionOfEndDate());
			compOffMap.put("createdBy", dto.getUpdatedBy());
			compOffMap.put("updatedBy", dto.getCreatedBy());
			Map finalMap= new LinkedHashMap();
			finalMap.put("data", compOffMap);
			compOffDataList.add(finalMap);
			//map = compOffCreditDao.updateCompOffDetails(bo);
			status=compOffCreditDao.saveCompOffData(compOffDataList,finalEmailRecipeintsList);
			//sending email code
			emailInvoke(dto,input);

		}
		return status;
	}

	

}
