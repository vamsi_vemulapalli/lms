package com.merilytics.service;

import java.util.List;
import java.util.Map;

public interface EmployeeDetailsService {
	public List<Map<String, Object>> getEmployeeDetails();

	public List<Map<String, Object>> getEmployee(Integer empId);

	public List<Map<String, Object>> getCurrentLeaveBalance(Integer empId);

	public List<Map<String, Object>> getEmployeeTeamDetails(Integer empId,int startPos,int PAGE_SIZE,String search);

	public List<Map<String, Object>> getEmployeeMeDetails(Integer empId,int startPos,int PAGE_SIZE,String search);

	public List<Map<String, Object>> getEmployeeHistory(Integer empId,int startPos,int PAGE_SIZE,String search);

	public List<Map<String, Object>> getTeamHistory(Integer empId,int startPos,int PAGE_SIZE,String search);

	public List<Map<String, Object>> yearToDateStatus(Integer empId);

	public List<Map<String, Object>> monthToDateStatus(Integer empId);

	public Map<String, Object> checkAdmin(Integer empId);

	public List<Map<String, Object>> getUserScreensList(Integer empId);
	
	public List<Map<String, Object>> paidLeaveHyperLinkMTD(Integer empId);
	
	public List<Map<String, Object>> paidLeaveHyperLinkYTD(Integer empId);

	public List<Map<String, Object>> getEmployeeTeamDetailsMobApproval(Integer empId, int startPos, int PAGE_SIZE,
			String search, Integer filter);

	public List<Map<String, Object>> getEmployeeTeamDetailsMobReview(Integer empId, int startPos, int PAGE_SIZE,
			String search,Integer isCompleted);

	

	List<Map<String, Object>> getEmployeeHistoryMobile(Integer empId, int startPos, int PAGE_SIZE, String search,
			String leaveType);

	List<Map<String, Object>> getTeamHistoryMobile(Integer empId, int startPos, int PAGE_SIZE, String search,
			String leaveType);

	List<Map<String, Object>> getLeaveCom(Integer empId);

	List<Map<String, Object>> monthToDateStatusMobile(Integer empId, Integer year, Integer month);
	
	
	
	

}
