package com.merilytics.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.dao.EmployeeDetailsDao;

@Service
public class EmployeeDetailsServiceImpl implements EmployeeDetailsService {

	@Autowired
	private EmployeeDetailsDao EmployeeDao;

	@Override
	public List<Map<String, Object>> getEmployeeDetails() {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployeeDetails();
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getEmployee(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployee(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getCurrentLeaveBalance(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getCurrentLeaveBalance(empId);
		return listofData;
	}
	
	@Override
	public List<Map<String, Object>> getLeaveCom(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getleaeveComments(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getEmployeeTeamDetails(Integer empId, int startPos, int PAGE_SIZE, String search) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployeeTeamDetails(empId, startPos, PAGE_SIZE, search);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getEmployeeTeamDetailsMobApproval(Integer empId, int startPos, int PAGE_SIZE,
			String search,Integer filter) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployeeTeamDetailsMobileApproval(empId, startPos, PAGE_SIZE, search,filter);
listofData.forEach(record->{
			
			List<Map<String, Object>> rec =	EmployeeDao.getleaeveReciepeints((Integer)record.get("leaveID"),record.get("leaveType").toString());
			
			record.put("recep", rec);
		});
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getEmployeeTeamDetailsMobReview(Integer empId, int startPos, int PAGE_SIZE,
			String search,Integer isCompleted) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployeeTeamDetailsMobileReview(empId, startPos, PAGE_SIZE, search,isCompleted);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getEmployeeMeDetails(Integer empId, int startPos, int PAGE_SIZE, String search) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployeeMeDetails(empId, startPos, PAGE_SIZE, search);
listofData.forEach(record->{
			
			List<Map<String, Object>> rec =	EmployeeDao.getleaeveReciepeints((Integer)record.get("leaveID"),record.get("leaveType").toString());
			
			record.put("recep", rec);
		});
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getEmployeeHistory(Integer empId, int startPos, int PAGE_SIZE, String search) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployeeHistory(empId, startPos, PAGE_SIZE, search);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getEmployeeHistoryMobile(Integer empId, int startPos, int PAGE_SIZE, String search,
			String leaveType) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getEmployeeHistoryMobileFilter(empId, startPos, PAGE_SIZE, search, leaveType);
		listofData.forEach(record -> {

			List<Map<String, Object>> rec = EmployeeDao.getleaeveReciepeints((Integer) record.get("leaveID"),record.get("leaveType").toString());

			record.put("recep", rec);
		});
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getTeamHistory(Integer empId, int startPos, int PAGE_SIZE, String search) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getTeamHistory(empId, startPos, PAGE_SIZE, search);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getTeamHistoryMobile(Integer empId, int startPos, int PAGE_SIZE, String search,
			String leaveType) {
		System.out.println("in service"+empId);
		
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getTeamHistoryMobile(empId, startPos, PAGE_SIZE, search, leaveType);
		System.out.println("listofData----"+listofData);
		listofData.forEach(record -> {

			List<Map<String, Object>> rec = EmployeeDao.getleaeveReciepeints((Integer) record.get("leaveID"),record.get("leaveType").toString());
System.out.println("inloop----"+rec);
			record.put("recep", rec);
		});
		return listofData;
	}

	@Override
	public List<Map<String, Object>> yearToDateStatus(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.yearToDateStatus(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> monthToDateStatus(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.monthToDateStatus(empId);
		return listofData;
	}
	@Override
	public List<Map<String, Object>> monthToDateStatusMobile(Integer empId,Integer year,Integer month) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.monthToDateStatusMobile(empId,year,month);
		return listofData;
	}
	@Override
	public Map<String, Object> checkAdmin(Integer empId) {
		Map<String, Object> listofData = new HashMap<String, Object>();
		listofData = EmployeeDao.checkAdmin(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getUserScreensList(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.getUserScreensList(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> paidLeaveHyperLinkMTD(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.paidLeaveHyperLinkMTD(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> paidLeaveHyperLinkYTD(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = EmployeeDao.paidLeaveHyperLinkYTD(empId);
		return listofData;
	}

}
