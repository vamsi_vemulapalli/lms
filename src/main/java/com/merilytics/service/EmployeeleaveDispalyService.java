package com.merilytics.service;

import java.util.List;
import java.util.Map;

public interface EmployeeleaveDispalyService {
	public List<Map<String, Object>> getEmailRecipientsList(Integer empId);
	public List<Map<String, Object>> getDefaultEmailRecipients(Integer empId);
	public boolean cancelLeaveByEmployee(Map<String, Object> input) throws Exception;
	public List<Map<String, Object>>  individualEmployeeCompOffCreditList(Integer empId);
	public List<Map<String, Object>> getUnpaidLeaveHyperLinKYearToDate(Integer empId);
	public List<Map<String, Object>> getUnpaidLeaveHyperLinKMonthToDate(Integer empId);
	
	public List<Map<String, Object>> getcalendarDaysDisplay(Map<String,Object> map);
	List<Map<String, Object>> getholidaysList(Map<String, Object> map);
	
	public List<Map<String, Object>> getEmployeeStatusDisplay(Integer empId);
	public boolean updateEmploymentStatus(Map<String, Object> map);
	public List<Map<String, Object>> getStatusPeriods(Integer empId);
	public List<Map<String, Object>> getUserScreens(Integer empId);
	public List<Map<String, Object>> getseniorMangersList(Integer empId);
	public List<Map<String, Object>> getImmediateSeniorManger(Integer empId);
	
	public List<Map<String, Object>> getConversionTypes(Integer empID);
	public Map<String, Object> getLoginNotification(Integer empId);
	public List<Map<String, Object>> getEmpStatus();
	List<Map<String, Object>> getholidaysListMobile(Map<String, Object> map);
	List<Map<String, Object>> getpunchDetails(Map data);


}
