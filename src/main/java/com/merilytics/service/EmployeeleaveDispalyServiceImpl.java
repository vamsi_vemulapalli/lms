package com.merilytics.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.bo.CompOffTransactionsBO;
import com.merilytics.bo.EmployeeLeaveTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;
import com.merilytics.dao.EmployeeLeaveDisplayDao;

@Service
public class EmployeeleaveDispalyServiceImpl implements EmployeeleaveDispalyService {
	@Autowired
	private EmployeeLeaveDisplayDao employeeLeaveDisplayDao;

	@Override
	public List<Map<String, Object>> getEmailRecipientsList(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getEmailRecipientsList(empId);
		return listofData;
	}

	@Override
	public boolean cancelLeaveByEmployee(Map<String, Object> input) throws Exception {
		boolean status = false;
		String data = (String) input.get("leaveType");
		if (data.equalsIgnoreCase("Work from Home")) {
			WorkFromHomeTransactionsBO bo = new WorkFromHomeTransactionsBO();

			bo.setWorkFromHomeID((Integer) input.get("leaveID"));
			bo.setEmployeeID((Integer) input.get("employeeID"));
			// System.out.println(input.get("leaveStatus").toString());
			bo.setWorkFromHomeStatus(4);
			bo.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
			bo.setUpdatedBy((Integer) input.get("employeeID"));
			bo.setApprovedByORRejectedBy((Integer) input.get("employeeID"));
			status = employeeLeaveDisplayDao.cancelWfhByEmployee(bo);

		} else if (data.equalsIgnoreCase("Compensatory Off Credit")) {
			CompOffTransactionsBO bo = new CompOffTransactionsBO();
			bo.setCompOffID((Integer) input.get("leaveID"));
			bo.setEmployeeID((Integer) input.get("employeeID"));
			bo.setCompOffStatus(4);
			bo.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
			bo.setUpdatedBy((Integer) input.get("employeeID"));
			bo.setApprovedByORRejectedBy((Integer) input.get("employeeID"));
			status = employeeLeaveDisplayDao.cancelCompoffEmployee(bo);
		} else {
			// System.out.println(input.get("leaveStatus").toString());
			EmployeeLeaveTransactionsBO bo = new EmployeeLeaveTransactionsBO();
			bo.setLeaveID((Integer) input.get("leaveID"));
			bo.setEmployeeID((Integer) input.get("employeeID"));
			bo.setLeaveStatus(4);
			bo.setReasonForRejectionORRevoke((String) input.get("reasonForRejectionORRevoke"));
			bo.setUpdatedBy((Integer) input.get("employeeID"));
			bo.setApprovedByORRejectedBy((Integer) input.get("employeeID"));

			// bo.setUpdatedDate(new Timestamp(new Date().getTime()));

			status = employeeLeaveDisplayDao.cancelLeaveByEmployee(bo);
		}
		return status;
	}

	@Override
	public List<Map<String, Object>> getDefaultEmailRecipients(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getDefaultEmailRecipients(empId);
		// remove second email recipient if L1 manager is paavan
		Set<Map<String, Object>> hs = new HashSet<Map<String, Object>>();
		hs.addAll(listofData);
		if (hs.size() == 1) {
			listofData.clear();

			listofData.addAll(hs);
		}

		return listofData;
	}

	@Override
	public List<Map<String, Object>> individualEmployeeCompOffCreditList(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.individualEmployeeCompOffCreditList(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getUnpaidLeaveHyperLinKYearToDate(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getUnpaidLeaveHyperLinKYearToDate(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getUnpaidLeaveHyperLinKMonthToDate(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getUnpaidLeaveHyperLinKMonthToDate(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getcalendarDaysDisplay(Map<String, Object> map) {

		/*
		 * List<Map<String, Object>> listofData = new ArrayList<Map<String,
		 * Object>>(); HashSet<Integer> set = new HashSet<Integer>(); listofData
		 * = employeeLeaveDisplayDao.getcalendarDaysDisplay(map);
		 * System.out.println(listofData); List<Map<String, Object>>
		 * listofFinalData = new ArrayList<Map<String, Object>>(); for
		 * (Map<String, Object> innerMap : listofData) { set.add((Integer)
		 * innerMap.get("week_number")); int week = (Integer)
		 * innerMap.get("week_number"); for (Integer s : set) {
		 * 
		 * Map<String, Object> inMap = new HashMap<String, Object>(); if (s ==
		 * week) { inMap.put("week_number", innerMap.get("week_number"));
		 * inMap.put("dates", innerMap.get("daynumber")); inMap.put("day",
		 * innerMap.get("day")); inMap.put("weekname",
		 * innerMap.get("weekname"));
		 * 
		 * } listofFinalData.add(inMap); }
		 * 
		 * }
		 * 
		 * System.out.println(set);
		 */

		List<Map<String, Object>> listorder = employeeLeaveDisplayDao.getcalendarDaysDisplay(map);
		Set<String> weeks = new LinkedHashSet<String>();
		Set<String> dayName = new LinkedHashSet<String>();
		List<Map<String, Object>> finallist = new ArrayList<>();

		listorder.forEach(internalmap -> {
			weeks.add(internalmap.get("week_number").toString());
			dayName.add(internalmap.get("weekname").toString());
		});

		weeks.forEach(week -> {

			Map<String, Object> internal = new LinkedHashMap<>();

			listorder.forEach(internalmap -> {

				if (week.equalsIgnoreCase(internalmap.get("week_number").toString())) {
					internal.put(internalmap.get("weekname").toString(), internalmap);
					if (internal.get("week_number") == null) {
						internal.put("week_number", internalmap.get("week_number"));
					}

				}
			});
			finallist.add(internal);
		});

		return finallist;
	}

	@Override
	public List<Map<String, Object>> getholidaysList(Map<String, Object> map) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getHolidaysList(map);
		return listofData;
	}
	@Override
	public List<Map<String, Object>> getholidaysListMobile(Map<String, Object> map) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getHolidaysListMobile(map);
		return listofData;
	}
	@Override
	public List<Map<String, Object>> getEmployeeStatusDisplay(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getEmployeeStatusDisplay(empId);
		return listofData;
	}

	@Override
	public boolean updateEmploymentStatus(Map<String, Object> map) {

		boolean status = employeeLeaveDisplayDao.updateEmploymentStatus(map);
		return status;

	}

	@Override
	public List<Map<String, Object>> getStatusPeriods(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getStatusPeriods(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getUserScreens(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getUserScreens(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getseniorMangersList(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getseniorMangersList(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getImmediateSeniorManger(Integer empId) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getImmediateSeniorManger(empId);
		return listofData;
	}

	@Override
	public List<Map<String, Object>> getConversionTypes(Integer empID) {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getConversionTypes(empID);
		return listofData;
	}
	
	@Override
	public List<Map<String, Object>> getEmpStatus() {
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		listofData = employeeLeaveDisplayDao.getEmployementStatus();
		return listofData;
	}

	@Override
	public Map<String, Object> getLoginNotification(Integer empId) {
		Map<String, Object> listofData = new HashMap<String, Object>();
		listofData = employeeLeaveDisplayDao.getLoginNotification(empId);
		return listofData;
	}
	@Override
	public List<Map<String, Object>> getpunchDetails(Map data) {
		
		return employeeLeaveDisplayDao.getpunchDetails(data);
	}
}
