package com.merilytics.service;

import java.util.Date;
import java.util.Map;

public interface LeaveValidationsService {
	public Map<String,Object> validationCheckForLeave(int empID,double noOFDays,Date eDate,Date sDate,int leaveTypeID,Integer startSession,Integer endSession);
	public Map<String,Object> validationCheckForCompOff(int empID,double noOFDays,Date sDate,Date eDate,Integer startSession,Integer endSession);
	public Map<String, Object> validationCheckForWFH(int empID,Date eDate, Date sDate,Float days,Integer startSession,Integer endSession);
}
