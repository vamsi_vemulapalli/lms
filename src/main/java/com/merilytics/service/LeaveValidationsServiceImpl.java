package com.merilytics.service;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.dao.LeaveValidationsDAO;
@Service
public class LeaveValidationsServiceImpl implements LeaveValidationsService {
	@Autowired
	private LeaveValidationsDAO leaveValidationsDao;
	@Override
	public Map<String, Object> validationCheckForLeave(int empID, double noOFDays,Date eDate,Date sDate, int leaveTypeID,Integer startSession,Integer endSession) {
		
		return leaveValidationsDao.validationCheckForLeave(empID, noOFDays, leaveTypeID,sDate,eDate,startSession,endSession);
	}
	@Override
	public Map<String, Object> validationCheckForCompOff(int empID, double noOFDays,Date sDate,Date eDate,Integer startSession,Integer endSession) {
		return leaveValidationsDao.validationCheckForCompOff(empID, noOFDays,sDate,eDate,startSession,endSession);
	}
	
	
	@Override
	public Map<String, Object> validationCheckForWFH(int empId,Date sDate, Date eDate,Float days,Integer startSession,Integer endSession) {
		return leaveValidationsDao.validationCheckForWFH(empId,sDate, eDate,days,startSession,endSession);
	}

}
