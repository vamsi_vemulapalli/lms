package com.merilytics.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.dao.UserListDAO;
@Service
public class MobileAPIServiceImpl implements MobileAPIService {

	@Autowired
	private UserListDAO userListDAO;
	
	@Override
	public Map<String, Object> getPunchTime(Map map) throws Exception {
		
		return userListDAO.MobileAPIPunchTimings(map);
	}

}
