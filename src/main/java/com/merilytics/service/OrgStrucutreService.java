package com.merilytics.service;

import java.util.List;
import java.util.Map;

public interface OrgStrucutreService {

	Map<String, Object> getEmployeeOrgStrucute( int empID) throws Exception;

	public List<Map<String, Object>> getEmployeeOrgStrucuteDropDown(int empID) throws Exception;

}
