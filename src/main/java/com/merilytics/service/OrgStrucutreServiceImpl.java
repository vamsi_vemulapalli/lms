package com.merilytics.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Named;

import com.merilytics.dao.OrgStrucutreDAO;
import com.merilytics.util.DAP;

@Named
public class OrgStrucutreServiceImpl implements OrgStrucutreService {

	@Resource
	private OrgStrucutreDAO orgStrucutreDAO;

	@Override
	public Map<String, Object> getEmployeeOrgStrucute(int empID) throws Exception {

		List<Map<String, Object>> empolyeeList = orgStrucutreDAO.getEmployeeOrgStrucute( empID);
		
		Map<String, Object> internalmap = empolyeeList.get(0);
		
		Map<String, Object> finalmap = new HashMap<String, Object>();
	
		finalmap.put("employeeID", internalmap.get("EmployeeID"));
		finalmap.put("employeeName", internalmap.get("name"));
		finalmap.put("designation", internalmap.get("designation"));
		finalmap.put("year", internalmap.get("dateOfJoining"));
		finalmap.put("emailID", internalmap.get("emailID"));

		/*Map<String, Object> internalMap = new HashMap<String, Object>();
		
		internalMap.put("date", internalmap.get("date"));
		internalMap.put("DAPID", internalmap.get("DAPID"));
		internalMap.put("month", internalmap.get("month"));
		internalMap.put("year", internalmap.get("year"));
		internalMap.put("hasWorkingManagerApproved", internalmap.get("hasWorkingManagerApproved"));
		internalMap.put("monthName", internalmap.get("monthName"));
		internalMap.put("dapCount", internalmap.get("dapCount"));
		internalMap.put("DAPDataID", internalmap.get("DAPDataID"));
		internalMap.put("status", internalmap.get("status"));
		internalMap.put("isEmployeeSubmited", internalmap.get("isEmployeeSubmited"));
		internalMap.put("isManager", internalmap.get("isManager"));
		*/

		List<Map<String, Object>> listofemployee = DAP.getemployeeReporting(empolyeeList,
				internalmap.get("EmployeeID").toString());

		if (!listofemployee.isEmpty()) {
			finalmap.put("reportees", listofemployee);
		}

		/*finalmap.put("monthlyComp", internalMap);*/

		return finalmap;
	}

	@Override
	public List<Map<String, Object>> getEmployeeOrgStrucuteDropDown(int empID) throws Exception {
		try {

			List<Map<String, Object>> empolyeeList = orgStrucutreDAO.getEmployeeOrgStrucuteDropDown(empID);
			return empolyeeList;
		} catch (Exception e) {
			throw e;
		}

	}

}
