package com.merilytics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.merilytics.dao.TaskSchedulerDAO;
import com.merilytics.mail.EmailTemplate2;
import com.merilytics.util.Conversion;
import com.merilytics.util.Loggers;
import com.merilytics.util.TaskSchedulerEmails;
@Configuration
@EnableAsync
@EnableScheduling
public class TaskSchedulerServiceImpl  {
	@Autowired
	private TaskSchedulerDAO taskSchedulerDAO;
	
	@Resource
	private EmailTemplate2 emailer;
	//@Scheduled(cron="0 0 2 * * ?")
	public void doSomething() {
		// something that should execute periodically

		try{
		taskInvoke();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/*@Scheduled(cron="0 0 1 * * ?")
	public void changeEmployeementStatus() {
		// something that should execute periodically

		System.out.println("yay.... i am enjoying here");
		try{
			changeEmployeeStatus();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	/*
	 * @Bean public TaskScheduler poolScheduler() { ThreadPoolTaskScheduler
	 * scheduler = new ThreadPoolTaskScheduler();
	 * scheduler.setThreadNamePrefix("poolScheduler"); scheduler.setPoolSize(10);
	 * return scheduler; }
	 */
	public  void changeEmployeeStatus() {
		Map<String,Object> data= null;
		try{
			data=taskSchedulerDAO.changeEmployeeMentStatusAuto();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public  void taskInvoke() {
		// get Session
         Map<String,Object> emailCredentials=null;
         
         NativeQuery query = null;
 		List<Map<String, Object>> list = null;
 		List<Map<String, Object>> managerDetails = null;
 		List<String> managerEmailList= null;
		  try{
				list = taskSchedulerDAO.getAutomationdetails();
				emailCredentials=taskSchedulerDAO.getEmailCredentials();
				//int i = 0;
				for(Map map:list){
					SimpleDateFormat myFormat =new SimpleDateFormat("MMM dd, yyyy");
					String inputString1 = ""+map.get("leaveDate");
					 Date date = null;
					try {
						date = new SimpleDateFormat("yyyy-MM-dd").parse(inputString1);
					} catch (ParseException e1) {
						e1.printStackTrace();
					}  
					  
				    String convertedDate = myFormat.format(date);
				    
				    
					//preparing content for mail
				    String sessionDetails=null;
				    Double daySession=(Double) map.get("noOfDays");
				    String sessionType= (String) map.get("sessionType");
				    if(daySession==1.0){
				    	sessionDetails="full day leave";
				    }else{
				    	sessionDetails="half day leave("+sessionType+")";

				    }
				    String punchIn = Conversion.objectToString(map.get("fromDateTime"));
				    String punchOut = Conversion.objectToString(map.get("toDateTime"));
				    if(punchIn!=null&&punchOut!=null){
				    SimpleDateFormat requiredFormat =new SimpleDateFormat("h:mm:ss a");
					 Date punchInDate = null;
					 Date punchOutDate = null;
					try {
						punchInDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(punchIn);
						punchOutDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(punchOut);
					} catch (ParseException e1) {
						e1.printStackTrace();
					}  
				    
					punchIn=requiredFormat.format(punchInDate);
					punchOut=requiredFormat.format(punchOutDate);
				    }else{
				    	punchIn="No timestamp recorded";
						punchOut="No timestamp recorded";
				    }
					//emails to manager
					managerDetails=taskSchedulerDAO.getSiloManager( (int)map.get("empId"));
					
					managerEmailList= new ArrayList<String>();
					for(Map details:managerDetails){
						if((int)details.get("employeeID")!=(int)map.get("empId")){
							managerEmailList.add((String)details.get("emailID"));
							//System.out.println("in for loop"+(String)details.get("emailID"));
							//emailContent=EmailTemplate.emailToManager((String)map.get("empName"),""+convertedDate, (String)details.get("employeeName"));
							//EmailAlert.sendEmailAlert(emailContent, (String)details.get("emailID"), map.get("empName")+" has been marked as absent on "+convertedDate, emailCredentials);
						}
					}
					String[] CCList = managerEmailList.toArray(new String[managerEmailList.size()]);
					//send mail to employee and manager
					String emailContent=TaskSchedulerEmails.emailToEmployeeWithMoreInfo((String)map.get("empName"),convertedDate,sessionDetails,punchIn,punchOut);
					//EmailAlert.sendEmailAlert(emailContent, (String)map.get("emailID"),CCList, map.get("empName")+" has been marked as absent on "+convertedDate, emailCredentials);
					emailer.sendMail((String)map.get("emailID"), CCList, map.get("empName")+" has been marked as absent on "+convertedDate, emailContent);
				/*
				 * ++i; if(i==25) { Thread.currentThread().sleep(60000); i=0; }
				 */
					Thread.sleep(3000);
				}
				//Iterate and send email's
				//write logs here
				if(list.size()==0||list==null){
				Loggers.TASK_LOGGER.info("no data returned from proc");
				}else{
					Loggers.TASK_LOGGER.info(list.size()+"---records returned from proc");
				}
		    }
		  catch(Exception e){
			  Loggers.TASK_LOGGER.info(e.getStackTrace());
			  e.printStackTrace();
		  } 
		  
		
		  
	}
	
	
}
