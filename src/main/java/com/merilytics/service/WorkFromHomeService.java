package com.merilytics.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.merilytics.dto.WorkFromHomeTransactionsDTO;

public interface WorkFromHomeService {
	public Map<String, Object> saveWfhDetails(WorkFromHomeTransactionsDTO dto,List<Map<String, Object>> input) throws Exception;
	public boolean saveLeaveEmailRecipients(Map<String, Object> input);
	
	public Map<String, Object> approveOrRejectNotification(Map<String, Object> input,String employeeName);
	
	public void remindLeaveRequest(Map<String, Object> input,float nOfdays) throws Exception;
	
	
	public boolean approveWFH(Map<String, Object> input);
	public Double countNoOfDays(Date sdate,Date edate,Integer startSession,Integer endSession,Integer leaveTypeID);
}
