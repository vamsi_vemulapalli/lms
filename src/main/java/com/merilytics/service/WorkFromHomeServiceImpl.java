package com.merilytics.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.merilytics.bo.LeaveEmailRecipientsTransactionsBO;
import com.merilytics.bo.WorkFromHomeTransactionsBO;
import com.merilytics.dao.ApplyWfhDao;
import com.merilytics.dao.EmployeeDetailsDao;
import com.merilytics.dao.EmployeeLeaveDisplayDao;
import com.merilytics.dto.WorkFromHomeTransactionsDTO;
import com.merilytics.mail.EmailTemplate2;
import com.merilytics.util.Conversion;
import com.merilytics.util.EmailTemplate;

@Service
public class WorkFromHomeServiceImpl implements WorkFromHomeService {
	@Autowired
	private ApplyWfhDao applyWfhdao;
	@Autowired
	private EmployeeDetailsDao EmployeeDao;
	@Resource
	private EmailTemplate2 emailer;
	
	@Autowired
	private EmployeeLeaveDisplayDao employeeLeaveDisplayDao;

	@Override
	public  Map<String, Object>  saveWfhDetails(WorkFromHomeTransactionsDTO dto,List<Map<String, Object>> input) throws Exception {
		
		WorkFromHomeTransactionsBO bo=new WorkFromHomeTransactionsBO();
		Map<String, Object> map = new HashMap<String, Object>();
		boolean status = false;
		/*for(Map<String, Object> email:input)
		{
			String leaveType="approval to Work From Home";
			Map<String, Object> detail=new HashMap<String, Object>();
			detail.put("name", email.get("employeeName"));
			detail.put("sdate", dto.getStartDate());
			detail.put("edate", dto.getEndDate());
			detail.put("reason", dto.getReasonForWorkFromHome());
			detail.put("duration", dto.getNoOfDays());
			detail.put("leave", leaveType);
			detail.put("firstsession", dto.getSessionOfStartDate());
			detail.put("secondsession", dto.getSessionOfEndDate());
			
			String emailid=(String)email.get("emailID");
			List<Map<String, Object>> finalmap = EmployeeDao.getEmployee(dto.getEmployeeID());
			String leaveAppliedBy=(String)finalmap.get(0).get("empName");
			
			String template = EmailTemplate.submittedMailToManagerforWfh(detail,leaveAppliedBy);
			emailer.sendMail(emailid, leaveAppliedBy +"would like approval to Work From Home", template);
		}*/
		
		
		if(dto.getWorkFromHomeID()==null)
		{
			bo.setEmployeeID(dto.getEmployeeID());
			bo.setNoOfDays(dto.getNoOfDays());
			bo.setReasonForWorkFromHome(dto.getReasonForWorkFromHome());
			bo.setStartDate(dto.getStartDate());
			bo.setSessionOfStartDate(dto.getSessionOfStartDate());
			bo.setEndDate(dto.getEndDate());
			bo.setSessionOfEndDate(dto.getSessionOfEndDate());
			bo.setWorkFromHomeStatus(dto.getWorkFromHomeStatus());
			bo.setUpdatedBy(dto.getUpdatedBy());
			bo.setCreatedBy(dto.getCreatedBy());
			bo.setCreatedDate(dto.getCreatedDate());
			bo.setUpdatedDate(dto.getUpdatedDate());
			bo.setApprovedByORRejectedBy(dto.getApprovedByORRejectedBy());
			bo.setReasonForRejectionORRevoke(dto.getReasonForRejectionORRevoke());
			bo.setIsApproveSLT(dto.getIsApproveSLT());
			bo.setIsAutomated(dto.getIsAutomated());
			
			map =applyWfhdao.saveWfhDetails(bo);
			//sending email code
			emailInvoke(dto,input);
		}
		else
		{
			bo.setWorkFromHomeID(dto.getWorkFromHomeID());
			bo.setEmployeeID(dto.getEmployeeID());
			bo.setNoOfDays(dto.getNoOfDays());
			bo.setReasonForWorkFromHome(dto.getReasonForWorkFromHome());
			bo.setStartDate(dto.getStartDate());
			bo.setSessionOfStartDate(dto.getSessionOfStartDate());
			bo.setEndDate(dto.getEndDate());
			bo.setSessionOfEndDate(dto.getSessionOfEndDate());
			bo.setWorkFromHomeStatus(dto.getWorkFromHomeStatus());
			bo.setUpdatedDate(dto.getUpdatedDate());
			bo.setUpdatedBy(dto.getUpdatedBy());
			bo.setApprovedByORRejectedBy(dto.getApprovedByORRejectedBy());
			bo.setReasonForRejectionORRevoke(dto.getReasonForRejectionORRevoke());
			bo.setIsApproveSLT(dto.getIsApproveSLT());
			bo.setIsAutomated(dto.getIsAutomated());
			map =applyWfhdao.updateWfhDetails(bo);
			//sending email code
			emailInvoke(dto,input);
		}
		return map;

	}
	
	
	@Override
	public Double countNoOfDays(Date sdate, Date edate,Integer startSession,Integer endSession,Integer leaveTypeID) {
		Double days=applyWfhdao.countNoOfDays(sdate,edate,startSession,endSession,leaveTypeID);
		return days;
	}
	
	public void emailInvoke(WorkFromHomeTransactionsDTO dto,List<Map<String, Object>> input){
		List<String> managerEmailList= new ArrayList<String>();
		String template=null;
		Map<String,Object> managerDetails=EmployeeDao.getManagerDetails(dto.getEmployeeID());
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee(dto.getEmployeeID());
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		
		for(Map<String, Object> email:input)
		{
			String leaveType="approval to Work from Home";
			Map<String, Object> detail=new HashMap<String, Object>();
			detail.put("name", managerDetails.get("Name"));
			detail.put("sdate", dto.getStartDate());
			detail.put("edate", dto.getEndDate());
			detail.put("reason", dto.getReasonForWorkFromHome());
			detail.put("duration", dto.getNoOfDays());
			detail.put("leave", leaveType);
			detail.put("firstsession", dto.getSessionOfStartDate());
			detail.put("secondsession", dto.getSessionOfEndDate());
			
			if((int)email.get("employeeID")!=(int)managerDetails.get("EmployeeID")){
					managerEmailList.add((String)email.get("emailID"));
				}
			
			
			 template = EmailTemplate.submittedMailToManagerforWfh(detail,leaveAppliedBy);
		}
		String[] CCList = managerEmailList.toArray(new String[managerEmailList.size()]);
		emailer.sendMail((String)managerDetails.get("Email"),CCList, leaveAppliedBy +" would like approval to Work from Home ", template);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public boolean saveLeaveEmailRecipients(Map<String, Object> input) {
		LeaveEmailRecipientsTransactionsBO bo = new LeaveEmailRecipientsTransactionsBO();
		boolean status = false;
		Integer id = 0;
		List<Map<String, Object>> email = (List<Map<String, Object>>) input.get("teamId");
		for (Map<String, Object> empId : email) {
			bo.setWorkFromHomeID(Conversion.objectToInteger(input.get("wfhid")));
			
			bo.setTeamMemberID(Conversion.objectToInteger(empId.get("employeeID")));
			id = (Integer) input.get("employeeId");
			bo.setCreatedBy(id);
			bo.setUpdatedBy(id);
			bo.setCreatedDate(new Timestamp(new Date().getTime()));
			bo.setUpdatedDate(new Timestamp(new Date().getTime()));
			status = applyWfhdao.saveLeaveEmailRecipients(bo);
		}

		return status;
	}

	@Override
	public boolean approveWFH(Map<String, Object> input) {
		WorkFromHomeTransactionsBO bo = null;
		boolean status=false;
		
		bo= new WorkFromHomeTransactionsBO();
		
		bo.setWorkFromHomeID(Conversion.objectToInteger(input.get("leaveID")));
		bo.setWorkFromHomeStatus(Conversion.objectToInteger(input.get("leaveStatus")));
		bo.setUpdatedBy(Conversion.objectToInteger(input.get("approvedByID")));
		bo.setReasonForRejectionORRevoke((String)input.get("reasonForRejectionORRevoke"));
		bo.setApprovedByORRejectedBy(Conversion.objectToInteger(input.get("approvedByID")));
		bo.setApprovedORRejectionDate(new Timestamp(new Date().getTime()));
		bo.setUpdatedDate(new Timestamp(new Date().getTime()));
		status=applyWfhdao.approveWFH(bo);
		
		return status;
	}

	@Override
	public Map<String, Object> approveOrRejectNotification(Map<String, Object> input, String employeeName) {
		boolean flag=false;
		String typeOfLeave="to Work from Home";
		Map<String,Object> map=new HashMap<String,Object>();
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee(Conversion.objectToInteger(input.get("employeeID")));
	   
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		String employeeEmail=(String)finalmap.get(0).get("emailID");
		String managerName=(String)finalmap.get(0).get("managerName");
		List<Map<String, Object>> managermap = EmployeeDao.getEmployee(Conversion.objectToInteger(input.get("approvedByID")));
		String Name=(String)managermap.get(0).get("empName");
		map.put("name", employeeName);
		map.put("employeeID", input.get("employeeID"));
		//map.put("leaveStatus", dto.getLeaveStatus());
		map.put("leaveType", input.get("leaveType"));
		map.put("reason", input.get("reasonForRejectionORRevoke"));
		map.put("managerName", managerName);
		map.put("typeOfLeave", typeOfLeave);
		map.put("aprovedBy", Name);
		
		
		map.put("firstsession", input.get("sessionOfStartDate"));
		map.put("secondsession",input.get("sessionOfEndDate"));
		int status=Conversion.objectToInteger(input.get("leaveStatus"));
		try{
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = (String) input.get("leaveStartDate");
		String endDate = (String) input.get("leaveEndDate");
		Date sdate=sf.parse(startDate);
		Date edate=sf.parse(endDate);
		map.put("startDate", sdate);
		map.put("endDate", edate);
		Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
		Integer endSession = Integer.parseInt(input.get("sessionOfEndDate")+"");
		Integer leaveTypeID=0;
		
		Double days=applyWfhdao.countNoOfDays(sdate,edate,startSession,endSession,leaveTypeID);
		Float d= new Float(days);
		map.put("duration", d);
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		String leavestatus="";
		Map<String, Object> statusMap = new HashMap<String, Object>();
		if(status==1)
		{
			leavestatus="approved";
			map.put("leaveStatus", leavestatus);
			String template = EmailTemplate.approveMail(map,leaveAppliedBy,managerName);
			emailer.sendMail(employeeEmail, Name.trim()  +" has "+leavestatus+" your application "+typeOfLeave  , template);
			//flag=true;
			statusMap.put("status", leavestatus);
			statusMap.put("flag", flag);
			
			/*leavestatus="aproved";
			map.put("leaveStatus", leavestatus);*/
		}
		else{
			leavestatus="rejected";
			map.put("leaveStatus", leavestatus);
			String template = EmailTemplate.approveOrRejectMail(map,leaveAppliedBy,managerName);
			emailer.sendMail(employeeEmail, Name.trim()   +" has "+leavestatus+" your application "+typeOfLeave  , template);
			//flag=true;
			statusMap.put("status", leavestatus);
			statusMap.put("flag", flag);
			
			/*leavestatus="rejected";
			map.put("leaveStatus", leavestatus);*/
		}
		return map;

		
		
	}

	@Override
	public void remindLeaveRequest(Map<String, Object> input, float nOfdays) throws Exception {
		String leaveType="Work from Home";
		Map<String, Object> detail=new HashMap<String, Object>();
		detail.put("name", input.get("employeeName"));
		detail.put("sdate", input.get("leaveStartDate"));
		detail.put("edate", input.get("leaveEndDate"));
		detail.put("reason", input.get("reasonForLeave"));
		detail.put("leaveType", input.get("leaveType"));
		detail.put("duration",nOfdays );
		detail.put("leave", leaveType);
		detail.put("firstsession", input.get("sessionOfStartDate"));
		detail.put("secondsession", input.get("sessionOfEndDate"));
		
		List<Map<String, Object>> finalmap = EmployeeDao.getEmployee((Integer)input.get("employeeID"));
		String leaveAppliedBy=(String)finalmap.get(0).get("empName");
		String managerEmail=(String)finalmap.get(0).get("managerEmail");
		String managerName=(String)finalmap.get(0).get("managerName");
		int gender=(int) finalmap.get(0).get("gender");
		
		String genderChar="";
		if(gender==1){
			genderChar="his";
		}else{
			genderChar="her";
		}
		detail.put("gender", genderChar);
		List<Map<String, Object>> seniorMgrsMailMap = employeeLeaveDisplayDao.getImmediateSeniorManger(Conversion.objectToInteger(input.get("employeeID")));
		for(Map<String,Object> names:seniorMgrsMailMap)
		{
			String srMgrName=(String)names.get("empName");
			String srMgremail=(String)names.get("email");
			String template = EmailTemplate.followUPMailByManagerToRemindEmployee(detail,leaveAppliedBy,srMgrName);
			emailer.sendMail(srMgremail, "Reminder � "+leaveAppliedBy  +"'s "+leaveType+" application."  , template);
		}
		
		/*String template = EmailTemplate.followUPMailByManagerToRemindEmployee(detail,leaveAppliedBy,managerName);
		emailer.sendMail(managerEmail, leaveAppliedBy  +" wants to remind you that "+genderChar+" "+ leaveType+" application is still pending."  , template);*/
	}

}
