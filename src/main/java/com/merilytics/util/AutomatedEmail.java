package com.merilytics.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AutomatedEmail {
	
	static String link = "http://apps.merilytics.com/LMS/#/Dashboard/Employee/EmployeeHome";
	static String mailID = "mailto:pmsupport@merilytics.com";
	

	// Follow-up mail by manager to remind employee
	public static String MailToApproverByApplicantWeekBeforeForLeave(Map<String, Object> input, String leaveAppliedBy,
			String managerName) {
		
		String time = "";
		String endtime = "";
		String startdate = null;
		String enddate = null;
		int startSession = (int) input.get("firstsession");
		int endSession = (int) input.get("secondsession");

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = formatter.parse(input.get("sdate").toString());
			Date date1 = formatter.parse(input.get("edate").toString());

			if (startSession == 1 || startSession == 2) {

				time = "9:00 AM";
			}
			if (startSession == 3) {
				time = "1:00 PM";
			}
			if (endSession == 2) {
				endtime = "1:00 PM";
			}
			if (endSession == 3 || endSession == 1) {
				endtime = "6:00 PM";
			}

			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy");
			startdate = sdf.format(date);
			enddate = sdf.format(date1);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + managerName + ","
				+ "<br/><br/>" +   " This is an automated e-mail to remind you that the"+""+input.get("leaveType")+ " application from  "
				+leaveAppliedBy +" "
				+"is still pending for approval and the leave is less than a week away" + " "
				+ " Please approve/reject the leave ASAP on the Leave Management Portal to avoid loss of pay for" +leaveAppliedBy+ 
				"Please find below the details � "
				+ "<br/><br/>" + "<u>LeaveType:</u>" + "  " + input.get("leaveType")

				+ "<br/>" + "<u>Dates:</u>" + " " + startdate + " " + time + " to " + enddate + "  " + endtime
				+ "<br/>" + "<u>Duration:</u>" + "  " + input.get("duration") + " " + "working days" + "<br/>"

				+ "<u>Reason:</u>" + "  " + input.get("reason") + " " 
				+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">PM Support</a></label></body></html>"
				+ "<label style='font-family : Arial; font-size:10px;'><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                           + "and proprietary to Merilytics Inc. and/or its affiliates."
                           + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                           + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                           + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                           + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";


		return emailContent;
	}

	// Submitted mail to manager when employee finishes DAP process

	public static String submittedMailToApproverAndApplicantForWfhCompoff(Map<String, Object> input, String leaveAppliedBy) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("MaternityLeave", 1);
		map.put("PaternityLeave", 2);
		map.put("Paid Time Off", 3);
		map.put("Floater Holiday", 4);
		map.put("CompOff", 5);
		map.put("Unpaid", 6);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String startdate = null;
		String enddate = null;
		int startSession = (int) input.get("firstsession");
		int endSession = (int) input.get("secondsession");
		String leaveTypeName = "";
		String time = "";
		String endtime = "";
		try {
			Date date = formatter.parse(input.get("sdate").toString());
			Date date1 = formatter.parse(input.get("edate").toString());

			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				Integer value = (Integer) entry.getValue();
				int leaveTypeId = (int) input.get("leaveType");
				if (leaveTypeId == value) {
					leaveTypeName = key;
				}
			}

			if (startSession == 1 || startSession == 2) {

				time = "9:00 AM";
			}
			if (startSession == 3) {
				time = "1:00 PM";
			}
			if (endSession == 2) {
				endtime = "1:00 PM";
			}
			if (endSession == 3 || endSession == 1) {
				endtime = "6:00 PM";
			}

			date = new Date(date.getTime());
			date1 = new Date(date1.getTime());

			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy");

			startdate = sdf.format(date);
			enddate = sdf.format(date1);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " +  input.get("name")
				+ "," + "<br/><br/>" +"This is an automated e-mail to remind you that the "+ input.get("leave")   
				+ " is still pending for approval."+ " Please approve/reject the leave ASAP on the Leave Management Portal to avoid loss of pay for"+ leaveAppliedBy+ "Please find below the details �" 
				+ "<br/><br/>" + "<u>LeaveType:</u>"+ "  " + leaveTypeName + "<br/>"
				

				+ "<u>Dates:</u>" + " " + startdate + " " + time + " to " + enddate + "  " + endtime + "<br/>"
				+ "<u>Duration:</u>" + "  " + input.get("duration") + " " + "working days" + "<br/>"

				+ "<u>Reason:</u>" + "  " + input.get("reason") + " " + "<br/>"
				
				
				
	
				
	+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"

				+ "<br/><br/>Regards" 
				+ "<br/><a href=" + mailID + ">PM Support</a></label></body></html>"
				
				+ "<label style='font-family : Arial; font-size:10px;'><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                           + "and proprietary to Merilytics Inc. and/or its affiliates."
                           + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                           + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                           + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                           + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";


		return emailContent;
	}

	public static String submittedMailToTeamOneWeekBefore(Map<String, Object> input, String leaveAppliedBy) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("MaternityLeave", 1);
		map.put("PaternityLeave", 2);
		map.put("Paid Time Off", 3);
		map.put("Floater Holiday", 4);
		map.put("CompOff", 5);
		map.put("Unpaid", 6);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String startdate = null;
		String enddate = null;
		int startSession = (int) input.get("firstsession");
		int endSession = (int) input.get("secondsession");
		String leaveTypeName = "";
		String time = "";
		String endtime = "";
		try {
			Date date = formatter.parse(input.get("sdate").toString());
			Date date1 = formatter.parse(input.get("edate").toString());

			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				Integer value = (Integer) entry.getValue();
				int leaveTypeId = (int) input.get("leaveType");
				if (leaveTypeId == value) {
					leaveTypeName = key;
				}
			}

			if (startSession == 1 || startSession == 2) {

				time = "9:00 AM";
			}
			if (startSession == 3) {
				time = "1:00 PM";
			}
			if (endSession == 2) {
				endtime = "1:00 PM";
			}
			if (endSession == 3 || endSession == 1) {
				endtime = "6:00 PM";
			}

			date = new Date(date.getTime());
			date1 = new Date(date1.getTime());

			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy");

			startdate = sdf.format(date);
			enddate = sdf.format(date1);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + "Team"
				+ "," + "<br/><br/>" +"This is to remind you that "+ leaveAppliedBy + " will be on " + input.get("leave")
				+ "  next week (details below). Please plan for the team�s bandwidth accordingly." 
				+ "<br/><br/>" + "<u>LeaveType:</u>"+ "  " + leaveTypeName + "<br/>"
				

				+ "<u>Dates:</u>" + " " + startdate + " " + time + " to " + enddate + "  " + endtime + "<br/>"
				+ "<u>Duration:</u>" + "  " + input.get("duration") + " " + "working days" + "<br/>"

				+ "<u>Reason:</u>" + "  " + input.get("reason") + " " + "<br/>"
				
				
				
	
				
	+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"

				+ "<br/><br/>Regards" 
				+ "<br/><a href=" + mailID + ">PM Support</a></label></body></html>"
				
				+ "<label style='font-family : Arial; font-size:10px;'><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                           + "and proprietary to Merilytics Inc. and/or its affiliates."
                           + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                           + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                           + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                           + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";


		return emailContent;
	}


	public static String approveMail(Map<String, Object> input, String leaveAppliedBy, String managerName) {
		String name=(String)input.get("aprovedBy");
		String emailContent = "<html><body><label style='font-family : Arial; font-size:10px;'>Hi " + input.get("name")
				+ "," + "<br/><br/>" + name + " has  " + input.get("leaveStatus") + " " + " your application "
				+ input.get("typeOfLeave") + " on the Leave Management Portal. Please find below the details - "
				+ "<br/>"
				+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">PM Support</a></label></body></html>"
				+ "<label style='font-family : Arial; font-size:10px;'><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                           + "and proprietary to Merilytics Inc. and/or its affiliates."
                           + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                           + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                           + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                           + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";


		return emailContent;
	}

	public static String approveOrRejectMail(Map<String, Object> input, String leaveAppliedBy, String managerName) {
	String leaveStatus=(String) input.get("leaveStatus");
	String name=(String)input.get("aprovedBy");
	
	String content="";
		if(leaveStatus.equalsIgnoreCase("rejected")){
			content="<u>Reject Reason:</u>" + "  " + input.get("reason");
		}
		String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + input.get("name")
				+ "," + "<br/><br/>" + name + " has " + input.get("leaveStatus") + " " + " your application "
				+ input.get("typeOfLeave") + " on the Leave Management Portal. Please find below the details - "
				+ "<br/><br/>"
				+ content 
				+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"
				
				

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">PM Support</a></label></body></html>"
				+ "<label style='font-family : Arial; font-size:10px; ><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                           + "and proprietary to Merilytics Inc. and/or its affiliates."
                           + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                           + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                           + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                           + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";

		

		return emailContent;
	}
	
	public static String submittedMailToLevelManager(Map<String, Object> input, String leaveAppliedBy) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String startdate = null;
		String enddate = null;
		int startSession = (int) input.get("firstsession");
		int endSession = (int) input.get("secondsession");
		String time = "";
		String endtime = "";
		try {
			Date date = formatter.parse(input.get("sdate").toString());
			Date date1 = formatter.parse(input.get("edate").toString());

			if (startSession == 1 || startSession == 2) {

				time = "9:00 AM";
			}
			if (startSession == 3) {
				time = "1:00 PM";
			}
			if (endSession == 2) {
				endtime = "1:00 PM";
			}
			if (endSession == 3 || endSession == 1) {
				endtime = "6:00 PM";
			}

			date = new Date(date.getTime());
			date1 = new Date(date1.getTime());

			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy");

			startdate = sdf.format(date);
			enddate = sdf.format(date1);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + input.get("name")
				+ "," + "<br/><br/>" + leaveAppliedBy + " has requested " + input.get("leave") + " "
				+ " on the Leave Management Portal. Please find below the details - " + "<br><br/>" + "<u>Dates:</u>"
				+ " " + startdate + " " + time + " to " + enddate + "  " + endtime + "<br/>" + "<u>Duration:</u>"
				+ "  " + input.get("duration") + " " + "working days" + "<br/>"

				+ "<u>Reason:</u>" + "  " + input.get("reason") +  " " + "<br/>"
				
				

+"<br/><a href=" + link + ">Approve</a>" +" "+"<br/><a href=" + link + ">Reject</a>"
				
	+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">PM Support</a></label></body></html>"
				+ "<label style='font-family : Arial; font-size:10px; '><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                           + "and proprietary to Merilytics Inc. and/or its affiliates."
                           + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                           + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                           + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                           + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";


		return emailContent;
	}
	
	
	public static String followUPMailByEmployeeToRemindManager(Map<String, Object> input, String leaveAppliedBy,
			String managerName) {
		
		String time = "";
		String endtime = "";
		String startdate = null;
		String enddate = null;
		int startSession = (int) input.get("firstsession");
		int endSession = (int) input.get("secondsession");

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = formatter.parse(input.get("sdate").toString());
			Date date1 = formatter.parse(input.get("edate").toString());

			if (startSession == 1 || startSession == 2) {

				time = "9:00 AM";
			}
			if (startSession == 3) {
				time = "1:00 PM";
			}
			if (endSession == 2) {
				endtime = "1:00 PM";
			}
			if (endSession == 3 || endSession == 1) {
				endtime = "6:00 PM";
			}

			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy");
			startdate = sdf.format(date);
			enddate = sdf.format(date1);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + managerName + ","
				+ "<br/><br/>" + leaveAppliedBy + " has sent you a reminder that  "+input.get("gender") +" "+ input.get("leave") + " "
				+ " application is still pending on the Leave Management Portal. Please find below the details - "
				+ "<br/><br/>" + "<u>LeaveType:</u>" + "  " + input.get("leaveType")

				+ "<br/>" + "<u>Dates:</u>" + " " + startdate + " " + time + " to " + enddate + "  " + endtime
				+ "<br/>" + "<u>Duration:</u>" + "  " + input.get("duration") + " " + "working days" + "<br/>"

				+ "<u>Reason:</u>" + "  " + input.get("reason") + " " 
				+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">PM Support</a></label></body></html>"
				+ "<label style='font-family : Arial; font-size:10px;'><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                           + "and proprietary to Merilytics Inc. and/or its affiliates."
                           + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                           + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                           + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                           + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";


		return emailContent;
	}



}
