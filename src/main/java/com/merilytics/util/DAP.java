package com.merilytics.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAP {

	public static List<Map<String, Object>> getemployeeReporting(List<Map<String, Object>> empolyeeList,
			String employeeID) {

		List<Map<String, Object>> finalempolyeeList = new ArrayList<>();

		for (Map<String, Object> internalmap : empolyeeList) {

			if (Conversion.twoStringComparision(internalmap.get("immediateReportingManagerID"), employeeID)) {

				Map<String, Object> finalmap = new HashMap<String, Object>();

				finalmap.put("employeeID", internalmap.get("EmployeeID"));
				finalmap.put("employeeName", internalmap.get("name"));
				finalmap.put("designation", internalmap.get("designation"));
				finalmap.put("year", internalmap.get("dateOfJoining"));
				finalmap.put("emailID", internalmap.get("emailID"));
				// emailID


				List<Map<String, Object>> listofemployee = getemployeeReporting(empolyeeList,
						internalmap.get("EmployeeID").toString());

				Map<String, Object> internalMap = new HashMap<String, Object>();

				//internalMap.put("date", internalmap.get("date"));
				internalMap.put("year", internalmap.get("dateOfJoining"));
				internalMap.put("isManager", internalmap.get("isManager"));
				internalMap.put("employeeID", internalmap.get("EmployeeID"));
				internalMap.put("employeeName", internalmap.get("name"));

				if (!listofemployee.isEmpty()) {
					finalmap.put("reportees", listofemployee);
				}

				finalmap.put("monthlyComp", internalMap);
				finalempolyeeList.add(finalmap);

			}

		}
		return finalempolyeeList;
	}

}
