package com.merilytics.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @author 21258
 *
 */
public class DateOperations {
	public static int diff(Date date1, Date date2) {
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();

		c1.setTime(date1);
		c2.setTime(date2);
		int diffDay = 0;

		if (c1.before(c2)) {
			diffDay = countDiffDay(c1, c2);
		} else {
			diffDay = countDiffDay(c2, c1);
		}

		return diffDay;
	}

	public static void DateDiff(Date date1, Date date2) {
		int diffDay = diff(date1, date2);
		//System.out.println("Different Day : " + diffDay);
	}

	public static int countDiffDay(Calendar c1, Calendar c2) {
		
		int returnInt = 0;
		while (!c1.after(c2)) {
			c1.add(Calendar.DAY_OF_MONTH, 1);
			returnInt++;
		}

		if (returnInt > 0) {
			returnInt = returnInt - 1;
		}

		return (returnInt);
	}
	
	
	public static float countNoOfDays(String startDate,String endDate,Map<String,Object> input) throws ParseException
	{
		/*String startDate = (String) input.get("leaveStartDate");
		String endDate = (String) input.get("leaveEndDate");
		
		Date sDate = formatter.parse(startDate);
		Date eDate = formatter.parse(endDate);*/
		
		
		float nOfdays = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date sDate = formatter.parse(startDate);
		Date eDate = formatter.parse(endDate);
	

		if (sDate.compareTo(eDate) == 0) {
			Integer startSession = new Integer(input.get("sessionOfStartDate") + "");
			Integer endSession = Integer.parseInt((String) input.get("sessionOfEndDate"));
			if(startSession==1||endSession==1){
				nOfdays=1;
			}
				if(startSession == 2 || startSession == 3 || endSession == 2 || endSession == 3)
				{
					double d = 0.5;
					float f = (float) d;
					nOfdays = f;
				

				}
			
			
			

		} else {

			Integer startSession = new Integer(input.get("startSession") + "");
			Integer endSession = Integer.parseInt((String) input.get("endSession"));
			if ((startSession == 1 || startSession == 2) && (endSession == 1 || endSession == 3)) {
				nOfdays = DateOperations.diff(sDate, eDate);
				nOfdays = nOfdays + 1;
				
			}
			else {
				nOfdays = DateOperations.diff(sDate, eDate);
				if (startSession == 3 && endSession == 2) {
				}

				else if (startSession == 2 || startSession == 3 || endSession == 2 || endSession == 3) {
					double d = 0.5;
					float f = (float) d;
					nOfdays = nOfdays + f;

				}
			}
		}
		return nOfdays;
	}

}
