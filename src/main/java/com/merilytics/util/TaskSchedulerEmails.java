package com.merilytics.util;

public class TaskSchedulerEmails {
	static String link = "http://apps.merilytics.com/LMS/#/Dashboard/Employee/EmployeeHome";
	static String mailID = "mailto:appsupport@merilytics.com";
	

	

	
	
	
public static String emailToEmployee(String empName,String leaveDate){
	
		String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + empName+ "," 
		 +"<br/><br/>"+ "This is to let you know that <b>you are marked as absent</b> on <b>"+ leaveDate +"</b> as there is no biometric login record on this date. " 
		 +"This will be reflected as an <b>unpaid leave</b> for this month. You may convert this into a paid leave through the leave management portal."
		 + "<br/><br/>"+"If you believe that this should not be a leave, you could request approval for a �Work From Home� and get it approved by your senior manager."
	 
		
		 
	
		+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"
		
		

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">App Support</a></label></body></html>"
		+ "<label style='font-family : Arial; font-size:10px; ><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
                   + "and proprietary to Merilytics Inc. and/or its affiliates."
                   + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
                   + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
                   + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
                   + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";

		
		return emailContent;
	}

public static String emailToManager(String empName,String leaveDate,String managerName){
	
	String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + managerName+ "," 
	 +"<br/><br/>"+ "This is to let you know that <b>"+empName+" has been marked as absent</b> on <b>"+ leaveDate +"</b> as there is no biometric login record on this date. " 
	 +"This will be reflected as an <b>unpaid leave</b> for this month. You may convert this into a paid leave through the leave management portal."
	 + "<br/><br/>"+"If you believe that this should not be a leave, you could request approval for a �Work From Home� and get it approved by your senior manager."
 
	
	 

	+"<br/><br/><a href=" + link + ">Please click here to view in the portal</a>"
	
	

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">App Support</a></label></body></html>"
	+ "<label style='font-family : Arial; font-size:10px; ><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
               + "and proprietary to Merilytics Inc. and/or its affiliates."
               + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
               + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
               + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
               + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";

	
	return emailContent;
}

public static String emailToEmployeeWithMoreInfo(String empName,String leaveDate,String sessionDetails,String punchIN,String punchOut){
	
	String emailContent = "<html><body><label style='font-family : Calibri; font-size:10px;'>Hi " + empName+ "," 
	 +"<br/><br/>"+ "This is to inform  <b>you that system has generated "+sessionDetails+" on "+ leaveDate +"</b> based on the following biometric timestamps recorded. " 
				/*
				 * +"If you believe that this should not be a leave, you could request the senior manager of your silo to approve converting this to �Work from Home� or �Work from Client� or �PTO� (whichever is appropriate) through the "
				 * +"<a href=" + link + ">leave management portal</a>."
				 */
	+"<br/><br/>First In: "+punchIN
	+"<br/>Last Out:  "+punchOut
	 

	
	

+ "<br/><br/>Regards" 
+ "<br/><a href=" + mailID + ">App Support</a></label></body></html>"
	+ "<label style='font-family : Arial; font-size:10px; ><font color='#585858'><br/><br/>The information contained in this e-mail is confidential "
               + "and proprietary to Merilytics Inc. and/or its affiliates."
               + "The information transmitted herewith is intended only for use by the individual or entity to which it is addressed."
               + " If the reader of this message is not the intended recipient, you are hereby notified that any review, retransmission, "
               + "dissemination, distribution, copying or other use of, or taking of any action in reliance upon this information is strictly prohibited."
               + "If you have received this communication in error, please contact the sender and delete the material from your computer.</font></label></body></html>";

	
	return emailContent;
}

}
