var lms=angular.module('lms',['ngMaterial', 'ngMessages', 'ngAnimate','ui.router','md.data.table'
							,'lms.ScreenDirectives','lms.Services','lms.LMSDirectives']);

lms.controller('LMSController',function($scope,$rootScope,$mdDialog,BusinessLogic1,BusinessLogic,Menu,$state){
	
	BusinessLogic1.GetMethod('empDetailsWithToken').then(function(response){
		var userDetails=angular.copy(response);
		
		if(userDetails.iD==21100){
			$state.go('Dashboard.Admin.AdminHome');
		}
		
	});
	
})


lms.config(['$stateProvider','$urlRouterProvider','$locationProvider','$mdDateLocaleProvider', '$compileProvider',function($stateProvider,$urlRouterProvider,$locationProvider,$mdDateLocaleProvider, $compileProvider){
	$compileProvider.preAssignBindingsEnabled(true);
	$mdDateLocaleProvider.formatDate = function (date) { if (date == undefined) { return "" } else { console.log(date +'  ---  '+   moment(date).format('MMM DD, YYYY'));  return moment(date).format('MMM DD, YYYY'); } };

	$urlRouterProvider.otherwise('Dashboard/Employee');
	$locationProvider.hashPrefix('');
	
	$stateProvider.state({name : 'Dashboard',url : '/Dashboard',template : '<dashboard layout="column" style="over-flow:hidden;" flex></dashboard>'})
				  .state({name : 'Dashboard.Employee',url : '/Employee',template : '<employee-view flex></employee-view>'})
				  .state({name : 'Dashboard.Admin',url : '/Admin',template : '<admin-view flex></admin-view>'})
				  .state({name : 'Dashboard.Employee.EmployeeHome',url : '/EmployeeHome',template : '<employee-home-view flex></employee-home-view>'})
				  .state({name : 'Dashboard.Admin.AdminHome',url : '/AdminHome',template : '<admin-home-view flex></admin-home-view>'})
				  .state({name : 'Dashboard.Admin.Settings',url : '/Settings',template : '<settings-view layout="column" flex></settings-view>'})
				  .state({name : 'Dashboard.Employee.SiloView',url : '/SiloView',template : '<silo-view flex></silo-view>'})
				  .state({name : 'Dashboard.Admin.EmpDetails',url : '/EmpDetails',template : '<emp-details-View flex></emp-details-view>'})
				  .state({name : 'Dashboard.Admin.EmpDetails.individualDetails',url : '/individualEmpDetails',template : '<individual-employee-view flex></individual-employee-view>'})
				  .state({name : 'Dashboard.Employee.individualDetails',url : '/individualEmployeeDetails',template : '<individual-employee-details flex></individual-employee-details>'});
}]);


lms.run(function($transitions, $rootScope, $window,$state,NoAccessNotification,BusinessLogic1,BusinessLogic,$transitions){
	$rootScope.loadingPanel = false;
	$rootScope.env = 'dev';
	$rootScope.devLink ='http://localhost:8080';
	$rootScope.testLink ='http://172.16.0.52:1100';
	$rootScope.prodLink = 'https://apps.merilytics.com';
	
	
	if($rootScope.env == 'prod'){
		$rootScope.ApplicationURL = $rootScope.prodLink;
	}
	else if($rootScope.env == 'dev'){
		$rootScope.ApplicationURL = $rootScope.devLink;
	}
	else if($rootScope.env == 'test'){
		$rootScope.ApplicationURL = $rootScope.testLink;
	}
	
	BusinessLogic1.GetMethod('empDetailsWithToken').then(function(response){
		$rootScope.userData=angular.copy(response);
		
		
			
			
	},function(reason){})
	
	
	$transitions.onSuccess({}, function(transition) { 
		if($rootScope.UserScreens == undefined){
			$rootScope.ProjectName = "LMS"
			BusinessLogic1.PostMethod('userStates',$rootScope.ProjectName).then(function(response){
				if($rootScope.userData.iD == 21100){
					//transition.$to().name = "Dashboard.Admin.AdminHome";
				}
				$rootScope.UserScreens = response;
				var count=0;
				for(var i=0;i<$rootScope.UserScreens.length;i++){
					if(transition.$to().name == $rootScope.UserScreens[i].state){
						$rootScope.Urlfromdb = $rootScope.UserScreens[i].url;
						count++;
						break;
					}
					else{
						continue;
					}
				}
				if(count >= 1){
					
					if(rootScope.env == 'dev'){
							$rootScope.Urlfromdb = $rootScope.Urlfromdb.replace(/apps.merilytics.com/,rootScope.devLink);
						}
						else if(rootScope.env == 'test'){
							$rootScope.Urlfromdb = $rootScope.Urlfromdb.replace(/apps.merilytics.com/,rootScope.testLink);
						}
					$window.location.href = $rootScope.Urlfromdb ;
					
				}
				else{
					$window.location.href = $rootScope.ApplicationURL + transition.$to().name ;
				}
			},function(reason){})
		}
		else{
			if($rootScope.userData.iD == 21100){
				//transition.$to().name = "Dashboard.Admin.AdminHome";
			}
			var count=0;
			for(var i=0;i<$rootScope.UserScreens.length;i++){
				if(transition.$to().name == $rootScope.UserScreens[i].state){
					$rootScope.Urlfromdb = $rootScope.UserScreens[i].url;
					count++;
					break;
				}
				else{
					continue;
				}
			}
			if(count >= 1){
				if(rootScope.env == 'dev'){
					$rootScope.Urlfromdb = $rootScope.Urlfromdb.replace(/apps.merilytics.com/,rootScope.devLink);
				}
				else if(rootScope.env == 'test'){
					$rootScope.Urlfromdb = $rootScope.Urlfromdb.replace(/apps.merilytics.com/,rootScope.testLink);
				}
				$window.location.href = $rootScope.Urlfromdb ;
			}
			else{
				$window.location.href = $rootScope.ApplicationURL + transition.$to().name ;
			}
		}
		
		
		
		
		});
	
	
})