
var ScreenDirectives = angular.module('lms.ScreenDirectives',[])

ScreenDirectives.directive('dashboard',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','BusinessLogic','BusinessLogic1','$transitions','Menu','notification','$filter', 'Export', function(window, state, Q, mdDialog,scope,rootScope,BusinessLogic,BusinessLogic1,$transitions,Menu,notification,filter, Export){
        	var DashboardController=this;
        	rootScope.hideMainCalendar = false;
        	scope.showHolidays=true;
        	rootScope.selectedWeek=0;
        	scope.query = { filter: '', order: '', limit: '10', page: 1 };
    		
        	DashboardController.date=new Date();
        	DashboardController.isAdmin=false;
        	DashboardController.CalenderDate=new Date();
        	DashboardController.calenderNextClicks=0
        	/*BusinessLogic.GetMethod('getAppList').then(function(response){ DashboardController.applicationList = []; DashboardController.applicationList = angular.fromJson(response); }, function(reason){ });*/
        	DashboardController.openMenu = function(mdMenu, ev){        		
        		BusinessLogic1.GetMethod('getApps').then(function(response){ DashboardController.applicationList = []; DashboardController.applicationList = angular.fromJson(response); }, function(reason){ })
        		mdMenu(ev);
        	}
        	/*BusinessLogic1.GetMethod('getApps').then(function(response){ DashboardController.applicationList = []; DashboardController.applicationList = angular.fromJson(response); }, function(reason){ })*/
    		DashboardController.applicationRedirection = function(applicationData){ if(applicationData.url == rootScope.ApplicationURL+'/LMS/#/') {  state.go('Dashboard.Employee.EmployeeHome'); } else{ window.location.href = applicationData.url}};
    	//	DashboardController.applicationList = [{'appName':'DAP', 'imageUrl': 'dap-icon.svg', 'url': rootScope.ApplicationURL+'/Authentication/#'},{'appName':'PM APP', 'imageUrl': 'Assets/dap-icon.svg', 'url': rootScope.ApplicationURL+'/Authentication/#'}];
    		
    	

        /*	window.onbeforeunload = handleBackFunctionality;
        	function handleBackFunctionality() {
                if (window.event) {
                    if (window.event.clientX < 40 && window.event.clientY < 0) {
                        alert("Clicked - Browser back button.");
                    }
                    else {
                        alert("Clicked - Browser refresh button.");
                    }
                }              
            }*/
        	
        /*	rootScope.$on('$locationChangeSuccess', function() {
                rootScope.actualLocation = $location.path();
                alert("hello");
            });        

           rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
                if(rootScope.actualLocation === newLocation) {
                    alert('Why did you use history back?');
                }
            });*/
        	
    //    	rootScope.makeeffect = false;
        /*	$transitions.onSuccess({ }, function(trans) {
        	    stateChangeSuccessCallBack(**trans.$to().self**, trans.params('to'));
        	});*/
        	    /*$transitions.onSuccess({}, function($transition){
  	       	    rootScope.todfgfd =  $transition.$to();
           	        rootScope.dfgfd = $transition.$from();           	       
           	        if(rootScope.todfgfd.name == "Dashboard.Employee"){
           	        	history.back();history.back();
           	        }
           	        else if((rootScope.todfgfd.parent.name != rootScope.dfgfd.parent.name) && (rootScope.comingfrombackbutton == true)) {
           	    	 if(rootScope.dfgfd.parent.name != 'Dashboard.Admin.EmpDetails'){           	    		 
           	    		 rootScope.makeeffect = true;               	    	 
                	     DashboardController.ClickPreviousButton();
           	    	 }           	    	
           	     }
           	     
           	    });
        	
        	
        	
        		DashboardController.ClickPreviousButton= function(){
        			rootScope.comingfrombackbutton = true;
        			if(rootScope.makeeffect == false){
        				history.back();
        			}
        			
        			
        			 
        			if(rootScope.makeeffect == true){
        				
        			if( ((rootScope.todfgfd.parent.name == 'Dashboard.Admin') && (rootScope.dfgfd.parent.name=='Dashboard.Admin')) || ((rootScope.todfgfd.parent.name == 'Dashboard.Employee') && (rootScope.dfgfd.parent.name=='Dashboard.Employee')))
        				{
        				DashboardController.changeView(rootScope.dfgfd.name);
        				}
        			else{
        				 if((rootScope.todfgfd.parent.name == 'Dashboard.Admin') && (rootScope.dfgfd.parent.name=='Dashboard.Employee')){
            				 DashboardController.changeModule('Admin');
            			 }
            			 else if((rootScope.todfgfd.parent.name == 'Dashboard.Employee') && (rootScope.dfgfd.parent.name=='Dashboard.Admin')){
    	                           DashboardController.changeModule('Employee');
                                     }
            			 
        			}
        			
        			}
        			
        			  
         
        	}*/
        	
        	
        	function Bind(){
        		//rootScope.menu_data=[];
        		if(DashboardController.userDetails.iD == 21100){
        			rootScope.selectedModule="Admin";
        		}
        		else{
        			rootScope.selectedModule="Employee";
        		}
        		
        		rootScope.selectedView= rootScope.selectedModule == 'Admin'? 'Dashboard.Admin.AdminHome':'Dashboard.Employee.EmployeeHome';
        		rootScope.menu_data=Menu.GetData(rootScope.selectedModule); 
        		if(DashboardController.userDetails.iD == 21100){
        			state.go('Dashboard.Admin.AdminHome');
        		}
        		else{
        			state.go('Dashboard.Employee.EmployeeHome'); 
        			
        			if(window.performance.navigation.type == 0){ 
        				/*if(DashboardController.myDetailsHistory.length==0){
        					var AutomatedNotifications=0;
						}
						else{
							var AutomatedNotifications=DashboardController.myDetailsHistory[0].AutomatedNotifications;
						}*/
        				/*if(DashboardController.teamDetailsFilter.length > 0 ||  DashboardController.myDetailsHistory[0].AutomatedNotifications > 0){*/
        				if(DashboardController.approvedNotifications > 0 || DashboardController.AutomatedNotifications > 0){
	        				mdDialog.show({
	        					clickOutsideToClose: false,
	        					skipHide: true,
	        					template: '<md-dialog flex="50" flex-md="70" flex-xs="100" flex-sm="100" aria-label="confirmation" md-theme="meritus">' +
	        					'  <md-dialog-content class="md-dialog-content"><h2 class="md-title" style="text-align:left; padding: 10px"> '+
	        					'  <p ng-bind="Msg" style="margin: 0" class="layout-row layout-align-center-center" ></p>'+
	        					'  <p ng-hide="PendingNotifications == 0 || PendingNotifications == null" style="margin: 0" class="layout-padding layout-row layout-align-center-center">You have <span style="color: #95AF28;font-weight: bold;">{{PendingNotifications}}</span> request(s) waiting for approval</p>'+
	        					'  <p  ng-hide="AutomatedNotifications == 0 || AutomatedNotifications == null" style="margin: 0" class="layout-padding layout-row layout-align-center-center"><span>System has generated <span style="color: #95AF28;font-weight: bold;">{{AutomatedNotifications}}</span> leave(s) for you in last 30 days</span></p>'+
	        					'  </h2></md-dialog-content>' +
	        					'  <md-dialog-actions layout="row" layout-align="end center">' +
	        					'    <md-button ng-click="closeDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">OK</md-button>' +
	        					'  </md-dialog-actions>' +
	        					'</md-dialog>',
	        					controller: function DialogController($mdDialog, $scope) { 
	        						$scope.Msg = 'Welcome back !';
	        						$scope.PendingNotifications = DashboardController.approvedNotifications; 
	        						$scope.AutomatedNotifications = DashboardController.AutomatedNotifications;
	        						$scope.closeDialog = function () { $mdDialog.hide(); }
	        					}
	        				});	
        				}        				
        	    	}
        			
        		}
        		
        	}
        	
        	BindUserData();
        	function BindUserData(){
        		BusinessLogic1.GetMethod('empDetailsWithToken').then(function(response){ 
        			DashboardController.userDetails=angular.copy(response);
        			rootScope.userID =angular.copy(DashboardController.userDetails.iD); 
        			rootScope.year=angular.copy(DashboardController.date.getFullYear());
        			rootScope.month =angular.copy(DashboardController.date.getMonth()+1);
        			rootScope.seniorManager = angular.copy(DashboardController.userDetails.seniorManager);
        			rootScope.isCoreTeam = angular.copy(DashboardController.userDetails.coreTeam);
        			getCalenderInfo(rootScope.year,rootScope.month,DashboardController.userDetails.iD)
        			BusinessLogic.PostMethod('checkAdmin',DashboardController.userDetails.iD).then(function(response){ 
        				DashboardController.isAdmin=response.data.isAdmin;
        				BusinessLogic.PostMethod('loginNotification',rootScope.userData.iD).then(function(response){   
        					/*var teamDetails= angular.fromJson(response.data.teamDetails);
        					var meHistory=angular.fromJson(response.data.myHistory);
        						DashboardController.teamDetailsFilter=filter('filter')(teamDetails,function(val,key){if(val.manager==1){return val;}});
        						DashboardController.myDetailsHistory=angular.copy(meHistory);*/
        				DashboardController.AutomatedNotifications = angular.fromJson(response.data.AutomatedNotifications);
        				DashboardController.approvedNotifications = angular.fromJson(response.data.approvedNotifications);
        						Bind();
        					},function(){});
        				
        			},function(reason){})
        		},function(){});
        		
        		
        		
        	}
        	
        	DashboardController.callLogoutRequest = function(ev){
        		var confirm = mdDialog.confirm().title('Are you sure you want to logout?').targetEvent(ev).ok('Yes').cancel('No');
        		mdDialog.show(confirm).then(function () { window.localStorage.removeItem("AuthenticationToken"); 
        			window.location.href= rootScope.ApplicationURL+'/Authentication/#/applications'; 
        		 }, function () { });
        	}
        	DashboardController.changePasswordRequest = function(ev){window.open(rootScope.ApplicationURL+'/Authentication/#/changePassword','_blank'); }
        	DashboardController.openManual = function(ev){
        		mdDialog.show({
        			multiple: true,  
        			templateUrl: './Partials/manualScreen.html',
				      parent: angular.element(document.body),
				      targetEvent: ev,
				      clickOutsideToClose: false,
					  escapeToClose: false,
				      controllerAs: 'manualScreenController',
				      controller: ['BusinessLogic', '$mdDialog', '$state',function(BL, mdDialog, state){
				    	  var manualScreenController = this;
				    	  manualScreenController.closeDialog = function(){ mdDialog.hide(); }
				    	  manualScreenController.url = './DAP/View/manual.mp4';
				      }],
				    }).then(function(answer) { }, function(reject) { });
        		}
        	
        	DashboardController.changeModule=function(module){
        		rootScope.selectedWeek=0;
        		rootScope.comingfrombackbutton = false;
        		rootScope.selectedModule=module;
        		rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        		//getMenu(scope.selectedModule);
        		rootScope.selectedView= module=='Admin'? 'Dashboard.Admin.AdminHome':'Dashboard.Employee.EmployeeHome';
        		DashboardController.CalenderDate=new Date();
        		getCalenderInfo(DashboardController.date.getFullYear(),DashboardController.date.getMonth()+1,DashboardController.userDetails.iD)
        		state.go(scope.selectedView);
        	}
        	
        	DashboardController.changeView=function(url){
        		rootScope.makeeffect = false;
        		rootScope.selectedView={};
        		rootScope.selectedView=url;
        	}
        	
        	DashboardController.selectedWeek = function(cd){ 
        		if(angular.copy(cd) == rootScope.selectedWeek){ 
        			rootScope.selectedWeek=0; 
        		}
        		else{
        			rootScope.selectedWeek=angular.copy(cd);
        		}
        		getCalenderInfo(rootScope.year,rootScope.month,DashboardController.userDetails.iD)
        	}
        	
            
        	DashboardController.ExportToExcel = function() { 
        		BusinessLogic.PostMethod('getLMSReports ',DashboardController.userDetails.iD).then(function(response){ 
    				Export.xlsExportMultipleJSON(angular.fromJson(response.data));
        		});
        	
        		
        	
        	} 
        
        	
        	DashboardController.getPreviousCalenderInfo = function(){
        		DashboardController.CalenderDate = new Date(DashboardController.CalenderDate.setMonth(DashboardController.CalenderDate.getMonth()-1));
        		
        		rootScope.year=angular.copy(DashboardController.CalenderDate.getFullYear());
        		rootScope.month=angular.copy((DashboardController.CalenderDate.getMonth()+1));
        		rootScope.selectedWeek =0;
        		getCalenderInfo(rootScope.year,rootScope.month,DashboardController.userDetails.iD)
        		}
  
        	DashboardController.getNextCalenderInfo = function(){
        		DashboardController.CalenderDate = new Date(DashboardController.CalenderDate.setMonth(DashboardController.CalenderDate.getMonth()+1,1));
        		rootScope.selectedWeek=0;
        		rootScope.year = angular.copy(DashboardController.CalenderDate.getFullYear());
        		rootScope.month = angular.copy((DashboardController.CalenderDate.getMonth()+1));
        		getCalenderInfo(rootScope.year,rootScope.month,DashboardController.userDetails.iD)
        	}
        	
        	
        	rootScope.$on("BindCalender", function(){ 
        		getCalenderInfo(rootScope.year,rootScope.month,rootScope.userData.iD); 
             });
        	
        	
        	function getCalenderInfo(Year,Month,empID){ 
        	
            	BusinessLogic.PostMethod('getcalendarDaysDisplay',{"Year":Year,"Month":Month,"empID":empID,"isAdmin": rootScope.selectedModule == 'Admin' ? 1 : 2}).then(function(response){ 
            		DashboardController.CalenderData=response.data; 
            		//alert(DashboardController.CalenderData[1].Sunday.monthYear);
            		getCalenderInfoDetail(Year,Month,empID);
    			},function(reason){});
        	}
        	
        	function getCalenderInfoDetail(Year,Month,empID){        		
        		BusinessLogic.PostMethod('getHolidaysList',{"Year":Year,"Month":Month,"empID":empID,"isAdmin": rootScope.selectedModule == 'Admin' ? 1 : 2,"week_number": rootScope.selectedWeek == 0 ? null :rootScope.selectedWeek}).then(function(response){ 
            		DashboardController.CalenderDataDetail=response.data;
            	},function(){});
        	}
        	
        }],
        controllerAs: 'DashboardController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Dashboard.html'
		}
})


.directive('adminView',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','Menu', function(window, state, Q, mdDialog,$scope,rootScope,Menu){
        	var AdminController=this;
        	rootScope.hideMainCalendar = false;
        	rootScope.selectedView='Dashboard.Employee.AdminHome';
        	Bind();
        	function Bind(){
        		state.go('Dashboard.Admin.AdminHome');
        	}
        	
        }],
        controllerAs: 'AdminController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Admin/Admin.html'
		}
})
.directive('employeeView',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','Menu', function(window, state, Q, mdDialog,scope,rootScope,Menu){
        	var EmployeeController=this;
        	rootScope.hideMainCalendar = false;
        	rootScope.selectedView='Dashboard.Employee.EmployeeHome'
        	Bind();
        	function Bind(){
        		state.go('Dashboard.Employee.EmployeeHome');
        	}
        	
        	
        }],
        controllerAs: 'EmployeeController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Employee/Employee.html'
		}
})
.directive('employeeHomeView',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','BusinessLogic','$timeout','notification','notificationWithNote','BusinessLogic1','Menu','$filter','convertMillToDate', function(window, state, Q, mdDialog,scope,rootScope,BusinessLogic,timeout,notification,notificationWithNote,BusinessLogic1,Menu,filter,convertMillToDate){

        	var EmployeeHomeController=this;
        	scope.isSM=rootScope.seniorManager;
        	scope.isCT=rootScope.isCoreTeam;
        	rootScope.selectedModule="Employee";
        	rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        	rootScope.selectedView='Dashboard.Employee.EmployeeHome';
        	scope.Employees=[];
        	rootScope.hideMainCalendar = false;
        	scope.selectedTransaction="me";
        	scope.selectedHistory=false;
        	EmployeeHomeController.LeaveFormSubmit=false;
        	EmployeeHomeController.SelectedEmails = [];
        	EmployeeHomeController.SelectedEmployeeInfo=[];
        	scope.flag = "ShowYeartoDate";
        	this.myDate = new Date();
        	EmployeeHomeController.minDate = new Date();
        	/*scope.query = { filter: '', order: '', limit: '10', page: 1 };*/
        	
        	EmployeeHomeController.selectedPage = 1;
        	EmployeeHomeController.pageSize = 10; 
        	EmployeeHomeController.searchValue = null;
        	scope.rowCount =[10,20,30];
        	
        	   	
        	Bind();
        	function Bind(){
        		BindUserData(); 
        	}
        	
        	function BindSessionData(){
        		if(angular.isUndefined(EmployeeHomeController.SessionType)){
        			BusinessLogic.GetMethod('getSessionType').then(function(response){ 
            			EmployeeHomeController.SessionType = angular.fromJson(response); 
            		},function(){});
        		}
        	}
        	
        	/*BindNoOfLeaves();*/
        	function BindNoOfLeaves(){
        		BusinessLogic.PostMethod('yearAndMonthToDateStatus',EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){ 
    			EmployeeHomeController.YearToDateLeaveConsolidation = response.data.yearToDateStatus[0];
    			EmployeeHomeController.MonthToDateLeaveConsolidation = response.data.monthToDateStatus[0];
    			EmployeeHomeController.compOffMinDate=new Date(angular.copy(EmployeeHomeController.YearToDateLeaveConsolidation.compoffDate));
    			EmployeeHomeController.compOffMaxDate=new Date(angular.copy(EmployeeHomeController.YearToDateLeaveConsolidation.compOffMax));
    			checking();
    			},function(){});
        	}
        	
        	function BindConversionTypes(){
        		EmployeeHomeController.ConversionTypes=[{"conversionType":"Leave","conversionTypeId":1}
        											   ,{"conversionType":"WFH","conversionTypeId":2}
        											   ,{"conversionType":"WFC","conversionTypeId":3}];
        	}
        	
        	function checking(){ 
        		
    		EmployeeHomeController.value = function(flag, check){
            	
              	 if(flag == "ShowYeartoDate" && check == "paidLeaves"){
              		 if(EmployeeHomeController.YearToDateLeaveConsolidation.noOfPaidLeaves > 0){
              			 return true;
              		 }
              	 }
              	 if(flag == "ShowYeartoDate" && check == "UnpaidLeaves"){
              		 if(EmployeeHomeController.YearToDateLeaveConsolidation.noOfUnpaidLeaves >0){
              			 return true;
              		 }
              	 }
              	 if(flag == "ShowMonthtoDate" && check == "paidLeaves"){
              		 if(EmployeeHomeController.MonthToDateLeaveConsolidation.noOfPaidLeaves > 0){
              			 return true;
              		 }
              	 }
              	 if(flag == "ShowMonthtoDate" && check == "UnpaidLeaves"){
              		 if(EmployeeHomeController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves >0){
              			 return true;
              		 }
              	 }
              	}
        	}
        	
        	
        	EmployeeHomeController.UnPaidToPaidApprove = function(ev,data){
        		var confirm = mdDialog.confirm().title('Do you want to approve paid leave conversion request? Click "Yes" to continue or "No" to go back.').targetEvent(ev).ok('Yes').cancel('No');
        		mdDialog.show(confirm).then(function () { 
        			data.approvedByORRejectedBy=rootScope.userData.iD;
        		BusinessLogic.PostMethod('approveFromUnpaidToPaid',data).then(function(response){
	          			BindTeamAndMe();
	          			BindNoOfLeaves();
	          			notification.notify(response.data.status);
	          			mdDialog.hide();
  	          		},function(reasone){
  	          			notification.notify(reasone.data.status);
  	          		});
        		}, function () { });
      		}
        	
        	EmployeeHomeController.UnPaidToPaidReject = function(ev,md){
        		if(scope.selectedTransaction == 'me' ){
        			var confirm = mdDialog.confirm().title('Do you want to cancel unpaid leave conversion request? Click "Yes" to continue or "No" to go back.').targetEvent(ev).ok('Yes').cancel('No');
            		mdDialog.show(confirm).then(function () { 
            				BusinessLogic.PostMethod('rejectFromUnpaidToPaid',md).then(function(response){
            					BindTeamAndMe();
            					notification.notify(response.data.status);
            					mdDialog.hide();
        	          			},function(reasone){
        	          				notification.notify(reasone.data.status);
        	          			});
               		 }, function () { });
        		}
        		else{
        			 mdDialog.show({ 
   	                  clickOutsideToClose: true,
   	                  scope: scope,        
   	                  preserveScope: true,    
   	    		      fullscreen: 'md' ,       
   	                  templateUrl: 'RejectSystemGeneratedLeave.html',
   	                  controller: function DialogController($scope, $mdDialog) {
   	                	 $scope.reject ={}; 
   	                	EmployeeHomeController.RejectsystemgeneratedData = md;
   	                	  $scope.closeView = function() { 
   	                	  mdDialog.hide(); 	                	 
   	                	  }
   	                	 EmployeeHomeController.Rejectsystemgeneratedleave = function(ev, form, data){ 
   	                		if(form.$invalid){  
   	               			angular.forEach(form.$error, function (field) {
   	                               angular.forEach(field, function(errorField){ errorField.$setTouched(); })
   	                        });
   	               		}
   	                	else {
   	                	 	md.biometricIssueReasonForRejectionOrRevoke = data.reasonForRejectionORRevoke;
   	                	 md.approvedByORRejectedBy=rootScope.userData.iD;
   	                	 	
   	                	 
   	                		BusinessLogic.PostMethod('rejectFromUnpaidToPaid', md).then(function(response){ 
   	              			 	if(response.status==200){ 
   	              			 		mdDialog.hide(); 	            
   	              			 		BindTeamAndMe();
   	              			 		BindNoOfLeaves();
   	              			 		BindCurrentLeaveBalance(); 
   	              			 		notification.notify(response.data.status);
   	              			 	}
   	              			 	else{ mdDialog.hide(); notification.notify(response.data.status); }
   	      	 				},function(){}); 	                		 
   	                	 }
   	                	 }
   	                	 
   	                	
   	                 }
   	               });
        		}
        		
        	}
        	
        	EmployeeHomeController.UnPaidToPaid = function(data){
        		scope.reject={};
        		EmployeeHomeController.UnpaidToPaidData = data;
        		if(data.noOfDays == '0.5'){
        			scope.hidePaidOption=true;
        		}
        		else{
        			scope.hidePaidOption=false;
        		}
        		mdDialog.show({ 
	                  clickOutsideToClose: true,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'UnPaidToPaid.html',
	                  controller: function DialogController($scope, $mdDialog) {
  	                  $scope.closeView = function() { 
 	                	  mdDialog.hide();
 	                  }
  	                  scope.reject = {conversionTypeId : null,firsthalfconversionTypeId: null,secondhalfconversionTypeId: null};
  	                 
  	                scope.showSplitconversion = false;
  	                BusinessLogic.GetMethod('getConversionTypes').then(function(response){ 
  	                	EmployeeHomeController.conversionType = angular.fromJson(response);
  	        		},function(reason){});
  	                
  	              EmployeeHomeController.splitconversiontype = function(){
  	            	  scope.showSplitconversion =  !scope.showSplitconversion;
  	            	 scope.reject = {conversionTypeId : null,firsthalfconversionTypeId: null,secondhalfconversionTypeId: null};
  	              }
  	                  
  	                EmployeeHomeController.UnPaidToPaidCon = function(ev,RejectionForm,reject){
  	                	if(RejectionForm.$invalid){  
  	            			angular.forEach(RejectionForm.$error, function (field) {
  	                            angular.forEach(field, function(errorField){
  	                              errorField.$setTouched();  
  	                            })
  	                          });
  	            		}
  	                	else {
  	                	data.reasonForRejectionORRevoke = reject.reasonForRejectionORRevoke;
  	                	data.firsthalfconversionTypeId = reject.firsthalfconversionTypeId; 
  	                	data.secondhalfconversionTypeId = reject.secondhalfconversionTypeId;
  	                	data.conversionTypeId = reject.conversionTypeId;
  	                	if(data.firsthalfconversionTypeId != null){
  	                		var FHct = filter('filter')(EmployeeHomeController.conversionType,function(val,key){if(val.conversionTypeId == reject.firsthalfconversionTypeId){
  	                			return val.conversionType
  	                			}})
  	  	                	data.firsthalfconversionType = FHct[0].conversionType;
  	                	}
  	                	if(data.secondhalfconversionTypeId != null){
  	                		var SHct = filter('filter')(EmployeeHomeController.conversionType,function(val,key){if(val.conversionTypeId == reject.secondhalfconversionTypeId){
  	                			return val.conversionType
  	                			}})
  	  	                	data.secondhalfconversionType = SHct[0].conversionType;
  	                	}
  	                	if(data.conversionTypeId != null){
  	                		var ct = filter('filter')(EmployeeHomeController.conversionType,function(val,key){if(val.conversionTypeId==reject.conversionTypeId){  return val.conversionType}})
  	  	                	data.conversionType = ct[0].conversionType;
  	                	}
  	                
  	                	BusinessLogic.PostMethod('requestingFromUnpaidToPaid',data).then(function(response){
			  	          			BindTeamAndMe();
			  	          			BindNoOfLeaves();
			  	          			notification.notify(response.data.status);
			  	          			mdDialog.hide();
				  	          		},function(reasone){
				  	          			notification.notify(reasone.data.status);
				  	          		})
  	                	     }
  	                	   }
  	                	}
	                });
        	}
        	
        	EmployeeHomeController.changeTab = function(data, isHistory){ 
        		if(scope.selectedTransaction != data || scope.selectedHistory != isHistory){ 
        			scope.selectedTransaction=data; 
        			scope.selectedHistory = isHistory;
        			EmployeeHomeController.selectedPage = 1;
                	EmployeeHomeController.pageSize = 10; 
                	EmployeeHomeController.searchValue = null;
        			BindTeamAndMe();
        		}
        	}
        	
        	EmployeeHomeController.pageChange = function(selectedPage, rowsPerPage, selectedTransaction){
    			if(selectedPage > 0 && selectedPage <= scope.pages){ 
    				EmployeeHomeController.selectedPage = selectedPage;        			
    				EmployeeHomeController.pageSize = rowsPerPage;
    			/*
    			if(selectedTransaction == 'History'){
    				BindHistory();
    			}
    			else if(selectedTransaction == 'team'){ 
    				BindTeam();
    			}*/
    				BindTeamAndMe();
    		  }
    		}
    		scope.onSearch = function(searchValue){        			
    			EmployeeHomeController.searchValue = searchValue;
    			/*(scope.selectedTransaction == 'team')? BindTeam() : BindHistory();*/
    			BindTeamAndMe();    			
    		}
    		
        	function BindTeamAndMe(){
        		EmployeeHomeController.startNumber = ((EmployeeHomeController.selectedPage - 1)* EmployeeHomeController.pageSize) + 1;    			
        		if(scope.selectedTransaction == 'me' && scope.selectedHistory){ 
        			BusinessLogic.PostMethod('getEmployeeMeHistoryDetails', {'empID' : EmployeeHomeController.SelectedEmployeeInfo.iD, 'pageNo': EmployeeHomeController.selectedPage, 'pageSize': EmployeeHomeController.pageSize, 'search':EmployeeHomeController.searchValue}).then(function(response){        				
        				EmployeeHomeController.myDetailsHistory = angular.fromJson(response.data.myHistory);
        				/*angular.forEach(EmployeeHomeController.myDetailsHistory, function(value, key){ 
        					value.createdDate = convertMillToDate.toDate(value.createdDate);
        				})*/        				 
            			scope.totalCountMe = angular.fromJson(response.data.totalCount);
            			scope.pages = angular.fromJson(response.data.pages);
            			scope.pagesList = Array.from(new Array(angular.fromJson(response.data.pages)),(val,index)=>index+1);
            			EmployeeHomeController.endNumber = ((EmployeeHomeController.selectedPage - 1)* EmployeeHomeController.pageSize)  + angular.fromJson(response.data.myHistory.length);
            			console.log(scope.totalCount);
            		},function(reason){});        		
        		}
        		if(scope.selectedTransaction == 'me' && !scope.selectedHistory){ 
        			BusinessLogic.PostMethod('getEmployeeMeDetails', {'empID' : EmployeeHomeController.SelectedEmployeeInfo.iD, 'pageNo': EmployeeHomeController.selectedPage, 'pageSize': EmployeeHomeController.pageSize, 'search':EmployeeHomeController.searchValue}).then(function(response){	          		
        				EmployeeHomeController.myDetails = angular.fromJson(response.data.myDetails);  
            			scope.totalCountMe = angular.fromJson(response.data.totalCount);
            			scope.pages = angular.fromJson(response.data.pages);
            			scope.pagesList = Array.from(new Array(angular.fromJson(response.data.pages)),(val,index)=>index+1);
            			EmployeeHomeController.endNumber = ((EmployeeHomeController.selectedPage - 1)* EmployeeHomeController.pageSize)  + angular.fromJson(response.data.myDetails.length);
            			console.log(scope.totalCount);
            		},function(reason){});
        		}
        		if(scope.selectedTransaction == 'team' && scope.selectedHistory){
        			BusinessLogic.PostMethod('getEmployeeTeamHistoryDetails', {'empID' : EmployeeHomeController.SelectedEmployeeInfo.iD, 'pageNo': EmployeeHomeController.selectedPage, 'pageSize': EmployeeHomeController.pageSize, 'search':EmployeeHomeController.searchValue}).then(function(response){	          		
        				EmployeeHomeController.teamDetailsHistory = angular.fromJson(response.data.teamHistory);
        				/*angular.forEach(EmployeeHomeController.teamDetailsHistory, function(value, key){ 
    					value.createdDate = convertMillToDate.toDate(value.createdDate);
        				})*/
            			scope.totalCountTeam = angular.fromJson(response.data.totalCount);
            			scope.pages = angular.fromJson(response.data.pages);
            			scope.pagesList = Array.from(new Array(angular.fromJson(response.data.pages)),(val,index)=>index+1);
            			EmployeeHomeController.endNumber = ((EmployeeHomeController.selectedPage - 1)* EmployeeHomeController.pageSize)  + angular.fromJson(response.data.teamHistory.length);
            			console.log(scope.totalCount);
            		},function(reason){});
        		}
        		if(scope.selectedTransaction == 'team' && !scope.selectedHistory){
        			BusinessLogic.PostMethod('getEmployeeTeamDetails', {'empID' : EmployeeHomeController.SelectedEmployeeInfo.iD, 'pageNo': EmployeeHomeController.selectedPage, 'pageSize': EmployeeHomeController.pageSize, 'search':EmployeeHomeController.searchValue}).then(function(response){	          		
        				EmployeeHomeController.teamDetails = angular.fromJson(response.data.teamDetails); 
            			scope.totalCountTeam = angular.fromJson(response.data.totalCount);
            			scope.pages = angular.fromJson(response.data.pages);
            			scope.pagesList = Array.from(new Array(angular.fromJson(response.data.pages)),(val,index)=>index+1);
            			EmployeeHomeController.endNumber = ((EmployeeHomeController.selectedPage - 1)* EmployeeHomeController.pageSize)  + angular.fromJson(response.data.teamDetails.length);
            			console.log(scope.totalCount);
            		},function(reason){});
        		}
        	}
        	
        	
        	function BindUserData(){ 
        		BusinessLogic1.GetMethod('empDetailsWithToken').then(function(response){ 
        			EmployeeHomeController.SelectedEmployeeInfo = angular.copy(response);     			
        			rootScope.EmployeeID = EmployeeHomeController.SelectedEmployeeInfo.iD;
        			scope.isCT=EmployeeHomeController.SelectedEmployeeInfo.coreTeam;
        			if(EmployeeHomeController.SelectedEmployeeInfo.iD == 21100){
						state.go('Dashboard.Admin.AdminHome');
        			}
        			BindNoOfLeaves(); 
        			BindTeamAndMe(); 
        			BindCurrentLeaveBalance();
        			
        		},function(){});
        	}
        
        	EmployeeHomeController.individualDetails = function(data){      		
        		if(data.viewAccess==1){ 
        			rootScope.iD=data.employeeID; 
        			state.go('Dashboard.Employee.individualDetails');
        		}
        	}
        
        		function BindCurrentLeaveBalance(){ 
           		 	BusinessLogic.PostMethod('getEmployeeInfo', EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){ 
           			 EmployeeHomeController.SelectedEmployeeCurrentLeaveBalance=angular.fromJson(response.data.currentleaveBalance);  
           			scope.isCT=EmployeeHomeController.SelectedEmployeeInfo.coreTeam;
           			},function(){});
           		}
        		
        	 	EmployeeHomeController.EmployeeCompoffDetails = function(value){
            		
                 	 mdDialog.show({
      	                  clickOutsideToClose: true,
      	                  scope: scope,        
      	                  preserveScope: true,    
      	    		      fullscreen: 'md' ,       
      	                  templateUrl: 'CompOffCheck.html',
      	                  controller: function DialogController($scope, $mdDialog) {
      	                	  
      	                	BusinessLogic.PostMethod('individualEmployeeCompOffCreditList',EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){ 
      	                  	EmployeeHomeController.compOffDetails = response.data; 
      	                  	},function(){});
      	                	
      	                	EmployeeHomeController.totalCompOffs = value;
      		 	                	  	$scope.closeView = function() {       			 	                		
      			 	                 		mdDialog.hide();  
      			 	                 		}
      	                	}
      	               });
              		
             	 }
        	 	
              	EmployeeHomeController.UnpaidLeavesCheck = function(flag){ 
              		if( (flag == "ShowYeartoDate" && EmployeeHomeController.YearToDateLeaveConsolidation.noOfUnpaidLeaves >0) || (flag == "ShowMonthtoDate" && EmployeeHomeController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves >0) ){
              				
            		 mdDialog.show({
      	                  clickOutsideToClose: true,
      	                  scope: scope,        
      	                  preserveScope: true,    
      	    		      fullscreen: 'md' ,       
      	                  templateUrl: 'UnPaidLeavesCheck.html',
      	                  controller: function DialogController($scope, $mdDialog) {
      	                  
  	                	BusinessLogic.PostMethod('getUnpaidleavesInfo',EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){  
                 		 EmployeeHomeController.UnpaidLeaveDetails = response.data;
                 	     if(flag == "ShowYeartoDate"){ 
                 	    	EmployeeHomeController.totalLeaves = EmployeeHomeController.YearToDateLeaveConsolidation.noOfUnpaidLeaves;
      		 	          EmployeeHomeController.UnpaidLeavesdata = EmployeeHomeController.UnpaidLeaveDetails.YearToDate; 
      		 	         }
	 	                 else if (flag == "ShowMonthtoDate"){ 
	 	                  EmployeeHomeController.totalLeaves = EmployeeHomeController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves
	 	            	  EmployeeHomeController.UnpaidLeavesdata = EmployeeHomeController.UnpaidLeaveDetails.MonthToDate; 
	 	                 }
             		   },
  	             	function(){});
  	                	
	 	           $scope.closeView = function() {    			 	                		
	 	            mdDialog.hide();  
	 	          }
      	         }
      	        });
              		
              }
             }
              	
              	EmployeeHomeController.PaidLeavesCheck = function(flag){ 
              		if((flag == "ShowYeartoDate" && EmployeeHomeController.YearToDateLeaveConsolidation.noOfPaidLeaves > 0) || (flag == "ShowMonthtoDate" && EmployeeHomeController.MonthToDateLeaveConsolidation.noOfPaidLeaves > 0) ){
          				 
           		 mdDialog.show({
     	                  clickOutsideToClose: true,
     	                  scope: scope,        
     	                  preserveScope: true,    
     	    		      fullscreen: 'md' ,       
     	                  templateUrl: 'PaidLeavesCheck.html',
     	                  controller: function DialogController($scope, $mdDialog) {
     	                
 	                	 BusinessLogic.PostMethod('getpaidleavesInfo',EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){  
 	       	         		EmployeeHomeController.paidLeaveDetails = response.data;
 	       	         		     
 	       	         		  if(flag == "ShowYeartoDate"){ 
 	       	         			EmployeeHomeController.totalLeaves = EmployeeHomeController.YearToDateLeaveConsolidation.noOfPaidLeaves
 	       	         		   	EmployeeHomeController.PaidLeavesdata = EmployeeHomeController.paidLeaveDetails.YearToDate;
 	       	         		   	
 	     		 	          }
 	    	 	              else if (flag == "ShowMonthtoDate"){ 
 	    	 	              EmployeeHomeController.PaidLeavesdata = EmployeeHomeController.paidLeaveDetails.MonthToDate; 
 	    	 	              EmployeeHomeController.totalLeaves = EmployeeHomeController.MonthToDateLeaveConsolidation.noOfPaidLeaves
 	    	 	               }
 	       	         		    
 	         		       	},
 	         		     function(){});	              
     	                	  
     		 	            
	              $scope.closeView = function() {    			 	                		
	            mdDialog.hide();  
	          }
     	     }
     	   });
          }
        }
        	 	
        	/* 	EmployeeHomeController.individualDetails = function(md){         	 		
        	 		
        	 		state.go('Dashboard.Employee.individualDetails'); 
        	 		 BusinessLogic.PostMethod('getEmployeeInfo', md).then(function(response){ 
        	 		EmployeeHomeController.SelectedEmployeeInfo = angular.fromJson(response.data.employyeInfo[0]);  
        	 		EmployeeHomeController.SelectedEmployeeInfo.employeeStatus='Notice'; 
        	 		EmployeeHomeController.SelectedEmployeeCurrentLeaveBalance=angular.fromJson(response.data.currentleaveBalance); 
	 					
	        		},function(){});
        	 		
        	 	}*/       	
        	
        	
        	EmployeeHomeController.ApproveLeave=function(ev,md){  
        		md.approvedByID = EmployeeHomeController.SelectedEmployeeInfo.iD; 
        		md.leaveStatus ="1";
        		var confirm = mdDialog.confirm().title("Click 'Yes' to confirm approval. Click 'No' to go back.").targetEvent(ev).ok('Yes').cancel('No');
        		mdDialog.show(confirm).then(function () { 
        			BusinessLogic.PostMethod('approveOrRejectLeaveRequest', md).then(function(response){ 
           			 	if(response.status==200){ 
           			 		BindCurrentLeaveBalance();
           			 		BindTeamAndMe();
           			 		BindNoOfLeaves();
           			 		rootScope.$emit("BindCalender", {});
           			 		notification.notify(response.data.status);
           			 	}
           			 	else{
           			 	}
   	 				},function(){});
        		 }, function () { });
        	}
              	
        	EmployeeHomeController.RemindLeave=function(md){        		
        		md.approvedByID = EmployeeHomeController.SelectedEmployeeInfo.iD; 
            	md.emailId =  EmployeeHomeController.SelectedEmployeeInfo.email;            	
        			BusinessLogic.PostMethod('remindLeaveRequest', md).then(function(response){ 
           			 	if(response.status==200){ 
           			 		BindTeamAndMe();
           			 		notification.notify(response.data.status);
           			 	}
           			 	else{
           			 	}
   	 				},function(){});
        		 
        	}
        	
        	        	
       /* 	EmployeeHomeController.RejectLeave=function(ev,md){ 
        		var confirm = mdDialog.confirm().title('Reason for rejecting the request').targetEvent(ev).ok('Yes').cancel('No');
        		mdDialog.show(confirm).then(function () { 
        			BusinessLogic.PostMethod('rejectLeaveRequest', md).then(function(response){
           			 	if(response.status==200){
           			 		notification.notify('Leave rejected.');
           			 	}
           			 	else{
           			 	}
   	 				},function(){});
        		 }, function () { });
        	}*/
        	EmployeeHomeController.RejectLeave = function(md){ 
            	 mdDialog.show({ 
 	                  clickOutsideToClose: true,
 	                  scope: scope,        
 	                  preserveScope: true,    
 	    		      fullscreen: 'md' ,       
 	                  templateUrl: 'RejectLeave.html',
 	                  controller: function DialogController($scope, $mdDialog) {
 	                	 $scope.reject ={}; 
 	                	EmployeeHomeController.RejectData = md;
	                	  $scope.closeView = function() { 
	                	  mdDialog.hide(); 	                	 
	                	  }
 	                	 EmployeeHomeController.Reject = function(ev, form, data){ 
 	                		if(form.$invalid){  
 	               			angular.forEach(form.$error, function (field) {
 	                               angular.forEach(field, function(errorField){ errorField.$setTouched(); })
 	                        });
 	               		}
 	                	else {
 	                	 	md.reasonForRejectionORRevoke = data.reasonForRejectionORRevoke;
 	                	 	md.approvedByID = EmployeeHomeController.SelectedEmployeeInfo.iD;
 	                	 	
 	                	 	md.leaveStatus = "3"; 
 	                		BusinessLogic.PostMethod('approveOrRejectLeaveRequest', md).then(function(response){ 
 	              			 	if(response.status==200){ 
 	              			 		mdDialog.hide(); 	            
 	              			 		BindTeamAndMe();
 	              			 		BindNoOfLeaves();
 	              			 		BindCurrentLeaveBalance(); 
 	              			 		notification.notify(response.data.status);
 	              			 	}
 	              			 	else{ mdDialog.hide(); notification.notify(response.data.status); }
 	      	 				},function(){}); 	                		 
 	                	 }
 	                	 }
 	                	 
 	                	
 	                 }
 	               });
            	};
        	
            	EmployeeHomeController.RevokeByEmployee=function(ev,md){  
            		md.employeeID = EmployeeHomeController.SelectedEmployeeInfo.iD;  
            		
            		var confirm = mdDialog.confirm().title('Do you want to revoke your request? Click "Yes" to continue or "No" to go back.').targetEvent(ev).ok('Yes').cancel('No');
            		mdDialog.show(confirm).then(function () { 
            			BusinessLogic.PostMethod('cancelLeaveByEmployee', md).then(function(response){ 
               			 	if(response.status==200){ 
               			 	BindTeamAndMe(); 
          			 		BindCurrentLeaveBalance(); 
          			 		BindNoOfLeaves();
          			 		rootScope.$emit("BindCalender", {});
          			 		mdDialog.hide(); 	              			 		
          			 		notification.notify(response.data.status);
               			 	}
               			 	else{
               			 		mdDialog.hide(); notification.notify(response.data.status);
               			 	}
       	 				},function(){});
            		 }, function () { });
            	} 
            	
        	EmployeeHomeController.ApplyForLeave = function(){
        		loadEmployeeEmails();
        		getDropDownLeaves();
        		BindSessionData();
        	 mdDialog.show({
	                  clickOutsideToClose: false,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'ApplyForLeave.html',
	                  multiple:true,
	                  controller: function DialogController($scope, $mdDialog) { 
	                	  EmployeeHomeController.SelectedEmails = [];
                		  EmployeeHomeController.searchText = null; 
              			  scope.ApplyLeave={};
              			  $scope.closeView = function() {
	              			  mdDialog.hide(); 
	                		}
              			  $scope.remove=function(item){
	                		 $scope.EmployeeHomeController.SelectedEmails.splice( item, 1);
	                         }
              			  scope.ApplyLeave = { sessionOfEndDate : "2", sessionOfStartDate : "2" }
	                	}
	               });
        	}
        	
        	EmployeeHomeController.ApplyForWorkFromHome = function(){
        		loadEmployeeEmails();
        		BindSessionData();
            	 mdDialog.show({
 	                  clickOutsideToClose: true,
 	                  scope: scope,        
 	                  preserveScope: true,    
 	    		      fullscreen: 'md' ,       
 	                  templateUrl: 'ApplyForWorkFromHome.html',
 	                  controller: function DialogController($scope, $mdDialog) {
 	                	 EmployeeHomeController.SelectedEmails = [];
	                	 EmployeeHomeController.searchText = null;
	                 	 scope.WorkFromHome={};
	                 	 
		 	             $scope.closeView = function() { 
	 	                 		mdDialog.hide();  
	 	                		}
 	                	 $scope.remove=function(item){
 	                		 $scope.EmployeeHomeController.SelectedEmails.splice( item, 1);
	                         }
 	                	scope.WorkFromHome = { sessionOfEndDate : "2", sessionOfStartDate : "2" } 
 	                	
 	                	}
 	               });
        	 }
        	
        	EmployeeHomeController.ApplyForCompOff = function(){
        		loadEmployeeEmails();
        		BindSessionData();
            	 mdDialog.show({
 	                  clickOutsideToClose: true,
 	                  scope: scope,        
 	                  preserveScope: true,    
 	    		      fullscreen: 'md' ,       
 	                  templateUrl: 'ApplyForCompOff.html',
 	                  controller: function DialogController($scope, $mdDialog) {
 	                	 EmployeeHomeController.SelectedEmails = [];
	                	 EmployeeHomeController.searchText = null; 
	                	 scope.Compoff={};
	                		 
 	                	  $scope.closeView = function() {
	 	                 		mdDialog.hide(); 
	 	                	  }
 	                	 $scope.remove=function(item){
 	                		 EmployeeHomeController.SelectedEmails.splice( item, 1);
	                         }
 	                	scope.CompOff =  { sessionOfEndDate : "2", sessionOfStartDate : "2" } 
 	                	
 	                	}
 	               });
        	 }
        	
 			function loadEmployeeEmails() {
	 			    
 				BusinessLogic.PostMethod('getEmailRecipientsList',EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){
	 					scope.EmployeeList = angular.fromJson(response.data);  
	 	               	scope.Employees = scope.EmployeeList.map(function(emp) { emp.lowername = emp.employeeName.toLowerCase(); return emp; });
	        		},function(){});
 				
 				BusinessLogic.PostMethod('getDefaultEmailRecipients',EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){
 					scope.EmployeeDefaultRec = angular.fromJson(response.data);  
 	               	
        		},function(){});
 				
 				
 				}

 			function getDropDownLeaves(){
 				BusinessLogic.PostMethod('getDropdownLeaves',EmployeeHomeController.SelectedEmployeeInfo.iD).then(function(response){
        			EmployeeHomeController.LeaveTypes = angular.fromJson(response.data); 
        		},function(){});
 			}  
        	

        	EmployeeHomeController.querySearch =function(query) {				
  			  		return query ? scope.Employees.filter( createFilterFor(query) ) : scope.Employees;
	            }
      	
        	function createFilterFor(query) { 
				 	var as= EmployeeHomeController.selectedItem;  
				 	var lowercaseQuery = angular.lowercase(query); 			       			      	
				 		return function filterFn(emp) {  
				 			return (emp.employeeName.toLowerCase().indexOf(lowercaseQuery) === 0); 
				 				};
			     } 			

        	EmployeeHomeController.selectedItemChange = function(as){
        		if(as!==null){
        			EmployeeHomeController.selectedItem = null; EmployeeHomeController.searchText = null;
        			var isExists=false;
        			if(EmployeeHomeController.SelectedEmails.length == 0){ EmployeeHomeController.SelectedEmails.push(angular.copy(as));  }
        			
        			angular.forEach(EmployeeHomeController.SelectedEmails,function(data){
        					if(data.employeeID == as.employeeID){ isExists=true;} 
            		})
            		if(!isExists){
            			EmployeeHomeController.SelectedEmails.push(angular.copy(as)); 
            		}
        		}
        		        		 
        	}   
        	
        	EmployeeHomeController.ApplyLeave = function(ev,form,data){
          		if(form.$invalid){  
          			angular.forEach(form.$error, function (field) {
                          angular.forEach(field, function(errorField){
                            errorField.$setTouched();  
                          })
                        });
          		}
          		else{  
          			if(!data.leaveFromDayHalf){  data.sessionOfStartDate = "1"; data.leaveFromDayHalf= false;}
              		if(!data.leaveToDayHalf){ data.sessionOfEndDate = "1"; data.leaveToDayHalf = false;}
              		
              		data.DefaultEmails =angular.copy(scope.EmployeeDefaultRec);
              		data.isApproveBySLT=0;
              		angular.forEach(EmployeeHomeController.SelectedEmails,function(value,key){
              			data.DefaultEmails.push(value); 
              		})
              		data.employeeID=EmployeeHomeController.SelectedEmployeeInfo.iD;	
              		
              		BusinessLogic.PostMethod('saveLeaveDetails',data).then(function(response){
              			mdDialog.hide(); 
          			scope.ApplyLeave={};  
          			EmployeeHomeController.SelectedEmails = [];
                  	EmployeeHomeController.searchText = null;
                  	BindTeamAndMe();
                  	notificationWithNote.notify(response.data.status,response.data.note);    
          		},function(reason){
           			if(reason.data.isApproveBySLT == 1){
          				mdDialog.show({
        					clickOutsideToClose: false,
        					skipHide: true,
        					template: '<md-dialog flex="40" flex-md="60" flex-xs="100" flex-sm="100" aria-label="confirmation" md-theme="meritus">' +
        					'  <md-dialog-content class="md-dialog-content"><h2 class="md-title" style="text-align:left; padding: 10px"><p ng-bind="Msg" style="margin: 0"></p></h2></md-dialog-content>' +
        					'  <md-dialog-actions layout="row" layout-align="end center">' +
        					'    <md-button ng-click="yesDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">Yes</md-button>' +
        					'    <md-button ng-click="closeDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">No</md-button>' +
        					'  </md-dialog-actions>' +
        					'</md-dialog>',
        					controller: function DialogController($mdDialog, $scope) {
        						$scope.Msg = reason.data.status;
        						$scope.closeDialog = function () { $mdDialog.hide(); }
        						$scope.yesDialog = function () { 
        							reason.data.data.isApproveBySLT=1;
              						BusinessLogic.PostMethod('saveLeaveDetails',reason.data.data).then(function(response){
              							mdDialog.hide();
              							BindTeamAndMe();
              							notificationWithNote.notify(response.data.status,response.data.note);
              		                	mdDialog.hide();
      	            				},function(reason){});
        						}
        					}
        				});	
           			}
           			else{
           				notification.notify(reason.data.status);
           			}
          			
          			
          		});

          		}
          	}
        	
        	EmployeeHomeController.ApplyWorkFromHome = function(ev,form,data){
        		
        		if(form.$invalid){
        			angular.forEach(form.$error, function (field) {
                        angular.forEach(field, function(errorField){
                          errorField.$setTouched();
                        })
                      });
        		}
        		else{
        			if(!data.leaveFromDayHalf){  data.sessionOfStartDate = "1"; data.leaveFromDayHalf = false;}
            		if(!data.leaveToDayHalf){ data.sessionOfEndDate = "1"; data.leaveToDayHalf = false;}
            		 
            		data.DefaultEmails =angular.copy(scope.EmployeeDefaultRec);
            		data.isApproveBySLT=0;
            		angular.forEach(EmployeeHomeController.SelectedEmails,function(value,key){
            			data.DefaultEmails.push(value);
            		})            		
            		data.employeeID=EmployeeHomeController.SelectedEmployeeInfo.iD;
            		BusinessLogic.PostMethod('saveWfhDetails',data).then(function(response){
            			mdDialog.hide(); 
                    	BindTeamAndMe();
                    	notification.notify(response.data.status);  
            		},function(reason){ 
                		if(reason.data.isApproveBySLT == 1){
                			
                			mdDialog.show({
            					clickOutsideToClose: false,
            					skipHide: true,
            					template: '<md-dialog flex="40" flex-md="60" flex-xs="100" flex-sm="100" aria-label="confirmation" md-theme="meritus">' +
            					'  <md-dialog-content class="md-dialog-content"><h2 class="md-title" style="text-align:left; padding: 10px"><p ng-bind="Msg" style="margin: 0"></p></h2></md-dialog-content>' +
            					'  <md-dialog-actions layout="row" layout-align="end center">' +
            					'    <md-button ng-click="yesDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">Yes</md-button>' +
            					'    <md-button ng-click="closeDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">No</md-button>' +
            					'  </md-dialog-actions>' +
            					'</md-dialog>',
            					controller: function DialogController($mdDialog, $scope) {
            						$scope.Msg = reason.data.status;
            						$scope.closeDialog = function () { $mdDialog.hide(); }
            						$scope.yesDialog = function () { 
            							reason.data.data.isApproveBySLT=1;
                  						BusinessLogic.PostMethod('saveWfhDetails',reason.data.data).then(function(response){
                  							mdDialog.hide();
                  							BindTeamAndMe();
                  		                	notification.notify(response.data.status);
                  		                	mdDialog.hide();
          	            				},function(reason){});
            						}
            					}
            				});	
                			}
                		else{
                			notification.notify(reason.data.status);
                		}
            		});
        		}
        	}
        	
        	EmployeeHomeController.ApplyCompOff = function(ev,form,data){
        		
        		if(form.$invalid){
        			angular.forEach(form.$error, function (field) {
                        angular.forEach(field, function(errorField){
                          errorField.$setTouched();
                        })
                      });
        		}
        		else{
        			
		        		if(!data.CompOffFromDayHalf){  data.sessionOfStartDate = "1"; data.CompOffFromDayHalf = false;}
		        		if(!data.CompOffToDayHalf){ data.sessionOfEndDate = "1"; data.CompOffToDayHalf = false;} 
		        		data.isApproveBySLT=0;
		        		data.DefaultEmails =angular.copy(scope.EmployeeDefaultRec);
		        		angular.forEach(EmployeeHomeController.SelectedEmails,function(value,key){
		        			data.DefaultEmails.push(value);
		        		})
		        		data.employeeID=EmployeeHomeController.SelectedEmployeeInfo.iD;
		        		
		        		BusinessLogic.PostMethod('saveCompOffDetails',data).then(function(response){
		                	mdDialog.hide();
		        			BindTeamAndMe();
		                	notification.notify(response.data.status );
		        		},function(reason){
		            		notification.notify(reason.data.status);
		        		});
        		}
        	}
        	
        	
        	
        	
           
        	
        }],
        controllerAs: 'EmployeeHomeController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Employee/EmployeeHome.html'
		}
})
.directive('siloView',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','BusinessLogic','Menu', function(window, state, Q, mdDialog,scope,rootScope,BusinessLogic,Menu){
        	var SiloController=this;
        	rootScope.hideMainCalendar = true;
        	scope.loadingPanel = true;
        	rootScope.selectedModule="Employee";
        	rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        	rootScope.selectedView='Dashboard.Employee.SiloView';
			 
        		BindEmployeeOrgStructure();
        	function BindEmployeeOrgStructure(){
        		BusinessLogic.PostMethod('getEmployeeOrgStrucuteforCEO',rootScope.EmployeeID).then(function(response){
        			SiloController.urgstructuredetails = [];
        			SiloController.urgstructuredetails = [angular.copy(response.data)];         		
        			scope.loadingPanel = false;        			
        		},function(){});
        		
        		SiloController.EmpDetails = function(EmployeeID){
        			rootScope.iD = EmployeeID;            		
            		state.go('Dashboard.Employee.individualDetails'); 
            	}       			
        		
        	}
        	
        	
        }],
        controllerAs: 'SiloController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Employee/Silo.html'
		}
})
.directive('adminHomeView',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','BusinessLogic','httpPreConfig','BusinessLogic1','notification','Menu', function(window, state, Q, mdDialog,scope,rootScope,BusinessLogic,httpPreConfig,BusinessLogic1,notification,Menu){
        	var AdminHomeController=this;
        	rootScope.selectedModule="Admin";
        	rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        	rootScope.selectedView='Dashboard.Admin.AdminHome'
        		var AuthenticationToken = "";
        	rootScope.hideMainCalendar = false;
        		scope.selectedTransaction="team";
        		AdminHomeController.selectedPage = 1;  
        		scope.pages = 1;
        		AdminHomeController.pageSize = 10;
        		scope.rowCount =[10,20,30];        		
        		AdminHomeController.searchValue = null;
        		        		
        		AdminHomeController.changeTab = function(data){ 
        			scope.selectedTransaction=data;
        			AdminHomeController.selectedPage = 1; 
            		AdminHomeController.pageSize = 10;
            		AdminHomeController.searchValue = null;          		
            		
        			if(data == 'team'){ 
        				BindTeam();
        			}
        			if(data == 'History'){
        				BindHistory();
        			}        			
        		}
        	
        		AdminHomeController.individualDetails = function(data){         		
            			rootScope.iD=data.employeeID; 
            			state.go('Dashboard.Admin.EmpDetails.individualDetails');
            	}
        		
        		
        		function BindHistory() {       			
            		
            		BusinessLogic.PostMethod('getAdminHistoryDetails', {'pageNo': AdminHomeController.selectedPage, 'pageSize': AdminHomeController.pageSize, 'search':AdminHomeController.searchValue}).then(function(response){	          		
        			AdminHomeController.teamDetailsHistory = angular.fromJson(response.data.myHistory); 
        			scope.totalCount = angular.fromJson(response.data.totalCount);
        			scope.pages = angular.fromJson(response.data.pages);
        			scope.pagesList = Array.from(new Array(angular.fromJson(response.data.pages)),(val,index)=>index+1);
        			AdminHomeController.startNumber = ((AdminHomeController.selectedPage - 1)* AdminHomeController.pageSize) +1; 
            		AdminHomeController.endNumber = ((AdminHomeController.selectedPage - 1)* AdminHomeController.pageSize) + angular.fromJson(response.data.myHistory.length); 
        		},function(reason){});            		
            	}
        		AdminHomeController.teamDetailsHistory ='';
        		
        		
        		AdminHomeController.pageChange = function(selectedPage, rowsPerPage, selectedTransaction){
        			if(selectedPage > 0 && selectedPage <= scope.pages){ 
        				AdminHomeController.selectedPage = selectedPage;        			
        				AdminHomeController.pageSize = rowsPerPage;
        			
        			if(selectedTransaction == 'History'){
        				BindHistory();
        			}
        			else if(selectedTransaction == 'team'){ 
        				BindTeam();
        			}
        		  }
        		}
        		scope.onSearch = function(searchValue){        			
        			AdminHomeController.searchValue = searchValue;
        			(scope.selectedTransaction == 'team')? BindTeam() : BindHistory(); 
        			
        		}
        		
        		BindTeam();
            	function BindTeam() {
            		BusinessLogic.PostMethod('getAdminTeamDetails', {'pageNo': AdminHomeController.selectedPage, 'pageSize': AdminHomeController.pageSize, 'search':AdminHomeController.searchValue}).then(function(response){ 	            		
	        			AdminHomeController.myDetailsHistory = angular.fromJson(response.data.teamHistory); 	        			
	        			scope.totalCount = angular.fromJson(response.data.totalCount);
	        			scope.pages = angular.fromJson(response.data.pages);
	        			scope.pagesList = Array.from(new Array(angular.fromJson(response.data.pages)),(val,index)=>index+1);           		
	        			AdminHomeController.startNumber = ((AdminHomeController.selectedPage - 1)* AdminHomeController.pageSize) + 1;
	        			AdminHomeController.endNumber = ((AdminHomeController.selectedPage - 1)* AdminHomeController.pageSize)  + angular.fromJson(response.data.teamHistory.length);
	        			
            		},function(reason){});
                	
            	}
            	
            	BindUserData();
            	function BindUserData(){
            		BusinessLogic1.GetMethod('empDetailsWithToken').then(function(response){
            			AdminHomeController.userDetails=angular.copy(response);             			
            		},function(){});
            		
            	}
            	
            	AdminHomeController.RejectLeave = function(md){
               	 mdDialog.show({ 
    	                  clickOutsideToClose: true,
    	                  scope: scope,        
    	                  preserveScope: true,    
    	    		      fullscreen: 'md' ,       
    	                  templateUrl: 'RejectLeave.html',
    	                  controller: function DialogController($scope, $mdDialog) {
    	                	 $scope.reject ={}; 
    	                	  $scope.closeView = function() { 
    	                	  mdDialog.hide(); 	                	 
    	                	  }
    	                	  AdminHomeController.RejectData = md;
    	                	  AdminHomeController.Reject = function(ev, form, data){ 
    	                			if(form.$invalid){  
    	 	 	               			angular.forEach(form.$error, function (field) {
    	 	 	                               angular.forEach(field, function(errorField){
    	 	 	                                 errorField.$setTouched();  
    	 	 	                               })
    	 	 	                             });
    	 	 	               		}
    	                			else {
		    	                	 	md.reasonForRejectionORRevoke = data.reasonForRejectionORRevoke; 
		    	                	 	md.approvedByID  = AdminHomeController.userDetails.iD;
		    	                	 	md.leaveStatus = "3";
		    	                		BusinessLogic.PostMethod('approveOrRejectLeaveRequest', md).then(function(response){ 
		    	              			 	if(response.status==200){ 
		    	              			 		mdDialog.hide();
		    	              			 		if(scope.selectedTransaction == 'History'){
		    	                    				BindHistory();
		    	                    			}
		    	                    			else if(scope.selectedTransaction == 'team'){ 
		    	                    				BindTeam();
		    	                    			}
		    	              			 		BindTeamAndMe(); 
		    	              			 		rootScope.$emit("BindCalender", {});		 	 
		    	              			 		notification.notify(response.data.status);
		    	              			 	}
		    	              			 	else{ mdDialog.hide(); notification.notify(response.data.status); }
		    	      	 				},function(){}); 	                		 
    	                			}
    	                	  }    	                	
    	                  }
    	               });
               	};
               	
               	AdminHomeController.ApproveLeave=function(ev,md){ 
               		md.approvedByID = AdminHomeController.userDetails.iD;
               		md.leaveStatus = "1"; 
               		
            		var confirm = mdDialog.confirm().title("Click 'Yes' to confirm approval. Click 'No' to go back.").targetEvent(ev).ok('Yes').cancel('No'); 
            		
            		mdDialog.show(confirm).then(function () { 
            			BusinessLogic.PostMethod('approveOrRejectLeaveRequest', md).then(function(response){
               			 	if(response.status==200){ 
               			 		BindTeam();	
               			 		rootScope.$emit("BindCalender", {});
               			 		notification.notify(response.data.status);
               			 	}
               			 	else{
               			 	}
       	 				},function(){});
            		 }, function () { });
            	}
               	
               	
               	AdminHomeController.UnPaidToPaidApprove = function(ev,data){
            		var confirm = mdDialog.confirm().title('Do you want to approve paid leave conversion request? Click "Yes" to continue or "No" to go back.').targetEvent(ev).ok('Yes').cancel('No');
            		mdDialog.show(confirm).then(function () { 
            			data.approvedByORRejectedBy=rootScope.userData.iD;
            		BusinessLogic.PostMethod('approveFromUnpaidToPaid',data).then(function(response){
            				BindTeam()    	          			
    	          			notification.notify(response.data.status);
    	          			mdDialog.hide();
      	          		},function(reasone){
      	          			notification.notify(reasone.data.status);
      	          		});
            		}, function () { });
          		}
            	
               	AdminHomeController.UnPaidToPaidReject = function(md){
            		/*var confirm = mdDialog.confirm().title('Do you want to cancel unpaid leave conversion request? Click "Yes" to continue or "No" to go back.').targetEvent(ev).ok('Yes').cancel('No');
            		mdDialog.show(confirm).then(function () { 
            				BusinessLogic.PostMethod('rejectFromUnpaidToPaid',data).then(function(response){
            					BindTeam()
            					notification.notify(response.data.status);
            					mdDialog.hide();
        	          			},function(reasone){
        	          				notification.notify(reasone.data.status);
        	          			});
               		 }, function () { });*/
               		
               		var confirm =  mdDialog.show({ 
  	                  clickOutsideToClose: true,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'RejectSystemGeneratedLeavesFromAdmin.html',
	                  controller: function DialogController($scope, $mdDialog) {
	                	 $scope.reject ={}; 
	                	  $scope.closeView = function() { 
	                	  mdDialog.hide(); 	                	 
	                	  }
	                	  AdminHomeController.rejectFromUnpaidToPaid = md;
	                	  AdminHomeController.UnPaidReject = function(ev, form, data){ 
	                		  if(form.$invalid){  
	 	 	               			angular.forEach(form.$error, function (field) {
	 	 	                               angular.forEach(field, function(errorField){
	 	 	                                 errorField.$setTouched();  
	 	 	                               })
	 	 	                             });
	 	 	               		}
	                			else {
	    	                	 	md.biometricIssueReasonForRejectionOrRevoke = data.reasonForRejectionORRevoke; 
	    	                	 	md.approvedByORRejectedBy  = AdminHomeController.userDetails.iD;
	    	                	 
	    	                		BusinessLogic.PostMethod('rejectFromUnpaidToPaid', md).then(function(response){ 
	    	              			 	if(response.status==200){ 
	    	              			 		mdDialog.hide();
	    	              			 		if(scope.selectedTransaction == 'History'){
	    	                    				BindHistory();
	    	                    			}
	    	                    			else if(scope.selectedTransaction == 'team'){ 
	    	                    				BindTeam();
	    	                    			}
	    	              			 		/*BindTeamAndMe(); */
	    	              			 		rootScope.$emit("BindCalender", {});		 	 
	    	              			 		notification.notify(response.data.status);
	    	              			 	}
	    	              			 	else{ mdDialog.hide(); notification.notify(response.data.status); }
	    	      	 				},function(){}); 	                		 
	                			}
	                		  
	                	  }    	                	
	                  }
	               });
            	} 	
        	
        	
        }],
        controllerAs: 'AdminHomeController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Admin/AdminHome.html'
		}
})

	
.directive('settingsView',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','httpPreConfig','notification','$rootScope','BusinessLogic','Menu', function(window, state, Q, mdDialog,scope,httpPreConfig,notification,rootScope,BusinessLogic,Menu){
        	var SettingsController=this;
        	rootScope.selectedModule="Admin";
        	rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        	rootScope.selectedView='Dashboard.Admin.Settings';
        	var AuthenticationToken="";
        	rootScope.hideMainCalendar = false;
        	SettingsController.selected={};
        	scope.LeaveType={};
        	scope.ExistingHoliday={};
        	SettingsController.selectedLeaveData={};
        	SettingsController.CurrentTableselected={};
        	SettingsController.CurrentTableselectedData={};
        	
        	scope.myDate = new Date();
        	scope.minDate = new Date(
        		      scope.myDate.getFullYear(),
        		      scope.myDate.getMonth(),
        		      scope.myDate.getDate());
        	SettingsController.minDate= new Date();
        	
        	scope.listStyle = { height: (window.innerHeight - 210) + 'px' };
        	scope.BodylistStyle = { height: (window.innerHeight - 500) + 'px' };
        	
        	SettingsController.BindData= function(){
        		Bind();
        	}
        	
        	
        	SettingsController.LeaveTypeEdit = function(index,data){
        		scope.LeaveType1={};
        		scope.LeaveType={};
        		
        		if(data.status=='lt'){
            		SettingsController.selected=index;
            		scope.LeaveType=angular.copy(data);
        		}
        		else{
        			SettingsController.selected=index;
            		scope.LeaveType1=angular.copy(data);
        		}
        		SettingsController.LT_OnChange(data);
        		
        	}
        	
        	SettingsController.LT_OnChange = function(data){
        		
        		if(data.status=='lt'){
            		if(parseInt(data.maxLeavesAllowedAtOnce) <= parseInt(data.totalLeavesAllowed)){
            			scope.isLTError=false;
            		}else{
            			scope.isLTError=true;
            		}
        		}
        		else{
        			if(parseInt(data.totalLeavesAllowed) <= 12){
            			scope.isESError=false;
            		}else{
            			scope.isESError=true;
            		}
        		}
        		
        		
        		
        	}
        	
        	SettingsController.HolidayListEdit = function(index,hl){
        		SettingsController.CurrentTableselected=index;
        		scope.ExistingHoliday=angular.copy(hl);
        		scope.ExistingHoliday.holidayDate = new Date(scope.ExistingHoliday.holidayDate);
        	}
        	
        	
        	Bind();
        	function Bind(){
        		BindLeavePolicySettings();
        		BindHolidayType();
            }
        	
        	function BindLeavePolicySettings(){
        		var promise = httpPreConfig({ method: 'GET', url: 'LeavePolicySettings', headers: { 'Content-Type': 'application/json', 'token': AuthenticationToken } });
            	promise.then(function (response) {  SettingsController.holiday_list = angular.fromJson(response.data.Holidays); SettingsController.LeaveTypes=angular.fromJson(response.data.leavePolicySettings); }, function(reason){ });
        	}
        	
        	function BindHolidayType(){
	        		BusinessLogic.GetMethod('getHolidayType').then(function(response){
	            		SettingsController.holidayTypes=angular.fromJson(response);
	            	},function(reason){ });
        	}
        	
        	SettingsController.DateChange = function(data){
        		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        		var t=new Date(data.holidayDate.toLocaleDateString());
        		data.dayOfTheWeek = days[t.getDay()];
        	}
        	
        	
        	SettingsController.SaveLeaveTypes =function(data){
        		
        		data.totalLeavesAllowed=parseInt(data.totalLeavesAllowed);
        		data.maxLeavesAllowedAtOnce=parseInt(data.maxLeavesAllowedAtOnce);
        		if(data.status=='lt'){
        				var promise = httpPreConfig({ method: 'POST', url: 'SaveorUpdateLeaveSettings', headers: { 'Content-Type': 'application/json', 'token': AuthenticationToken },'data':{'empId':21225,'data':data} })
                    	promise.then(function (response) { if(response.status == 200){ scope.LeaveType1={};scope.LeaveType={}; BindLeavePolicySettings(); notification.notify(response.data.status); 
                    	 SettingsController.selected=-1; } }, function(reason){ notification.notify(reason.data.status); });
        		}
        		else{
        			if(data.totalLeavesAllowed){
        				var promise = httpPreConfig({ method: 'POST', url: 'saveOrUpdateProbationAndNoticePeriod', headers: { 'Content-Type': 'application/json', 'token': AuthenticationToken },'data':{'empId':21225,'data':{'employmentStatusID':data.leaveTypeID,'employmentStatus':data.leaveType,'durationInMonths':data.totalLeavesAllowed,'createdBy':data.createdBy}} }),
            			deferObject = deferObject || Q.defer();
            			promise.then(function (response) { if(response.status == 200){ scope.LeaveType1={};scope.LeaveType={}; Bind(); notification.notify(response.data.status); SettingsController.selected=-1; } }, function(reason){ notification.notify(reason.data.status); });
            			}
        			else{
        				notification.notify('Please provide all required details.');
        			}
        			}
        		
        			
        	}
        	
        	SettingsController.SaveorUpdateHolidays=function(data,form,ev){
        		
        		if(data){
        			
        			if(data.holidayName && data.holidayTypeID && data.holidayDate){
            			var promise = httpPreConfig({ method: 'POST', url: 'saveOrUpdateHolidays', headers: { 'Content-Type': 'application/json', 'token': AuthenticationToken },'data':{'empId':21225,'data':data} }),
            			deferObject = deferObject || Q.defer();
                		promise.then(function (response) { notification.notify(response.data.status); scope.Holiday={}; form.$setPristine();
        				form.$setUntouched();  BindLeavePolicySettings();  rootScope.$emit("BindCalender", {});  SettingsController.CurrentTableselected=-1; }, function(reason){
                		}); 
	    			}else{
	    				notification.notify('Please provide all required details.');
	    			}
        			
            		}
        		else{
        			notification.notify('Please provide all required details.');
        		}
        	}
        	
        	
        	SettingsController.DeleteHolidays=function(data){
        		
        		var confirm = mdDialog.confirm().title("Click 'Yes' to confirm deletion. Click 'No' to go back.").targetEvent().ok('Yes').cancel('No');
        		mdDialog.show(confirm).then(function () { 
        			var promise = httpPreConfig({ method: 'POST', url: 'DeleteHolidays', headers: { 'Content-Type': 'application/json', 'token': AuthenticationToken },'data':data }),
        			deferObject = deferObject || Q.defer();
            		promise.then(function (response) { 
    				if(response.status == 200){ BindLeavePolicySettings();  rootScope.$emit("BindCalender", {});  notification.notify(response.data.status); SettingsController.selected=-1;
    	        	SettingsController.CurrentTableselected=-1; }
            		}, function(reason){  }); 
        		}, function () { });
        		
        		
        	}
        	
        	
        	
        	
        }],
        controllerAs: 'SettingsController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Admin/Settings.html'
		}
})
.directive('empDetailsView',function(){
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','httpPreConfig','BusinessLogic','$rootScope','$timeout','notification','$filter','Menu'
        						, function(window, state, Q, mdDialog,scope,httpPreConfig,BusinessLogic,rootScope,timeout,notification,$filter,Menu){
        	var EmpDetailsController=this;
        	rootScope.selectedModule="Admin";
        	rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        	rootScope.selectedView='Dashboard.Admin.EmpDetails'
        	var AuthenticationToken="";
        	rootScope.hideMainCalendar = false;
        	scope.query = { filter: '', order: '', limit: '10', page: 1 };
        	
        	EmpDetailsController.isTable=true;
        	
        	this.myDate = new Date();

        	EmpDetailsController.minDate  = new Date(
        	    this.myDate.getFullYear()-1,
        	    03,
        	    01
        	  );
            
        	EmpDetailsController.maxDate = new Date(
        		    this.myDate.getFullYear() + 1,
        		    02,
        		    31
        		  );
        	
        	Bind();
        	function Bind(){
            	 BusinessLogic.GetMethod('getEmployeeDeatils').then(function(response){ 
                	 	EmpDetailsController.EmployeeDetails = angular.fromJson(response);
                	 	scope.empDetails = EmpDetailsController.EmployeeDetails;
             	},function(reason){ });
               }
        	
        	EmpDetailsController.SelectedEmployee = function(ed){ 
        		rootScope.iD = ed.empID; 
        		state.go('Dashboard.Admin.EmpDetails.individualDetails'); 
        		/*BusinessLogic.PostMethod('getEmployeeInfo', ed.empID).then(function(response){ 
                	EmpDetailsController.SelectedEmployeeInfo = angular.fromJson(response.data.employyeInfo[0]);  
                	EmpDetailsController.SelectedEmployeeInfo.employeeStatus='Notice'; 
                	EmpDetailsController.SelectedEmployeeCurrentLeaveBalance=angular.fromJson(response.data.currentleaveBalance); 
 					
        		},function(){});*/
                
        	}
        	
        	EmpDetailsController.listAccordingTosearch = function(searchText){
        		scope.empDetails = $filter('filter')(EmpDetailsController.EmployeeDetails, searchText);
        		
        	}
        	
        }],
        controllerAs: 'EmpDetailsController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Admin/EmpDetails.html'
		}
})
.directive('individualEmployeeView',function(){ 
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','BusinessLogic','notification','Menu', function(window, state, Q, mdDialog,scope,rootScope,BusinessLogic,notification,Menu){
        	var EmployeeIndividualController=this;
        	rootScope.selectedModule="Admin";
        	rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        	
        	rootScope.hideMainCalendar = false;
        	scope.ShowYeartoDate = true;
        	scope.ShowMonthtoDate = false;
        	scope.flag= "ShowYeartoDate"; 
        	this.myDate = new Date();
        	scope.EmployeeIndividualController.SelectedEmails = []; 
        	
        	rootScope.selectedView='Dashboard.Admin.EmpDetails'
        		EmployeeIndividualController.minDate=new Date();
        	
        	
        	Bind();
        	function Bind(){
        			BindEmployeeInfo()
        			BindStatusPeriods()
        			BindEmployeeStatusDisplay()
        			BindNoOfLeaves()
        			
        		}
        	
        	function BindEmployeeStatusDisplay(){
        		BusinessLogic.PostMethod('getEmployeeStatusDisplay',rootScope.iD).then(function(response){  
          			scope.EmploymentStatus = response.data;
          			if(response.data.StatusEndDate != null){
          				scope.EmploymentStatus.StatusEndDate=new Date(response.data.StatusEndDate);
          			}
          			EmployeeIndividualController.EmployeeCurrentStatusClick("initial",response.data.employmentStatusID);
          		}, function(){});
        	}
        	
        	function BindNoOfLeaves(){
        		BusinessLogic.PostMethod('yearAndMonthToDateStatus',rootScope.iD).then(function(response){ 
        			EmployeeIndividualController.YearToDateLeaveConsolidation = response.data.yearToDateStatus[0];
        			EmployeeIndividualController.MonthToDateLeaveConsolidation = response.data.monthToDateStatus[0];
        			EmployeeIndividualController.compOffMinDate=new Date(angular.copy(EmployeeIndividualController.YearToDateLeaveConsolidation.compoffDate));
        			EmployeeIndividualController.compOffMaxDate=new Date(angular.copy(EmployeeIndividualController.YearToDateLeaveConsolidation.compOffMax));
        			
        			checking(); 
        		},function(){});
        	}
        	
        	function checking(){       	   
                EmployeeIndividualController.value = function(flag, check){ 
            	 if(flag == "ShowYeartoDate" && check == "paidLeaves"){ 
            		 if(EmployeeIndividualController.YearToDateLeaveConsolidation.noOfPaidLeaves > 0){
            			 return true;
            		 }
            	 }
            	 if(flag == "ShowYeartoDate" && check == "UnpaidLeaves"){ 
            		 if(EmployeeIndividualController.YearToDateLeaveConsolidation.noOfUnpaidLeaves >0){
            			 return true;
            		 }
            	 }
            	 if(flag == "ShowMonthtoDate" && check == "paidLeaves"){ 
            		 if(EmployeeIndividualController.MonthToDateLeaveConsolidation.noOfPaidLeaves > 0){
            			 return true;
            		 }
            	 }
            	 if(flag == "ShowMonthtoDate" && check == "UnpaidLeaves"){ 
            		 if(EmployeeIndividualController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves >0){
            			 return true;
            		 }
            	 }
            	}
           }
        	
        	function BindStatusPeriods(){
        		BusinessLogic.PostMethod('getStatusPeriods',rootScope.iD).then(function(response){
              		EmployeeIndividualController.EmploymentStatusValues = response.data;
              		}, function(){});
        	}
        	
        	function BindEmployeeInfo(){
        		BusinessLogic.PostMethod('getEmployeeInfo', rootScope.iD).then(function(response){  
          			EmployeeIndividualController.SelectedEmployeeCurrentLeaveBalance=angular.fromJson(response.data.currentleaveBalance);  
          			EmployeeIndividualController.SelectedEmployeeInfo = angular.fromJson(response.data.employyeInfo[0]); 
          			},function(){});
        	}
        	
        	function BindGetSessionTypes(){
        		 BusinessLogic.GetMethod('getSessionType').then(function(response){ 
                     EmployeeIndividualController.SessionType = angular.fromJson(response);
                 	},function(reason){ });
        	}
        	
        	EmployeeIndividualController.EmployeeCurrentStatusClick = function(flag,statusID){
  				scope.EmploymentStatus.employmentStatusID = statusID;
  				EmployeeIndividualController.displayval = " "; 
  				angular.forEach(EmployeeIndividualController.EmploymentStatusValues, function(value, key){
                  		 if(value.employmentStatusID == statusID){ 
                  			 if(flag == "change"){ 
                  			value.Checked = !value.Checked; 
                  			scope.EmploymentStatus.employmentStatusID = (value.Checked)? statusID : null; 
                  			if(statusID == 3 || statusID == 4){
                  				this.today = new Date();    	                  				
                  				scope.EmploymentStatus.StatusEndDate = new Date(
                  						this.today.setMonth(this.today.getMonth() + 2));	
                  			}
                  			if(statusID == 2){
                  				this.today = new Date();    	                  				
                  				scope.EmploymentStatus.StatusEndDate = new Date(
                  						this.today.setMonth(this.today.getMonth() + 6));	
                  			}
                  			
                  			scope.EmploymentStatus.StatusEndDate = (value.Checked)? scope.EmploymentStatus.StatusEndDate : null;
                  			EmployeeIndividualController.EndDateDisable = (value.Checked)? false : true;
                  			 }
                  			 else if(flag == "initial"){
                  				 value.Checked = true; 	                  				
                  			 }	                  			  
                  		 }
                  		
                  		 else{ 
                  			value.Checked=false;	                  			
                  		 }
                  		 
                  		 if(value.Checked == true){     	                  			 
	                  		EmployeeIndividualController.displayval = value.employmentStatus + " End Date";
	                  		EmployeeIndividualController.EndDateDisable = false;
	                  	}    	                  		
         	 });    
        	
        	EmployeeIndividualController.saveDetails = function(ev,form,data){           		          		        		
      		 	BusinessLogic.PostMethod('updateEmploymentStatus',data).then(function(response){ 
              		EmployeeIndividualController.MonthToDateUnpaidDetails = response.data;
              		dropDownLeaves(); 
              		notification.notify(response.data.status);
               		}, function(reason){
               			notification.notify(reason.data.status);
               		});          		 
      			}
        	
        	function BindCurrentLeaveBalance(){         		
          		EmployeeIndividualController.EndDateDisable = true;
          		         	 
             	}
        	
            	
           EmployeeIndividualController.EmployeeCurrentStatus = function(selectedValue){ 
            	if(selectedValue != null){ 
            	 angular.forEach(EmployeeIndividualController.EmploymentStatusValues, function(value, key){ 
            	
            	  if(value.employmentStatusID == selectedValue){ 
            		  EmployeeIndividualController.displayval = value.employmentStatus; 
            	  }
            	 })
            	 EmployeeIndividualController.displayval = EmployeeIndividualController.displayval + " End Date";  
            	}
            	
            }
           
    	EmployeeIndividualController.UnpaidLeavesCheck = function(flag){ 
    	if( (flag == "ShowYeartoDate" && EmployeeIndividualController.YearToDateLeaveConsolidation.noOfUnpaidLeaves >0) || (flag == "ShowMonthtoDate" && EmployeeIndividualController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves >0) ){
          	 mdDialog.show({
                  clickOutsideToClose: true,
                  scope: scope,        
                  preserveScope: true,    
    		      fullscreen: 'md' ,       
                  templateUrl: 'UnPaidLeavesCheck.html',
                  controller: function DialogController($scope, $mdDialog) {
                	  
                	BusinessLogic.PostMethod('getUnpaidleavesInfo',rootScope.iD).then(function(response){  
                		EmployeeIndividualController.UnpaidLeaveDetails = response.data;
                		
                		 if(flag == "ShowYeartoDate"){ 
           	                 EmployeeIndividualController.UnpaidLeavesdata = EmployeeIndividualController.UnpaidLeaveDetails.YearToDate;
           	                 EmployeeIndividualController.TotalUnpaidLeavesTaken = EmployeeIndividualController.YearToDateLeaveConsolidation.noOfUnpaidLeaves; 
        	 	               }
        	 	              else if (flag == "ShowMonthtoDate"){ 
        	 	              EmployeeIndividualController.UnpaidLeavesdata = EmployeeIndividualController.UnpaidLeaveDetails.MonthToDate;
        	 	             EmployeeIndividualController.TotalUnpaidLeavesTaken = EmployeeIndividualController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves;
        	 	               }
             			},
             			function(){});
                	
              	
   	                
            	  	$scope.closeView = function() {    			 	                		
                 		mdDialog.hide();  
                 		}
                	}
               });               		
      	 } 
    	}
            	EmployeeIndividualController.PaidLeavesCheck = function(flag){ 
            	 if((flag == "ShowYeartoDate" && EmployeeIndividualController.YearToDateLeaveConsolidation.noOfPaidLeaves > 0) || (flag == "ShowMonthtoDate" && EmployeeIndividualController.MonthToDateLeaveConsolidation.noOfPaidLeaves > 0) ){
              		 mdDialog.show({
        	                  clickOutsideToClose: true,
        	                  scope: scope,        
        	                  preserveScope: true,    
        	    		      fullscreen: 'md' ,       
        	                  templateUrl: 'PaidLeavesCheck.html',
        	                  controller: function DialogController($scope, $mdDialog) { 
        	                  
	                	  BusinessLogic.PostMethod('getpaidleavesInfo',rootScope.iD).then(function(response){  
	          		      	EmployeeIndividualController.paidLeaveDetails = response.data;
	          		      if(flag == "ShowYeartoDate"){ 
       		 	           EmployeeIndividualController.PaidLeavesdata = EmployeeIndividualController.paidLeaveDetails.YearToDate;
       		 	           EmployeeIndividualController.TotalPaidLeavesTaken = EmployeeIndividualController.YearToDateLeaveConsolidation.noOfPaidLeaves;
       		 	               }
       		 	              else if (flag == "ShowMonthtoDate"){  
       		 	           EmployeeIndividualController.PaidLeavesdata = EmployeeIndividualController.paidLeaveDetails.MonthToDate; 
       		 	          EmployeeIndividualController.TotalPaidLeavesTaken = EmployeeIndividualController.MonthToDateLeaveConsolidation.noOfPaidLeaves;
       		 	               }
	          		       	},
	          		       	function(){});
	                	  
        		 	            
		 	          $scope.closeView = function() {    			 	                		
		 	            mdDialog.hide();  
		 	          }
        	        }
        	      });                		
               	}
            }
            	
            	EmployeeIndividualController.EmployeeCompoffDetails = function(value){ 
            		
            		mdDialog.show({
	                  clickOutsideToClose: true, 
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'CompOffCheck.html',
	                  controller: function DialogController($scope, $mdDialog) {
	                	  
	                   BusinessLogic.PostMethod('individualEmployeeCompOffCreditList',rootScope.iD).then(function(response){ 
            			EmployeeIndividualController.compOffDetails = response.data; 
             			},
             			function(){});
	                	  
	                	  EmployeeIndividualController.totalCompOffs = value;
	                	  
		 	              $scope.closeView = function() {       			 	                		
			 	            mdDialog.hide();  
		 	              }  		 	                	  	
	                	}
	               });
        		
       	 }
        		EmployeeIndividualController.changeYearOrMonthToDate = function() { 
                	
            		if(scope.flag == "ShowYeartoDate"){
            			scope.flag= "ShowMonthtoDate";
            			BindCurrentLeaveBalance();
            		}
            		else if (scope.flag == "ShowMonthtoDate") {
            			scope.flag= "ShowYeartoDate";
            			BindCurrentLeaveBalance();
            		}
            	}
        	}
        	
        	
        	 
        	EmployeeIndividualController.ApplyForLeave = function(){
        		BindGetSessionTypes()
        		dropDownLeaves()
           	 mdDialog.show({
	                  clickOutsideToClose: false,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'ApplyForLeave.html',
	                  controller: function DialogController($scope, $mdDialog) { 
	                	 EmployeeIndividualController.SelectedEmails = []; 
               		  EmployeeIndividualController.searchText = null; 
             			  scope.ApplyLeave={}; 
             			                			  
	                	  $scope.closeView = function() {
	              			  mdDialog.hide();  	
	              			}
	                	 $scope.remove=function(item){
	                		 EmployeeIndividualController.SelectedEmails.splice( item, 1);
	                         }
	                	scope.ApplyLeave = { sessionOfEndDate : "2", sessionOfStartDate : "2" } 
	                	loadDefaultEmails(EmployeeIndividualController.SelectedEmployeeInfo.empID); 	                	
	                	}
	               });
       	};
       	
            EmployeeIndividualController.ApplyForWorkFromHome = function(){
            	BindGetSessionTypes()
           	 mdDialog.show({
	                  clickOutsideToClose: true,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'ApplyForWorkFromHome.html',
	                  controller: function DialogController($scope, $mdDialog) {
	                	 EmployeeIndividualController.SelectedEmails = [];
	                 	 EmployeeIndividualController.searchText = null;
	                 	 scope.WorkFromHome={};
	                 		
	                	  $scope.closeView = function() {		                 		 
		                	  mdDialog.hide(); 
	                		}
	                	 $scope.remove=function(item){
	                		 EmployeeIndividualController.SelectedEmails.splice( item, 1);
	                         }
	                	scope.WorkFromHome = { sessionOfEndDate : "2", sessionOfStartDate : "2" } 
	                	loadDefaultEmails(EmployeeIndividualController.SelectedEmployeeInfo.empID); 
	                	 	
	                	}
	               });
           	};
            
            EmployeeIndividualController.ApplyForCompOff = function(){
            	BindGetSessionTypes()
           	 mdDialog.show({
	                  clickOutsideToClose: true,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'ApplyForCompOff.html',
	                  controller: function DialogController($scope, $mdDialog) {
	                	 EmployeeIndividualController.SelectedEmails = [];
	                 	 EmployeeIndividualController.searchText = null; 
	                	 scope.CompOff={};
	                		 
	                	  $scope.closeView = function() {
	                		  mdDialog.hide(); 
	                		 }
	                	 $scope.remove=function(item){
	                		 EmployeeIndividualController.SelectedEmails.splice( item, 1);
	                         }
	                	scope.CompOff =  { sessionOfEndDate : "2", sessionOfStartDate : "2" }
	                	loadDefaultEmails(EmployeeIndividualController.SelectedEmployeeInfo.empID);	
	                	 
	                	}
	               });
           	};
           	
           	
            function dropDownLeaves(){
	   		     BusinessLogic.PostMethod('getDropdownLeaves',rootScope.iD).then(function(response){
	   		    	 EmployeeIndividualController.LeaveTypes = angular.fromJson(response.data);
	   		     },function(){});
            }
            
           	loadEmployeeEmails = function(ed) { 
 			    var EmployeeList=[]; 
 		        BusinessLogic.PostMethod('getEmailRecipientsList', rootScope.iD).then(function(response){
 					scope.EmployeeList = angular.fromJson(response.data);  
 	               	scope.Employees = scope.EmployeeList.map(function(emp) { emp.lowername = emp.employeeName.toLowerCase(); return emp; });
        		},function(){});
			} 
         
          loadDefaultEmails = function(ed) {
        	 BusinessLogic.PostMethod('getDefaultEmailRecipients',ed).then(function(response){
					scope.EmployeeDefaultRec = angular.fromJson(response.data);
             	loadEmployeeEmails(ed);     	               	
     		},function(){});
         }
         
     	EmployeeIndividualController.selectedItemChange = function(as){ 
    		if(as!==null){
    			var isExists=false;
    			if(EmployeeIndividualController.SelectedEmails.length == 0){ 
    				EmployeeIndividualController.SelectedEmails.push(angular.copy(as)); 
    			}
    			
    			angular.forEach(EmployeeIndividualController.SelectedEmails,function(data){
    					if(data.employeeID == as.employeeID){ isExists=true;} 
        		})
        		if(!isExists){
        			EmployeeIndividualController.SelectedEmails.push(angular.copy(as)); 
        		}
    			EmployeeIndividualController.selectedItem = null; 
    			EmployeeIndividualController.searchText = null; 
    		}
    	}
         
         EmployeeIndividualController.querySearch =function(query) {	
        	return query ? scope.Employees.filter( createFilterFor(query) ) : scope.Employees; 
        }		
    	

			EmployeeIndividualController.selectedVegetables=[];
			EmployeeIndividualController.autocompleteDemoRequireMatch=false;
			
		
			
		  function createFilterFor(query) {
				 var as= EmployeeIndividualController.selectedItem; 
		      var lowercaseQuery = angular.lowercase(query);			       			      	
			      return function filterFn(emp) {  			    	 
			    		return (emp.employeeName.toLowerCase().indexOf(lowercaseQuery) === 0); 		    	  
			      	};
			    }
			    
			     
           	
        	EmployeeIndividualController.ApplyLeave = function(ev,form,data){ 
            	if(form.$invalid){ 
        			angular.forEach(form.$error, function (field) {
                        angular.forEach(field, function(errorField){
                          errorField.$setTouched();
                        })
                      });
        		}
            	else { 
            		if(!data.leaveFromDayHalf){  data.sessionOfStartDate = "1";}
            		if(!data.leaveToDayHalf){ data.sessionOfEndDate = "1";}
            		data.isApproveBySLT =0;
            		data.DefaultEmails =scope.EmployeeDefaultRec;
            		angular.forEach(EmployeeIndividualController.SelectedEmails,function(value,key){
            			data.DefaultEmails.push(angular.copy(value)); 
            		})
            		data.SelectedEmails = EmployeeIndividualController.SelectedEmails;
            		data.employeeID=EmployeeIndividualController.SelectedEmployeeInfo.empID;
            	
            		BusinessLogic.PostMethod('saveLeaveDetails', data).then(function(response){
                    	scope.ApplyLeave={};                 	
                    	EmployeeIndividualController.SelectedEmails = [];                	
                    	mdDialog.hide(); 
                    	
                    	
                    	EmployeeIndividualController.searchText = null; 
                    	notification.notify(response.data.status);
                    	 					
            		},function(reason){
            			if(reason.data.isApproveBySLT == 1){
              				mdDialog.show({
            					clickOutsideToClose: false,
            					skipHide: true,
            					template: '<md-dialog flex="40" flex-md="60" flex-xs="100" flex-sm="100" aria-label="confirmation" md-theme="meritus">' +
            					'  <md-dialog-content class="md-dialog-content"><h2 class="md-title" style="text-align:left; padding: 10px"><p ng-bind="Msg" style="margin: 0"></p></h2></md-dialog-content>' +
            					'  <md-dialog-actions layout="row" layout-align="end center">' +
            					'    <md-button ng-click="yesDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">Yes</md-button>' +
            					'    <md-button ng-click="closeDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">No</md-button>' +
            					'  </md-dialog-actions>' +
            					'</md-dialog>',
            					controller: function DialogController($mdDialog, $scope) {
            						$scope.Msg = reason.data.status; 
            						$scope.closeDialog = function () { $mdDialog.hide(); }
            						$scope.yesDialog = function () { 
            							reason.data.data.isApproveBySLT=1;
                  						BusinessLogic.PostMethod('saveLeaveDetails',reason.data.data).then(function(response){
                  							mdDialog.hide(); 
                  							/*BindTeamAndMe();*/
                  		                	notification.notify(response.data.status);
                  		                	mdDialog.hide();
          	            				},function(reason){});
            						}
            					}
            				});	
               			}
               			else{
               				notification.notify(reason.data.status);
               			}
            		});
            	}
            	}
            	
            	
            	EmployeeIndividualController.ApplyWorkFromHome = function(ev,form,data){
            		if(form.$invalid){ 
            			angular.forEach(form.$error, function (field) {
                            angular.forEach(field, function(errorField){
                              errorField.$setTouched();
                            })
                          });
            		}
            		else {
            		if(!data.leaveFromDayHalf){  data.sessionOfStartDate = "1";}
            		if(!data.leaveToDayHalf){ data.sessionOfEndDate = "1";}
            		data.isApproveBySLT = 0;
            		data.DefaultEmails =scope.EmployeeDefaultRec;
            		angular.forEach(EmployeeIndividualController.SelectedEmails,function(value,key){
            			data.DefaultEmails.push(value); 
            		})
            		data.employeeID=EmployeeIndividualController.SelectedEmployeeInfo.empID; 
                           
                	BusinessLogic.PostMethod('saveWfhDetails', data).then(function(response){
                		
                		 mdDialog.hide();
                    	 scope.WorkFromHome={};                 	
                    	 EmployeeIndividualController.SelectedEmails = [];                	 
                    	 EmployeeIndividualController.searchText = null; 
                    	 notification.notify(response.data.status ); 
                		 
            			},
            			function(reason){
            				if(reason.data.isApproveBySLT == 1){
                    			mdDialog.show({
                					clickOutsideToClose: false,
                					skipHide: true,
                					template: '<md-dialog flex="40" flex-md="60" flex-xs="100" flex-sm="100" aria-label="confirmation" md-theme="meritus">' +
                					'  <md-dialog-content class="md-dialog-content"><h2 class="md-title" style="text-align:left; padding: 10px"><p ng-bind="Msg" style="margin: 0"></p></h2></md-dialog-content>' +
                					'  <md-dialog-actions layout="row" layout-align="end center">' +
                					'    <md-button ng-click="yesDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">Yes</md-button>' +
                					'    <md-button ng-click="closeDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">No</md-button>' +
                					'  </md-dialog-actions>' +
                					'</md-dialog>',
                					controller: function DialogController($mdDialog, $scope) {
                						$scope.Msg = reason.data.status;
                						$scope.closeDialog = function () { $mdDialog.hide(); }
                						$scope.yesDialog = function () { 
                							reason.data.data.isApproveBySLT=1;
                      						BusinessLogic.PostMethod('saveWfhDetails',reason.data.data).then(function(response){
                      							mdDialog.hide();
                      							/*BindTeamAndMe();*/
                      		                	notification.notify(response.data.status);
                      		                	mdDialog.hide();
              	            				},function(reason){});
                						}
                					}
                				});	
                    			}
                    		else{
                    			notification.notify(reason.data.status);
                    		}
         			});
            	  }
            	}
            	
            	EmployeeIndividualController.ApplyCompOff = function(ev,form,data){
            		if(form.$invalid){ 
            			angular.forEach(form.$error, function (field) {
                            angular.forEach(field, function(errorField){
                              errorField.$setTouched();
                            })
                          });
            		}
            		else{
            		if(!data.CompOffFromDayHalf){  data.sessionOfStartDate = "1";}
            		if(!data.CompOffToDayHalf){ data.sessionOfEndDate = "1";}
            		
            		data.DefaultEmails =scope.EmployeeDefaultRec;
            		angular.forEach(EmployeeIndividualController.SelectedEmails,function(value,key){
            			data.DefaultEmails.push(value); 
            		})
            		data.employeeID=EmployeeIndividualController.SelectedEmployeeInfo.empID; 
            	
                    BusinessLogic.PostMethod('saveCompOffDetails', data).then(function(response){
                    	
                    		mdDialog.hide();
                    		scope.CompOff={};                 	
                    		EmployeeIndividualController.SelectedEmails = [];                	 
                    		EmployeeIndividualController.searchText = null; 
                    		notification.notify(response.data.status);
                    	  
            			},
            			function(reason){
            				notification.notify(reason.data.status);
         			});
            	  }
            	}
        	
        	
        }],
        controllerAs: 'EmployeeIndividualController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Admin/individualEmployeeDetails.html'
		}
})

.directive('individualEmployeeDetails',function(){ 
	return{
		restrict : "E",
        controller: ['$window', '$state', '$q', '$mdDialog','$scope','$rootScope','BusinessLogic','BusinessLogic1','notification','Menu', function(window, state, Q, mdDialog,scope,rootScope,BusinessLogic,BusinessLogic1,notification,Menu){
        	var selectedEmployeeController = this;
        	rootScope.hideMainCalendar = false;        	
        	scope.flag= "ShowYeartoDate";
        	rootScope.selectedModule="Employee";
        	rootScope.menu_data=Menu.GetData(rootScope.selectedModule);
        	rootScope.selectedView='Dashboard.Employee.EmployeeHome';
        	selectedEmployeeController.minDate=new Date();
        	this.myDate = new Date(); 
        	if(rootScope.iD == undefined){
        		state.go('Dashboard.Employee.SiloView');
        	}
        	//selectedEmployeeController.compOffMinDate  = new Date(this.myDate.getFullYear()-1,03,01); 
                
        	/*selectedEmployeeController.maxDate = new Date(
            		    this.myDate.getFullYear() + 1,
            		    02,
            		    31
            		  ); */
        	
        	rootScope.$emit("BindCalender", {});
        	
        	Bind();
        	function Bind(){
        		BindCurrentLeaveBalance();
            	function BindCurrentLeaveBalance(){         		
              BusinessLogic.PostMethod('getEmployeeInfo', rootScope.iD).then(function(response){  
      			selectedEmployeeController.SelectedEmployeeCurrentLeaveBalance=angular.fromJson(response.data.currentleaveBalance);  
      			selectedEmployeeController.SelectedEmployeeInfo = angular.fromJson(response.data.employyeInfo[0]); 
      			BindNoOfLeaves(); 
      			
        		},function(){});
      		 
      			BusinessLogic.PostMethod('getStatusPeriods',rootScope.iD).then(function(response){ 
              		selectedEmployeeController.EmploymentStatusValues = response.data;
              		/*BusinessLogic.PostMethod('getEmployeeStatusDisplay',rootScope.iD).then(function(response){  
              			scope.EmploymentStatus = response.data; 
              			if(response.data.StatusEndDate != null){
              				scope.EmploymentStatus.StatusEndDate=new Date(response.data.StatusEndDate);
              			}              			
              		selectedEmployeeController.EmployeeCurrentStatusClick("initial",response.data.employmentStatusID);            			
              	   	}, function(){});*/
              		
               	}, function(){});
          		
      			selectedEmployeeController.EndDateDisable = true;
      			selectedEmployeeController.EmployeeCurrentStatusClick = function(flag,statusID){
      				scope.EmploymentStatus.employmentStatusID = statusID;
      				selectedEmployeeController.displayval = " ";      				
             	 angular.forEach(selectedEmployeeController.EmploymentStatusValues, function(value, key){
	                  		 if(value.employmentStatusID == statusID){
	                  			 if(flag == "change"){
	                  			value.Checked = !value.Checked;
	                  			scope.EmploymentStatus.employmentStatusID = (value.Checked)? statusID : null;	                  			 
	                  			/*scope.EmploymentStatus.StatusEndDate = null;*/
	                  			if(statusID == 3 || statusID == 4){
	                  				this.today = new Date();    	                  				
	                  				scope.EmploymentStatus.StatusEndDate = new Date(
	                  						this.today.setMonth(this.today.getMonth() + 2));	
	                  			}
	                  			if(statusID == 2){
	                  				this.today = new Date();    	                  				
	                  				scope.EmploymentStatus.StatusEndDate = new Date(
	                  						this.today.setMonth(this.today.getMonth() + 6));	
	                  			}
	                  			scope.EmploymentStatus.StatusEndDate = (value.Checked)? scope.EmploymentStatus.StatusEndDate : null;
	                  			selectedEmployeeController.EndDateDisable = (value.Checked)? false : true;
	                  			 }
	                  			 else if(flag == "initial"){
	                  				 value.Checked = true; 	                 				
	                  			 }	                  			  
	                  		 }
	                  		
	                  		 else{
	                  			value.Checked=false;	                  			
	                  		 }
	                  		 
	                  		 if(value.Checked == true){
	                  		selectedEmployeeController.displayval = value.employmentStatus + " End Date";
	                  		selectedEmployeeController.EndDateDisable = false;
		                  	}
             	 });             	 
             	}
      			
          		selectedEmployeeController.saveDetails = function(ev,form,data){
          			
	          		BusinessLogic.PostMethod('updateEmploymentStatus',data).then(function(response){ 
	              		selectedEmployeeController.MonthToDateUnpaidDetails = response.data;
	              		dropDownLeaves();
	              		notification.notify(response.data.status);
	               	}, function(reason){
	               		notification.notify(reason.data.status);
	               	});
          		 
          		}
           	  }
            	
            	function BindNoOfLeaves(){
            		BusinessLogic.PostMethod('yearAndMonthToDateStatus',rootScope.iD).then(function(response){ 
            			selectedEmployeeController.YearToDateLeaveConsolidation = response.data.yearToDateStatus[0];
            			selectedEmployeeController.MonthToDateLeaveConsolidation = response.data.monthToDateStatus[0];
            			selectedEmployeeController.compOffMinDate=new Date(angular.copy(selectedEmployeeController.YearToDateLeaveConsolidation.compoffDate));
            			selectedEmployeeController.compOffMaxDate=new Date(angular.copy(selectedEmployeeController.YearToDateLeaveConsolidation.compOffMax));
            			checking();
            			},function(){});
            	}
            	
            	function checking(){
            		selectedEmployeeController.value = function(flag, check){  
                    	
                      	 if(flag == "ShowYeartoDate" && check == "paidLeaves"){
                      		 if(selectedEmployeeController.YearToDateLeaveConsolidation.noOfPaidLeaves > 0){
                      			 return true;
                      		 }
                      	 }
                      	 if(flag == "ShowYeartoDate" && check == "UnpaidLeaves"){
                      		 if(selectedEmployeeController.YearToDateLeaveConsolidation.noOfUnpaidLeaves >0){
                      			 return true;
                      		 }
                      	 }
                      	 if(flag == "ShowMonthtoDate" && check == "paidLeaves"){
                      		 if(selectedEmployeeController.MonthToDateLeaveConsolidation.noOfPaidLeaves > 0){
                      			 return true;
                      		 }
                      	 }
                      	 if(flag == "ShowMonthtoDate" && check == "UnpaidLeaves"){
                      		 if(selectedEmployeeController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves >0){
                      			 return true;
                      		 }
                      	 }
                      }
            	}
            	
            	selectedEmployeeController.UnpaidLeavesCheck = function(flag){ 
            		if( (flag == "ShowYeartoDate" && selectedEmployeeController.YearToDateLeaveConsolidation.noOfUnpaidLeaves >0) || (flag == "ShowMonthtoDate" && selectedEmployeeController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves >0) ){            		
                  	 mdDialog.show({
       	                  clickOutsideToClose: true,
       	                  scope: scope,        
       	                  preserveScope: true,    
       	    		      fullscreen: 'md' ,       
       	                  templateUrl: 'UnPaidLeavesCheck.html',
       	                  controller: function DialogController($scope, $mdDialog) {
       	                  BusinessLogic.PostMethod('getUnpaidleavesInfo',rootScope.iD).then(function(response){  
     	    		      	selectedEmployeeController.UnpaidLeaveDetails = response.data;  
     	    		       if(flag == "ShowYeartoDate"){
     	    		    	  selectedEmployeeController.TotalUnpaidLeavesTaken = selectedEmployeeController.YearToDateLeaveConsolidation.noOfUnpaidLeaves;
  	       	                 selectedEmployeeController.UnpaidLeavesdata = selectedEmployeeController.UnpaidLeaveDetails.YearToDate;
  			 	               }
  			 	              else if (flag == "ShowMonthtoDate"){
  			 	              selectedEmployeeController.TotalUnpaidLeavesTaken = selectedEmployeeController.MonthToDateLeaveConsolidation.noOfUnpaidLeaves;
  			 	              selectedEmployeeController.UnpaidLeavesdata = selectedEmployeeController.UnpaidLeaveDetails.MonthToDate;
  			 	               }
     	    		       	},
     	    		       	function(){});
       	                	
	       	                
	                	  	$scope.closeView = function() {    			 	                		
	 	                 		mdDialog.hide();  
	 	                 		}
       	                	}
       	               });               		
              	 }
            	}
            	selectedEmployeeController.PaidLeavesCheck = function(flag){
            		if((flag == "ShowYeartoDate" && selectedEmployeeController.YearToDateLeaveConsolidation.noOfPaidLeaves > 0) || (flag == "ShowMonthtoDate" && selectedEmployeeController.MonthToDateLeaveConsolidation.noOfPaidLeaves > 0) ){
              		 mdDialog.show({
        	                  clickOutsideToClose: true,
        	                  scope: scope,        
        	                  preserveScope: true,    
        	    		      fullscreen: 'md' ,       
        	                  templateUrl: 'PaidLeavesCheck.html',
        	                  controller: function DialogController($scope, $mdDialog) { 
        	                BusinessLogic.PostMethod('getpaidleavesInfo',rootScope.iD).then(function(response){  
        	    		      	selectedEmployeeController.paidLeaveDetails = response.data;
        	    		        if(flag == "ShowYeartoDate"){ 
        	    		        	selectedEmployeeController.TotalPaidLeavesTaken =  selectedEmployeeController.YearToDateLeaveConsolidation.noOfPaidLeaves;
             		 	           selectedEmployeeController.PaidLeavesdata = selectedEmployeeController.paidLeaveDetails.YearToDate; 
             		 	               }
             		 	              else if (flag == "ShowMonthtoDate"){ 
             		 	            	selectedEmployeeController.TotalPaidLeavesTaken = selectedEmployeeController.MonthToDateLeaveConsolidation.noOfPaidLeaves;
             		 	           selectedEmployeeController.PaidLeavesdata = selectedEmployeeController.paidLeaveDetails.MonthToDate; 
             		 	               }
        	    		       	},
        	    		       	function(){});
        	                
        	                
        		 	            
        		 	              $scope.closeView = function() {    			 	                		
     			 	            mdDialog.hide();  
     			 	          }
        	                	}
        	               });
                		
               	 }
            	}
            	
            	selectedEmployeeController.EmployeeCompoffDetails = function(value){ 
        		
           	 mdDialog.show({
	                  clickOutsideToClose: true, 
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'CompOffCheck.html',
	                  controller: function DialogController($scope, $mdDialog) {
	                	  selectedEmployeeController.totalCompOffs = value;
	                	  
	                	  BusinessLogic.PostMethod('individualEmployeeCompOffCreditList',rootScope.iD).then(function(response){ 
	                			selectedEmployeeController.compOffDetails = response.data; 
	                 			},
	                 			function(){});
	                	  
		 	              $scope.closeView = function() {       			 	                		
			 	            mdDialog.hide();  
		 	              }  		 	                	  	
	                	}
	               });
        		
       	 }
        		selectedEmployeeController.changeYearOrMonthToDate = function() { 
                	
            		if(scope.flag == "ShowYeartoDate"){
            			scope.flag= "ShowMonthtoDate";
            			 BindCurrentLeaveBalance();
            		}
            		else if (scope.flag == "ShowMonthtoDate") {
            			scope.flag= "ShowYeartoDate";
            			BindCurrentLeaveBalance();
            		}
            	}
        	}
        	
        	 BusinessLogic.GetMethod('getSessionType').then(function(response){ 
                 selectedEmployeeController.SessionType = angular.fromJson(response);
             	},function(reason){ });
        	 
        	selectedEmployeeController.ApplyForLeave = function(){
        		
           	 mdDialog.show({
	                  clickOutsideToClose: false,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'ApplyForLeave.html',
	                  controller: function DialogController($scope, $mdDialog) { 
	                	 selectedEmployeeController.SelectedEmails = []; 
               		  selectedEmployeeController.searchText = null; 
             			  scope.ApplyLeave={}; 
             			                			  
	                	  $scope.closeView = function() {
	              			  mdDialog.hide();  	
	                		 }
	                	 $scope.remove=function(item){
	                		 selectedEmployeeController.SelectedEmails.splice( item, 1);
	                         }
	                	scope.ApplyLeave = { sessionOfEndDate : "2", sessionOfStartDate : "2" } 
	                	loadDefaultEmails(selectedEmployeeController.SelectedEmployeeInfo.empID); 	                	
	                	}
	               });
       	};
       	
            selectedEmployeeController.ApplyForWorkFromHome = function(){
           	 mdDialog.show({
	                  clickOutsideToClose: true,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'ApplyForWorkFromHome.html',
	                  controller: function DialogController($scope, $mdDialog) {
	                	 selectedEmployeeController.SelectedEmails = [];
	                 	 selectedEmployeeController.searchText = null;
	                 	 scope.WorkFromHome={};
	                 		
	                	  $scope.closeView = function() { 		                 		 
		                	  mdDialog.hide(); 
	                		}
	                	 $scope.remove=function(item){
	                		 selectedEmployeeController.SelectedEmails.splice( item, 1);
	                         }
	                	scope.WorkFromHome = { sessionOfEndDate : "2", sessionOfStartDate : "2" } 
	                	loadDefaultEmails(selectedEmployeeController.SelectedEmployeeInfo.empID); 
	                	 	
	                	}
	               });
           	};
            
            selectedEmployeeController.ApplyForCompOff = function(){
           	 mdDialog.show({
	                  clickOutsideToClose: true,
	                  scope: scope,        
	                  preserveScope: true,    
	    		      fullscreen: 'md' ,       
	                  templateUrl: 'ApplyForCompOff.html',
	                  controller: function DialogController($scope, $mdDialog) {
	                	 selectedEmployeeController.SelectedEmails = [];
	                 	 selectedEmployeeController.searchText = null; 
	                	 scope.CompOff={};
	                		 
	                	  $scope.closeView = function() { 
	                		  mdDialog.hide();
	                		 }
	                	 $scope.remove=function(item){
	                		 selectedEmployeeController.SelectedEmails.splice( item, 1);
	                         }
	                	scope.CompOff =  { sessionOfEndDate : "2", sessionOfStartDate : "2" }
	                	loadDefaultEmails(selectedEmployeeController.SelectedEmployeeInfo.empID);	
	                	 
	                	}
	               });
           	};
           	
            function dropDownLeaves(){
   		     BusinessLogic.PostMethod('getDropdownLeaves',rootScope.iD).then(function(response){
   		    	 selectedEmployeeController.LeaveTypes = angular.fromJson(response.data);
   		     },function(){});         
            }
            
           	loadEmployeeEmails = function(ed) { 
 			      
 				var EmployeeList=[]; 
 		  
                BusinessLogic.PostMethod('getEmailRecipientsList', rootScope.iD).then(function(response){
 					scope.EmployeeList = angular.fromJson(response.data);  
 	               	scope.Employees = scope.EmployeeList.map(function(emp) { emp.lowername = emp.employeeName.toLowerCase(); return emp; });
        		},function(){});
				
 			} 
           	dropDownLeaves();
         loadDefaultEmails = function(ed) {
        	 BusinessLogic.PostMethod('getDefaultEmailRecipients',ed).then(function(response){
					scope.EmployeeDefaultRec = angular.fromJson(response.data);
             	loadEmployeeEmails(ed);     	               	
     		},function(){});
         }
         
     	selectedEmployeeController.selectedItemChange = function(as){ 
    		if(as!==null){
    			var isExists=false;
    			if(selectedEmployeeController.SelectedEmails.length == 0){ 
    				selectedEmployeeController.SelectedEmails.push(angular.copy(as)); 
    			}
    			
    			angular.forEach(selectedEmployeeController.SelectedEmails,function(data){
    					if(data.employeeID == as.employeeID){ isExists=true;} 
        		})
        		if(!isExists){
        			selectedEmployeeController.SelectedEmails.push(angular.copy(as)); 
        		}
    			selectedEmployeeController.selectedItem = null; 
    			selectedEmployeeController.searchText = null; 
    		}
    	}
         
         selectedEmployeeController.querySearch =function(query) {	
        	return query ? scope.Employees.filter( createFilterFor(query) ) : scope.Employees; 
        }		
    	

			selectedEmployeeController.selectedVegetables=[];
			selectedEmployeeController.autocompleteDemoRequireMatch=false;
			
		
			
		  function createFilterFor(query) {
				 var as= selectedEmployeeController.selectedItem; 
		      var lowercaseQuery = angular.lowercase(query);			       			      	
			      return function filterFn(emp) {  			    	 
			    		return (emp.employeeName.toLowerCase().indexOf(lowercaseQuery) === 0); 		    	  
			      	};
			    }
			    
			     
           	
        	selectedEmployeeController.ApplyLeave = function(ev,form,data){ 
            	if(form.$invalid){ 
        			angular.forEach(form.$error, function (field) {
                        angular.forEach(field, function(errorField){
                          errorField.$setTouched();
                        })
                      });
        		}
            	else { 
            		if(!data.leaveFromDayHalf){  data.sessionOfStartDate = "1";}
            		if(!data.leaveToDayHalf){ data.sessionOfEndDate = "1";}
            		
            		data.DefaultEmails =scope.EmployeeDefaultRec;
            		angular.forEach(selectedEmployeeController.SelectedEmails,function(value,key){
            			data.DefaultEmails.push(angular.copy(value)); 
            		})
            		data.SelectedEmails = selectedEmployeeController.SelectedEmails;
            		data.employeeID=selectedEmployeeController.SelectedEmployeeInfo.empID;
        			data.isApproveBySLT = 0;
            		BusinessLogic.PostMethod('saveLeaveDetails', data).then(function(response){
                    	scope.ApplyLeave={};                 	
                    	selectedEmployeeController.SelectedEmails = [];                	
                    	mdDialog.hide(); 
                    	
                    	
                    	selectedEmployeeController.searchText = null; 
                    	notification.notify(response.data.status);
                    	 					
            		},function(reason){ 
            			if(reason.data.isApproveBySLT == 1){
              				mdDialog.show({
            					clickOutsideToClose: false,
            					skipHide: true,
            					template: '<md-dialog flex="40" flex-md="60" flex-xs="100" flex-sm="100" aria-label="confirmation" md-theme="meritus">' +
            					'  <md-dialog-content class="md-dialog-content"><h2 class="md-title" style="text-align:left; padding: 10px"><p ng-bind="Msg" style="margin: 0"></p></h2></md-dialog-content>' +
            					'  <md-dialog-actions layout="row" layout-align="end center">' +
            					'    <md-button ng-click="yesDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">Yes</md-button>' +
            					'    <md-button ng-click="closeDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">No</md-button>' +
            					'  </md-dialog-actions>' +
            					'</md-dialog>',
            					controller: function DialogController($mdDialog, $scope) {
            						$scope.Msg = reason.data.status;
            						$scope.closeDialog = function () { $mdDialog.hide(); }
            						$scope.yesDialog = function () { 
            							reason.data.data.isApproveBySLT=1;
                  						BusinessLogic.PostMethod('saveLeaveDetails',reason.data.data).then(function(response){
                  							mdDialog.hide();
                  							/*BindTeamAndMe();*/
                  		                	notification.notify(response.data.status);
                  		                	mdDialog.hide();
          	            				},function(reason){});
            						}
            					}
            				});	
               			}
               			else{
               				notification.notify(reason.data.status);
               			}
         			});
            	}
            	}
            	
            	
            	selectedEmployeeController.ApplyWorkFromHome = function(ev,form,data){
            		if(form.$invalid){ 
            			angular.forEach(form.$error, function (field) {
                            angular.forEach(field, function(errorField){
                              errorField.$setTouched();
                            })
                          });
            		}
            		else {
            		if(!data.leaveFromDayHalf){  data.sessionOfStartDate = "1";}
            		if(!data.leaveToDayHalf){ data.sessionOfEndDate = "1";}
            		
            		data.DefaultEmails =scope.EmployeeDefaultRec;
            		angular.forEach(selectedEmployeeController.SelectedEmails,function(value,key){
            			data.DefaultEmails.push(value); 
            		})
            		data.employeeID=selectedEmployeeController.SelectedEmployeeInfo.empID; 
            		data.isApproveBySLT = 0;       
                	BusinessLogic.PostMethod('saveWfhDetails', data).then(function(response){
                		
                		 mdDialog.hide();
                    	 scope.WorkFromHome={};                 	
                    	 selectedEmployeeController.SelectedEmails = [];                	 
                    	 selectedEmployeeController.searchText = null; 
                    	 notification.notify(response.data.status ); 
                		 
            			},
            			function(reason){
            				if(reason.data.isApproveBySLT == 1){
                    			
                    			mdDialog.show({
                					clickOutsideToClose: false,
                					skipHide: true,
                					template: '<md-dialog flex="40" flex-md="60" flex-xs="100" flex-sm="100" aria-label="confirmation" md-theme="meritus">' +
                					'  <md-dialog-content class="md-dialog-content"><h2 class="md-title" style="text-align:left; padding: 10px"><p ng-bind="Msg" style="margin: 0"></p></h2></md-dialog-content>' +
                					'  <md-dialog-actions layout="row" layout-align="end center">' +
                					'    <md-button ng-click="yesDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">Yes</md-button>' +
                					'    <md-button ng-click="closeDialog()" type="button" class="meritus-btn md-raised" style="padding: 0 !important;">No</md-button>' +
                					'  </md-dialog-actions>' +
                					'</md-dialog>',
                					controller: function DialogController($mdDialog, $scope) {
                						$scope.Msg = reason.data.status;
                						$scope.closeDialog = function () { $mdDialog.hide(); }
                						$scope.yesDialog = function () { 
                							reason.data.data.isApproveBySLT=1;
                      						BusinessLogic.PostMethod('saveWfhDetails',reason.data.data).then(function(response){
                      							mdDialog.hide();
                      							/*BindTeamAndMe();*/
                      		                	notification.notify(response.data.status);
                      		                	mdDialog.hide();
              	            				},function(reason){});
                						}
                					}
                				});	
                    			}
                    		else{
                    			notification.notify(reason.data.status);
                    		}
         			});
            	  }
            	}
            	
            	selectedEmployeeController.ApplyCompOff = function(ev,form,data){
            		if(form.$invalid){ 
            			angular.forEach(form.$error, function (field) {
                            angular.forEach(field, function(errorField){
                              errorField.$setTouched();
                            })
                          });
            		}
            		else{
            		if(!data.CompOffFromDayHalf){  data.sessionOfStartDate = "1";}
            		if(!data.CompOffToDayHalf){ data.sessionOfEndDate = "1";}
            		
            		data.DefaultEmails =scope.EmployeeDefaultRec;
            		angular.forEach(selectedEmployeeController.SelectedEmails,function(value,key){
            			data.DefaultEmails.push(value); 
            		})
            		data.employeeID=selectedEmployeeController.SelectedEmployeeInfo.empID; 
            	
                    BusinessLogic.PostMethod('saveCompOffDetails', data).then(function(response){
                    	
                    		mdDialog.hide();
                    		scope.CompOff={};                 	
                    		selectedEmployeeController.SelectedEmails = [];                	 
                    		selectedEmployeeController.searchText = null; 
                    		notification.notify(response.data.status);
                    	  
            			},
            			function(reason){
            				notification.notify(reason.data.status);
         			});
            	  }
            	}
            	
        }],
        controllerAs: 'selectedEmployeeController',
        link: function(scope, element, attrs, controllers) { },
        templateUrl : 'Partials/Employee/selectedEmployeeDetails.html'
		}
});

