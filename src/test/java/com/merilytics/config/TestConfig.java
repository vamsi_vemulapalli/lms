package com.merilytics.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.merilytics.service.AdminSettingsService;
import com.merilytics.service.AdminSettingsServiceImpl;

@Configuration

	@ComponentScan(basePackages = {

			"com.merilytics.dao" ,"com.merilytics.service","com.merilytics.bo","com.merilytics.dto"
	})

	 

	public class TestConfig {

	 

	    @Bean

	    public AdminSettingsService getMyService() {

	        return new AdminSettingsServiceImpl();

	    }

	 

	}