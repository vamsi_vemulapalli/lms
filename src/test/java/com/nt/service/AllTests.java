package com.nt.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
public class AllTests {
	private IntrAmtCalculatorService service;
	
	@Before
	public void setUp(){
		service=new IntrAmtCalculatorService();
	}
	
	@Test
	//@Ignore(value="100")
	public void testWithValidInputs(){
		float actual=service.calcIntrAmt(10000, 20, 2);
		assertEquals(4000,actual,0.1f);
	}
	@Test
	public void testWithValidInputs1(){
		float actual=service.calcIntrAmt(10000.4f, 20.3f, 2.2f);
		assertEquals(4466.178f,actual,0.1f);
	}
	
	/*@Test(expected=IllegalArgumentException.class)
	public void testWithInvalidInPuts(){
		float acutal=service.calcIntrAmt(10, 23, 4);
		fail("Exception is excepted but not Raised");
	}*/

}
