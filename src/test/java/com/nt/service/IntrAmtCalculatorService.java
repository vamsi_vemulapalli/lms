package com.nt.service;

public class IntrAmtCalculatorService {
	
	public float calcIntrAmt(float pamt,float time,float rate){
		try{
		Thread.sleep(200);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		if(pamt<=0 || time<=0 || rate<=0 ){
			throw new IllegalArgumentException("Invalid inputs");
		}
		return (pamt*time*rate)/100.0f;
	}
}
